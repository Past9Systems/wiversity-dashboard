import * as React from 'react'

import './SplashBackground.scss'

export default class SplashBackground extends React.Component {
  public render() {
    return <div className='splash-background'>{this.props.children}</div>
  }
}
