import * as React from 'react'
import If from './If'

import './FileInput.scss'

import { IconDefinition } from '@fortawesome/fontawesome-common-types'
import { FontAwesomeIcon as Icon } from '@fortawesome/react-fontawesome'
import {
  fileAltIcon,
  fileArchiveIcon,
  fileAudioIcon,
  fileCodeIcon,
  fileExcelIcon,
  fileIcon,
  fileImageIcon,
  filePdfIcon,
  filePowerpointIcon,
  fileVideoIcon,
  fileWordIcon,
  timesCircleIcon,
  timesCircleSolidIcon
} from '../lib/Icons'

function previewImage(file: File): JSX.Element {
  return (
    <div className='file-input__image-preview'>
      <img
        src={URL.createObjectURL(file)}
        className='file-input__image-preview-image'
      />
    </div>
  )
}

const fileTypePreviews = new Map<string[], (file: File) => JSX.Element>([
  [['jpg', 'jpeg', 'gif', 'bmp', 'ico', 'png', 'svg'], previewImage]
])

const fileTypeIcons = new Map<string[], IconDefinition>([
  [['txt', 'rtf'], fileAltIcon],
  [['doc', 'docx'], fileWordIcon],
  [['avi', 'flv', 'wmv', 'mov', 'mp4', 'mpg', 'mpeg'], fileVideoIcon],
  [['ppt', 'pptx'], filePowerpointIcon],
  [['pdf'], filePdfIcon],
  [['jpg', 'jpeg', 'gif', 'tiff', 'bmp', 'ico', 'png', 'svg'], fileImageIcon],
  [['xls', 'xlsx'], fileExcelIcon],
  [
    [
      'bat',
      'c',
      'cc',
      'cmd',
      'cpp',
      'cs',
      'css',
      'cxx',
      'f',
      'go',
      'h',
      'html',
      'hxx',
      'hpp',
      'inc',
      'java',
      'js',
      'jsp',
      'jsx',
      'php',
      'pl',
      'ps',
      'py',
      'rb',
      'rs',
      'sh',
      'vb',
      'xml'
    ],
    fileCodeIcon
  ],
  [['wav', 'mp3', 'aac', 'ogg', 'wma'], fileAudioIcon],
  [['7z', 'bzip2', 'gzip', 'tar', 'zip', 'wim'], fileArchiveIcon]
])

function _getFilePreview(file: File): JSX.Element {
  // Check if the file has an extension. If not, show a generic file icon.
  const parts = file.name.split('.')

  if (parts.length < 2)
    return <Icon icon={fileIcon} className='file-input__file-icon' />

  // Get the file extension
  const ext = parts[parts.length - 1]

  // Render a preview of the file, if a preview function exists for the file type
  for (const kvp of Array.from(fileTypePreviews)) {
    const key = kvp[0]
    const callback = kvp[1]
    if (key.indexOf(ext) > -1) return callback(file)
  }

  // If no file preview function is found, return an icon representing the file type
  for (const kvp of Array.from(fileTypeIcons)) {
    const key = kvp[0]
    const value = kvp[1]
    if (key.indexOf(ext) > -1)
      return <Icon icon={value} className='file-input__file-icon' />
  }

  // If no preview or icon has been found, just return the generic icon
  return <Icon icon={fileIcon} className='file-input__file-icon' />
}

export interface IFileInputProps {
  files: IUniqueFile[]
  onChange?: (value: IUniqueFile[]) => void
  disabled?: boolean
  error?: Error
  loading?: boolean
  multiple?: boolean
}

export interface IUniqueFile extends File {
  id: number
}

export default class FileInput extends React.Component<IFileInputProps> {
  private static _onInputDrop(e: React.DragEvent<HTMLInputElement>) {
    e.stopPropagation()
  }

  private static _onContainerDrop(e: React.DragEvent<HTMLDivElement>): boolean {
    e.preventDefault()
    e.stopPropagation()
    return false
  }

  private static _getFileSize(bytes: number): string {
    const denominations = ['B', 'KB', 'MB', 'GB', 'TB']
    let numThousands = 0
    let remaining = bytes
    while (remaining / 1024 > 1) {
      remaining /= 1024
      ++numThousands
    }
    return `${numThousands > 0 ? remaining.toFixed(1) : remaining} ${
      denominations[numThousands]
    }`
  }

  private static _getClasses(): string {
    return ['file-input'].filter(n => !!n).join(' ')
  }

  private _fileIdSeed = 0

  public constructor(props: IFileInputProps) {
    super(props)
  }

  public render() {
    const p = this.props

    return (
      <div
        className={FileInput._getClasses()}
        onDrop={e => FileInput._onContainerDrop(e)}
        onDragOver={e => FileInput._onContainerDrop(e)}
      >
        <div className='file-input__target-container'>
          <input
            className='file-input__input'
            type='file'
            multiple={p.multiple}
            disabled={p.disabled}
            onChange={e => this._onAddFiles(e)}
            onDrop={e => FileInput._onInputDrop(e)}
            onDragOver={e => FileInput._onInputDrop(e)}
          />
          <div className='file-input__target-background'>
            Click or drag-and-drop here to add{' '}
            {this.props.multiple ? 'files' : 'a file'}.
          </div>
        </div>
        <div className='file-input__file-list'>
          <If condition={this.props.files.length > 0}>
            {this.props.files.map((f, i) => this._renderFile(f, i))}
          </If>
          <If condition={this.props.files.length <= 0}>
            <div className='file-input__no-files'>
              No file{this.props.multiple ? 's' : ''} selected
            </div>
          </If>
        </div>
        {this._renderErrors()}
      </div>
    )
  }

  private _getNextFileId() {
    return ++this._fileIdSeed
  }

  private _onAddFiles(e: React.ChangeEvent<HTMLInputElement>) {
    const fileList: FileList | null = e.target.files

    let files: IUniqueFile[] = this.props.files

    if (fileList !== null) {
      if (this.props.multiple) {
        for (let i = 0; i < fileList.length; ++i) {
          const file = fileList.item(i)
          if (file === null) continue
          const uniqueFile = file as IUniqueFile
          uniqueFile.id = this._getNextFileId()
          files.push(uniqueFile)
        }
      } else {
        const file = fileList.item(0)
        if (file !== null) {
          const uniqueFile = file as IUniqueFile
          uniqueFile.id = this._getNextFileId()
          files = [uniqueFile]
        }
      }
    }

    if (this.props.onChange) this.props.onChange(files)
  }

  private _renderFile(file: IUniqueFile, index: number): JSX.Element {
    let classes = 'file-input__file'

    if (this.props.multiple) classes += ' file-input__file-multiple'

    return (
      <div className={classes} key={index} title={file.name}>
        <button
          className='file-input__file-close-button'
          onClick={e => this._removeFile(file.id)}
          title='Remove file'
        >
          <Icon icon={timesCircleIcon} />
        </button>
        {_getFilePreview(file)}
        <div className='file-input__file-name'>{file.name}</div>
        <div className='file-input__file-size'>
          {FileInput._getFileSize(file.size)}
        </div>
      </div>
    )
  }

  private _removeFile(id: number) {
    if (this.props.onChange)
      this.props.onChange(this.props.files.filter(f => f.id !== id))
  }

  private _renderErrors(): JSX.Element | undefined {
    if (!this.props.error) return undefined

    const err = this.props.error as any

    const messages: string[] = []
    if (err.invalidFields)
      for (const key in err.invalidFields) messages.push(err.invalidFields[key])

    return (
      <div className='file-input__error-container'>
        <If condition={messages.length > 0}>
          One or more files could not be processed for the following reasons:
          <div className='file-input__error-list'>
            {messages.map((m, i) => (
              <div className='file-input__error' key={i}>
                <Icon
                  className='file-input__error-icon'
                  icon={timesCircleSolidIcon}
                />
                {m}
              </div>
            ))}
          </div>
        </If>
      </div>
    )
  }
}
