import * as React from 'react'

import './Center.scss'

export interface ICenterProps {
  maxWidth: string
  top?: string
}

export default class Center extends React.Component<ICenterProps> {
  public render() {
    const p = this.props

    return (
      <div
        className='center'
        style={{
          marginTop: p.top,
          maxWidth: p.maxWidth
        }}
      >
        {p.children}
      </div>
    )
  }
}
