import * as React from 'react'
import TextField from './TextField'

import ProgressBar from './ProgressBar'
import './Toolbar.scss'

export interface IMenuProps {
  inProgress?: boolean
}

export default class Menu extends React.Component<IMenuProps> {
  public render() {
    return (
      <div className='toolbar'>
        <ProgressBar inProgress={this.props.inProgress} />
        <div className='toolbar__search'>
          <TextField
            placeholder='Search'
            marginTop='0'
            marginBottom='0'
            maxWidth='100%'
          />
        </div>
      </div>
    )
  }
}
