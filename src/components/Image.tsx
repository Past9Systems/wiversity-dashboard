import * as React from 'react'
import If from './If'

import { FontAwesomeIcon as Icon } from '@fortawesome/react-fontawesome'
import { banIcon, imageIcon, spinnerThirdIcon } from '../lib/Icons'

import { inject, observer } from 'mobx-react'
import ApiClient from '../api/ApiClient'
import './Image.scss'

export interface IImageProps {
  api?: ApiClient
  documentId: string | null
  token?: string | null
  bottomSpace?: number
}

interface IImageState {
  isLoading: boolean
  isLoaded: boolean
  isErrored: boolean
  imageBlob: Blob | null
}

@inject('api')
@observer
export default class Image extends React.Component<IImageProps, IImageState> {
  public container: React.RefObject<HTMLDivElement>

  public constructor(props: IImageProps) {
    super(props)
    this.state = {
      imageBlob: null,
      isErrored: false,
      isLoaded: false,
      isLoading: false
    }
  }

  public async componentWillReceiveProps(nextProps: IImageProps) {
    if (nextProps.token && nextProps.token !== this.props.token) {
      this._onLoadStart()

      try {
        const res = await this.props.api!.getDocument(
          nextProps.documentId!,
          nextProps.token!
        )
        this._onLoaded(res)
      } catch (e) {
        this._onError()
      }
    }
  }

  public render() {
    const styles: any = {}

    if (this.props.bottomSpace)
      styles.paddingBottom = `${this.props.bottomSpace}rem`

    return (
      <div className='image' style={styles}>
        {this.props.children}
        <If
          condition={
            !this.props.token ||
            (!this.state.isLoading &&
              !this.state.isLoaded &&
              !this.state.isErrored)
          }
        >
          <div className='image__fake'>
            <Icon className='image__fake-icon' icon={imageIcon} />
          </div>
        </If>
        <If condition={this.state.isErrored}>
          <div className='image__error'>
            <span className='image__error-icon-layers fa-layers'>
              <Icon className='image__error-icon-1' icon={imageIcon} />
              <Icon className='image__error-icon-2' icon={banIcon} />
            </span>
          </div>
        </If>
        <If condition={this.state.isLoading}>
          <div className='image__loading'>
            <span className='image__loading-icon-layers fa-layers'>
              <Icon className='image__loading-icon-1' icon={imageIcon} />
              <Icon
                className='image__loading-icon-2'
                icon={spinnerThirdIcon}
                spin={true}
              />
            </span>
          </div>
        </If>
        <If condition={!!this.props.token}>
          <img
            className={`image__loaded ${
              this.state.isLoaded ? '' : 'image__loaded--hidden'
            }`}
            src={
              !this.state.isLoaded
                ? ''
                : URL.createObjectURL(this.state.imageBlob!)
            }
          />
        </If>
      </div>
    )
  }

  private _onLoadStart() {
    this.setState({
      imageBlob: null,
      isErrored: false,
      isLoaded: false,
      isLoading: true
    })
  }

  private _onLoaded(image: Blob) {
    this.setState({
      imageBlob: image,
      isErrored: false,
      isLoaded: true,
      isLoading: false
    })
  }

  private _onError() {
    this.setState({
      imageBlob: null,
      isErrored: true,
      isLoaded: false,
      isLoading: false
    })
  }
}
