import * as React from 'react'
import Heading from '../components/Heading'
import Notification from '../components/Notification'
import ProgressBar from '../components/ProgressBar'
import SlottedComponent from '../components/SlottedComponent'
import { Status } from '../state/Store'

import { Params } from 'router5'
import { RouteNames } from '../Routes'
import Breadcrumbs, { IBreadcrumbProps } from './Breadcrumbs'
import Content from './Content'
import './Frame.scss'
import If from './If'

export interface IFrameProps {
  headingSize?: 1 | 2 | 3 | 4 | 5 | 6
  heading?: string | null
  status?: Status | Status[]
  inProgress?: boolean
  center?: boolean
  centerWidth?:
    | 1
    | 2
    | 3
    | 4
    | 5
    | 6
    | 7
    | 8
    | 9
    | 10
    | 11
    | 12
    | 13
    | 14
    | 15
    | 16
    | 17
    | 18
    | 19
    | 20
    | 21
    | 22
    | 23
    | 24
  noPadding?: boolean
  breadcrumbs?: IBreadcrumbProps
  headingRouteName?: RouteNames
  headingRouteParams?: Params
}

export default class Frame extends SlottedComponent<IFrameProps> {
  private static _renderStatus(status: Status, key?: number) {
    if (status && status.hasError)
      return (
        <Notification
          key={key}
          type='error'
          message={status.error && status.error.message}
        />
      )

    if (status && status.successful && status.successMessage)
      return (
        <Notification
          key={key}
          type='success'
          message={status.successMessage}
        />
      )

    return null
  }

  public footer: React.ReactNode
  public rightOfHeading: React.ReactNode
  public aboveHeading: React.ReactNode
  public belowHeading: React.ReactNode

  public render() {
    this.resolveSlots()
    return (
      <>
        <div className='frame'>
          <If condition={!!this.props.breadcrumbs}>
            <Breadcrumbs {...this.props.breadcrumbs!} />
          </If>
          <div className='frame__header'>
            <Heading
              level={this.props.headingSize || 1}
              text={this.props.heading}
              loading={this.props.inProgress}
              routeName={this.props.headingRouteName}
              routeParams={this.props.headingRouteParams}
            >
              <Content slot='right'>{this.rightOfHeading}</Content>
              <Content slot='above'>{this.aboveHeading}</Content>
              <Content slot='below'>{this.belowHeading}</Content>
            </Heading>
          </div>
          <ProgressBar inProgress={this.props.inProgress} />
          {this._renderBody()}
          {this._renderFooter()}
        </div>
        {this._renderAfter()}
      </>
    )
  }

  private _renderStatuses() {
    if (!this.props.status) return null

    if (Array.isArray(this.props.status))
      return this.props.status.map((s, i) => Frame._renderStatus(s, i))

    return Frame._renderStatus(this.props.status)
  }

  private _renderBody() {
    if (!this.children) return null

    return (
      <div
        className={`frame__body ${
          this.props.noPadding ? 'frame__body--borderless' : ''
        }`}
      >
        {this._renderStatuses()}
        {this.children}
      </div>
    )
  }

  private _renderAfter() {
    if (this.children) return null
    return this._renderStatuses()
  }

  private _renderFooter() {
    if (!this.footer) return null
    return <div className='frame__footer'>{this.footer}</div>
  }
}
