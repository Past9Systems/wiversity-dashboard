import * as React from 'react'
import Keys from '../lib/Keys'
import If from './If'

import translate from '../lib/translate'
import './TextField.scss'

export interface ITextFieldProps {
  label?: string
  placeholder?: string
  value?: string
  disabled?: boolean
  onEnter?: () => void
  onChange?: (value: string) => void
  stacked?: boolean
  password?: boolean
  autoFocus?: boolean
  errorField?: string
  error?: Error
  readonly?: boolean
  loading?: boolean
  marginTop?: string
  marginBottom?: string
  width?: string
  minWidth?: string
  maxWidth?: string
  float?: 'left' | 'right'
  multiline?: boolean
  height?: string
}

export default class TextField extends React.Component<ITextFieldProps> {
  public render() {
    const p = this.props

    const styles = {
      float: this.props.float,
      marginBottom: this.props.marginBottom,
      marginTop: this.props.marginTop,
      maxWidth: this.props.maxWidth,
      minWidth: this.props.minWidth,
      width: this.props.width
    }

    return (
      <div className={this._getClasses()} style={styles}>
        <If condition={p.label !== undefined}>
          <label className='text-field__label'>{p.label}</label>
        </If>
        <If condition={!p.readonly}>
          {this._renderInput()}
          {this._renderError()}
        </If>
        <If condition={!!p.readonly && !!p.loading && !p.value}>
          <span className='text-field__readonly text-field__readonly--loading' />
        </If>
        <If condition={!!p.readonly && !p.loading}>
          <span className='text-field__readonly'>{p.value}</span>
        </If>
      </div>
    )
  }

  private _renderInput() {
    const p = this.props

    if (p.multiline) {
      return (
        <textarea
          className='text-field__input text-field__input--multiline'
          disabled={p.disabled}
          onChange={e => p.onChange && p.onChange(e.target.value)}
          onKeyDown={e => this._processKey(e)}
          placeholder={p.placeholder}
          autoFocus={p.autoFocus}
          style={{ height: p.height }}
          value={p.value}
        />
      )
    } else {
      return (
        <input
          className='text-field__input'
          type={p.password ? 'password' : 'text'}
          value={p.value}
          disabled={p.disabled}
          onChange={e => p.onChange && p.onChange(e.target.value)}
          onKeyDown={e => this._processKey(e)}
          placeholder={p.placeholder}
          autoFocus={p.autoFocus}
        />
      )
    }
  }

  private _getClasses(): string {
    return ['text-field', this.props.stacked ? 'text-field--stacked' : '']
      .filter(n => !!n)
      .join(' ')
  }

  private _processKey(
    e:
      | React.KeyboardEvent<HTMLInputElement>
      | React.KeyboardEvent<HTMLTextAreaElement>
  ) {
    if (e.keyCode === Keys.Enter) {
      if (this.props.onEnter) this.props.onEnter()
    }
  }

  private _renderError(): JSX.Element | undefined {
    if (!this.props.errorField) return undefined
    if (!this.props.error) return undefined

    const err = this.props.error as any

    if (!err.invalidFields) return undefined

    if (!err.invalidFields[this.props.errorField]) return undefined

    return (
      <div className='text-field__error'>
        {translate(err.invalidFields[this.props.errorField])}
      </div>
    )
  }
}
