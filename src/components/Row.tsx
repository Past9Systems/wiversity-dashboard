import * as React from 'react'

import './Row.scss'

export interface IRowProps {
  topSpace?: number
  bottomSpace?: number
}

export default class Row extends React.Component<IRowProps> {
  public render() {
    const p = this.props

    const styles: any = {}

    if (p.topSpace) styles.marginTop = `${p.topSpace}rem`
    if (p.bottomSpace) styles.marginBottom = `${p.bottomSpace}rem`

    return (
      <div className='row' style={styles}>
        {p.children}
      </div>
    )
  }
}
