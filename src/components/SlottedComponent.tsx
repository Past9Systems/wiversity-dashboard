import * as React from 'react'
import Content from './Content'

export default class SlottedComponent<TProps> extends React.Component<TProps> {
  public children: React.ReactNode

  public resolveSlots(): void {
    const children: React.ReactChild[] = []

    if (this.props.children) {
      React.Children.forEach(this.props.children, child => {
        const value = child.valueOf()
        const anyValue = value as any
        if (
          anyValue.type &&
          anyValue.type.prototype &&
          anyValue.type.prototype.hasOwnProperty('isSlotContent')
        ) {
          const content = value as Content
          this[content.props.slot] = content.props.children
        } else {
          children.push(child)
        }
      })
    }

    this.children = children.length > 0 ? children : null
  }
}
