import * as React from 'react'

import './Card.scss'

export interface ICardProps {
  top?: string
}

export class Card extends React.Component<ICardProps> {
  private static _getClasses(): string {
    return ['card'].filter(n => !!n).join(' ')
  }

  public render() {
    const p = this.props

    return (
      <div className={Card._getClasses()} style={{ marginTop: p.top }}>
        {p.children}
      </div>
    )
  }
}

export class CardHeader extends React.Component<{}> {
  public render() {
    const p = this.props

    return <div className='card__header'>{p.children}</div>
  }
}

export class CardBody extends React.Component<{}> {
  public render() {
    const p = this.props

    return <div className='card__body'>{p.children}</div>
  }
}

export class CardFooter extends React.Component<{}> {
  public render() {
    const p = this.props

    return <div className='card__footer'>{p.children}</div>
  }
}
