import * as React from 'react'
import { Params, Router } from 'router5'

import { inject, observer } from 'mobx-react'
import './HeadingDetails.scss'

export interface IHeadingDetailsProps {
  router?: Router
  routeName?: string
  routeParams?: Params
}

@inject('router')
@observer
export default class EditSchoolPage extends React.Component<
  IHeadingDetailsProps
> {
  public render() {
    return (
      <div
        className={`heading-details ${
          !!this.props.routeName ? 'heading-details--link' : ''
        }`}
        onClick={e => {
          if (this.props.routeName)
            this.props.router!.navigate(
              this.props.routeName,
              this.props.routeParams || {}
            )
        }}
      >
        {this.props.children}
      </div>
    )
  }
}
