import * as React from 'react'

import './Breadcrumbs.scss'

import { FontAwesomeIcon as Icon } from '@fortawesome/react-fontawesome'
import { inject, observer } from 'mobx-react'
import { Params, Router } from 'router5'
import { angleRightIcon, caretLeftIcon, caretRightIcon } from '../lib/Icons'
import { RouteNames } from '../Routes'

export interface IBreadcrumbProps {
  router?: Router
  noMargin?: boolean
  current?: string | null
  crumbs: Array<{
    text?: string | null
    routeName: RouteNames
    routeParams: Params
  }>
  waitFor?: any[]
}

interface IBreadcrumbsState {
  dragging: boolean
  dragStartX: number
  dragCurrentX: number
  lastStoppedX: number
  left: number
  noClick: boolean
}

@inject('router')
@observer
export default class Breadcrumbs extends React.Component<
  IBreadcrumbProps,
  IBreadcrumbsState
> {
  private _container: HTMLDivElement | null
  private _slider: HTMLUListElement | null

  public get _minLeft(): number {
    if (!this._container) return 0
    if (!this._slider) return 0
    if (this._slider.offsetWidth < this._container.offsetWidth) return 0
    return this._container.offsetWidth - this._slider.offsetWidth
  }

  public constructor(props: IBreadcrumbProps) {
    super(props)
    this.state = {
      dragCurrentX: 0,
      dragStartX: 0,
      dragging: false,
      lastStoppedX: 0,
      left: 0,
      noClick: false
    }
  }

  public componentDidUpdate(prevProps: IBreadcrumbProps) {
    if (prevProps === this.props) return
    const minLeft = this._minLeft
    if (this.state.lastStoppedX !== minLeft || this.state.left !== minLeft) {
      this.setState({
        lastStoppedX: this._minLeft,
        left: this._minLeft
      })
    }
  }

  public componentDidMount() {
    window.addEventListener('mouseup', this._onMouseUp.bind(this))
    window.addEventListener('mousemove', this._onMouseMove.bind(this))
  }

  public componentWillUnmount() {
    window.removeEventListener('mouseup', this._onMouseUp.bind(this))
    window.removeEventListener('mousemove', this._onMouseMove.bind(this))
  }

  public render() {
    let leftIndicatorClasses =
      'breadcrumbs__more-indicator breadcrumbs__more-indicator--left'
    if (this.state.left >= 0)
      leftIndicatorClasses += ' breadcrumbs__more-indicator--invisible'

    let rightIndicatorClasses =
      'breadcrumbs__more-indicator breadcrumbs__more-indicator--right'
    if (this.state.left <= this._minLeft)
      rightIndicatorClasses += ' breadcrumbs__more-indicator--invisible'

    // Defer displaying the breadcrumb bar until all the route params have been loaded
    if (Array.isArray(this.props.waitFor)) {
      for (const item of this.props.waitFor) {
        if (!item)
          return (
            <div ref={e => (this._container = e)} className='breadcrumbs' />
          )
      }
    }

    return (
      <div ref={e => (this._container = e)} className='breadcrumbs'>
        <div className={leftIndicatorClasses}>
          <Icon icon={caretLeftIcon} />
        </div>
        <ul
          ref={e => (this._slider = e)}
          className='breadcrumbs__slider'
          style={{ left: `${this.state.left}px` }}
          onTouchStart={e => this._onTouchStart(e)}
          onTouchMove={e => this._onTouchMove(e)}
          onTouchEnd={e => this._onTouchEnd(e)}
          onMouseDown={e => this._onMouseDown(e)}
          onMouseMove={e => this._onMouseMove(e)}
          onMouseUp={e => this._onMouseUp(e)}
          onClick={e => this._onClick(e)}
        >
          {this.props.crumbs.map((c, i) => (
            <li className='breadcrumbs__crumb' key={i}>
              <a
                draggable={false}
                className='breadcrumbs__crumb-link'
                onClick={e => {
                  e.preventDefault()

                  if (this.state.noClick) {
                    this.setState({ noClick: false })
                    return
                  }

                  this.props.router!.navigate(c.routeName, c.routeParams)
                }}
                href={this.props.router!.buildUrl(c.routeName, c.routeParams)}
              >
                {c.text}
              </a>
              <Icon
                className='breadcrumbs__crumb-separator'
                icon={angleRightIcon}
              />
            </li>
          ))}
          <li className='breadcrumbs__crumb breadcrumbs__crumb--no-link'>
            {this.props.current}
          </li>
        </ul>
        <div className={rightIndicatorClasses}>
          <Icon icon={caretRightIcon} />
        </div>
      </div>
    )
  }

  private _start(x: number) {
    if (this.state.dragging) return
    this.setState({
      dragCurrentX: x,
      dragStartX: x,
      dragging: true
    })
  }

  private _move(x: number, isTouch: boolean) {
    if (!this.state.dragging) return

    let left =
      this.state.lastStoppedX + this.state.dragCurrentX - this.state.dragStartX

    const minLeft = this._minLeft
    if (left < minLeft) left = minLeft

    if (left > 0) left = 0

    this.setState({
      dragCurrentX: x,
      left,
      noClick: !isTouch
    })
  }

  private _end() {
    if (!this.state.dragging) return
    this.setState({
      dragCurrentX: 0,
      dragStartX: 0,
      dragging: false,
      lastStoppedX: this.state.left
    })
  }

  private _onClick(e: React.MouseEvent<HTMLUListElement>) {
    this.setState({
      noClick: false
    })
  }

  private _onMouseDown(e: React.MouseEvent<HTMLUListElement>) {
    e.stopPropagation()
    this._start(e.pageX)
  }

  private _onMouseMove(e: React.MouseEvent<HTMLUListElement>) {
    e.stopPropagation()
    this._move(e.pageX, false)
  }

  private _onMouseUp(e: React.MouseEvent<HTMLUListElement>) {
    e.stopPropagation()
    this._end()
  }

  private _onTouchStart(e: React.TouchEvent<HTMLUListElement>) {
    e.stopPropagation()
    if (e.touches.length !== 1) return
    const touch = e.touches[0]
    this._start(touch.pageX)
  }

  private _onTouchMove(e: React.TouchEvent<HTMLUListElement>) {
    e.stopPropagation()
    if (e.touches.length !== 1) return
    const touch = e.touches[0]
    this._move(touch.pageX, true)
  }

  private _onTouchEnd(e: React.TouchEvent<HTMLUListElement>) {
    e.stopPropagation()
    this._end()
  }
}
