import * as React from 'react'

import * as ReactDOM from 'react-dom'
import './Droppable.scss'

export interface IDroppableProps {
  onDrop: () => void
  onEnter?: () => void
  onLeave?: () => void
  canDrop: boolean
  style?: React.CSSProperties
}

export default class Droppable extends React.Component<IDroppableProps> {
  private _element: HTMLDivElement | null
  private _touchMoveWindowListener: any = this._onTouchMove.bind(this)
  private _touchEndWindowListener: any = this._onTouchEnd.bind(this)
  private _lastTouchEventWasHit = false

  public constructor(props: IDroppableProps) {
    super(props)
  }

  public componentDidMount() {
    window.addEventListener('touchmove', this._touchMoveWindowListener)
    window.addEventListener('touchend', this._touchEndWindowListener)
  }

  public componentWillUnmount() {
    window.removeEventListener('touchmove', this._touchMoveWindowListener)
    window.removeEventListener('touchend', this._touchEndWindowListener)
  }

  private _onTouchMove(e: React.TouchEvent<HTMLDivElement>) {
    const hit = this._touchIsHit(e)
    if (hit && !this._lastTouchEventWasHit) this._onEnter()
    else if (!hit && this._lastTouchEventWasHit) this._onLeave()

    this._lastTouchEventWasHit = hit
  }

  private _onTouchEnd(e: React.TouchEvent<HTMLDivElement>) {
    const hit = this._touchIsHit(e)
    if (hit) this._onDrop()
  }

  private _touchIsHit(e: React.TouchEvent<HTMLDivElement>) {
    let touch = e.touches[0]

    if (!touch) touch = e.changedTouches[0]

    const cX = touch.clientX
    const cY = touch.clientY

    const rect = this._element!.getBoundingClientRect()

    return (
      cX > rect.left && cX < rect.right && cY > rect.top && cY < rect.bottom
    )
  }

  private _onPointerUp(e: React.PointerEvent<HTMLDivElement>) {
    this._onDrop()
  }

  private _onPointerEnter(e: React.PointerEvent<HTMLDivElement>) {
    this._onEnter()
  }

  private _onPointerLeave(e: React.PointerEvent<HTMLDivElement>) {
    this._onLeave()
  }

  private _onDrop() {
    if (this.props.onDrop) this.props.onDrop()
  }

  private _onEnter() {
    if (this.props.onEnter) this.props.onEnter()
  }

  private _onLeave() {
    if (this.props.onLeave) this.props.onLeave()
  }

  public render() {
    return (
      <div
        style={this.props.style}
        ref={el => (this._element = el)}
        className='droppable'
        onPointerUp={e => this._onPointerUp(e)}
        onPointerEnter={e => this._onPointerEnter(e)}
        onPointerLeave={e => this._onPointerLeave(e)}
      >
        {this.props.children}
      </div>
    )
  }
}
