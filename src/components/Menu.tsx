import * as React from 'react'
import { Params, Router } from 'router5'
import { RouteNames } from '../Routes'
import If from './If'

import { IconDefinition } from '@fortawesome/fontawesome-common-types'
import { FontAwesomeIcon as Icon } from '@fortawesome/react-fontawesome'
import {
  barsIcon,
  chalkboardIcon,
  homeIcon,
  powerOffIcon,
  schoolIcon,
  userGraduateIcon,
  userIcon
} from '../lib/Icons'

import { inject, observer } from 'mobx-react'
import IConfig from '../IConfig'
import Store from '../state/Store'
import './Menu.scss'

interface ILinkProps {
  store?: Store
  router?: Router
  config?: IConfig
  isCurrent?: boolean
  caption: string
  icon: IconDefinition
  onClick?: () => void
  routeName?: string
  routeParams?: Params
  tooltip: string
  style?: object
}

@inject('store', 'router', 'config')
@observer
class Link extends React.Component<ILinkProps> {
  public render() {
    const routeName = this.props.routeName || RouteNames.Home
    const routeParams = this.props.routeParams || {}

    return (
      <a
        title={this.props.tooltip}
        style={this.props.style}
        className={`menu__link ${
          this.props.isCurrent ? 'menu__link--current' : ''
        }`}
        href={this.props.router!.buildUrl(routeName, routeParams)}
        onClick={e => this._navigate(e, routeName, routeParams)}
      >
        <div className='menu__link-icon'>
          <Icon icon={this.props.icon} />
        </div>
        <If condition={!!this.props.caption}>
          <div className='menu__link-text'>{this.props.caption}</div>
        </If>
      </a>
    )
  }

  private _navigate(
    e: React.MouseEvent<HTMLAnchorElement>,
    routeName: string,
    routeParams: Params = {}
  ) {
    e.preventDefault()

    if (window.innerWidth <= this.props.config!.smallDeviceMaxWidth)
      this.props.store!.app.dashboard.toggleMenu(false)

    if (this.props.onClick) this.props.onClick()

    this.props.router!.navigate(routeName, routeParams)
  }
}

export interface IMenuProps {
  store?: Store
}

@inject('store')
@observer
export default class Menu extends React.Component<IMenuProps> {
  public render() {
    return (
      <>
        <button
          className='menu__toggle'
          onClick={() => this.props.store!.app.dashboard.toggleMenu()}
        >
          <Icon icon={barsIcon} />
        </button>
        <nav
          className={`menu ${
            this.props.store!.app.dashboard.menuOpen
              ? 'menu--open'
              : 'menu--closed'
          }`}
        >
          <Link
            isCurrent={
              this.props.store!.route === RouteNames.Home ||
              this.props.store!.route === RouteNames.SchoolsList
            }
            caption='Home'
            tooltip='Home'
            icon={homeIcon}
            routeName={RouteNames.Home}
          />
          <If condition={!!this.props.store!.app.dashboard.schoolId}>
            <Link
              isCurrent={this.props.store!.route === RouteNames.SchoolDetails}
              caption='School'
              tooltip='View school details'
              icon={schoolIcon}
              routeName={RouteNames.SchoolDetails}
              routeParams={{
                schoolId: this.props.store!.app.dashboard.schoolId
              }}
            />
            <Link
              isCurrent={this.props.store!.route === RouteNames.CoursesInSchool}
              caption='Courses'
              tooltip='View courses in school'
              icon={chalkboardIcon}
              routeName={RouteNames.CoursesInSchool}
              routeParams={{
                schoolId: this.props.store!.app.dashboard.schoolId
              }}
            />
            <Link
              isCurrent={
                this.props.store!.route === RouteNames.StudentsInSchoolList
              }
              caption='Students'
              tooltip='View students in school (all courses)'
              icon={userGraduateIcon}
              routeName={RouteNames.StudentsInSchoolList}
              routeParams={{
                schoolId: this.props.store!.app.dashboard.schoolId
              }}
            />
          </If>
          <Link
            isCurrent={this.props.store!.route === RouteNames.Account}
            caption='My account'
            tooltip='View/edit my account details'
            icon={userIcon}
            routeName={RouteNames.Account}
            routeParams={{}}
            style={{ position: 'absolute', bottom: '3rem' }}
          />
          <Link
            caption='Log out'
            tooltip='Log out of Wiversity dashboard'
            icon={powerOffIcon}
            onClick={() => this.props.store!.app.logOut()}
            style={{ position: 'absolute', bottom: 0 }}
          />
        </nav>
        <div
          className={`menu__overlay ${
            this.props.store!.app.dashboard.menuOpen
              ? 'menu__overlay--visible'
              : 'menu__overlay--hidden'
          }`}
          onClick={() => this.props.store!.app.dashboard.toggleMenu(false)}
        />
      </>
    )
  }
}
