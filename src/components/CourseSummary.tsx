import { inject, observer } from 'mobx-react'
import * as React from 'react'
import { Course } from '../api/Dtos'
import { RouteNames } from '../Routes'
import './CourseSummary.scss'
import Frame from './Frame'

import { IServices } from '..'
import DashboardState from '../state/DashboardState'

export interface ICourseSummaryProps {
  dashboard?: DashboardState
  course: Course
}

@inject((services: IServices) => ({
  dashboard: services.store.app.dashboard
}))
@observer
export default class CourseSummary extends React.Component<
  ICourseSummaryProps
> {
  public render() {
    return (
      <Frame
        headingSize={4}
        heading={this.props.course.name}
        headingRouteName={RouteNames.CourseDetails}
        headingRouteParams={{
          courseId: this.props.course.id,
          schoolId: this.props.dashboard!.schoolId
        }}
      />
    )
  }
}
