import * as React from 'react'

import './ProgressBar.scss'

export interface IProgressBarInternalState {
  progress: number
}

export interface IProgressBarProps {
  inProgress?: boolean
  noLayout?: boolean
}

export default class ProgressBar extends React.Component<
  IProgressBarProps,
  IProgressBarInternalState
> {
  private _progressAnimationTimer = 0
  private _completedAnimationTimer = 0

  public constructor(props: IProgressBarProps) {
    super(props)
    this.state = {
      progress: 0
    }
  }

  public componentDidUpdate(
    prevProps: IProgressBarProps,
    prevState: IProgressBarInternalState
  ) {
    if (!prevProps.inProgress && this.props.inProgress)
      this._startProgressAnimation()

    if (prevProps.inProgress && !this.props.inProgress) {
      this._startCompletedAnimation()
    }
  }

  public componentWillUnmount() {
    this._clearAnimations()
  }

  public render() {
    return (
      <div className={this._getClasses()}>
        <div
          className='progress-bar__progress'
          style={{ width: `${this.state && this.state.progress}%` }}
        />
      </div>
    )
  }

  private _getClasses(): string {
    return [
      'progress-bar',
      this.props.noLayout ? 'progress-bar--no-layout' : '',
      this.props.inProgress ? 'progress-bar--in-progress' : ''
    ]
      .filter(n => !!n)
      .join(' ')
  }

  private _startProgressAnimation() {
    this._clearAnimations()
    this._progressAnimationTimer = window.setInterval(
      () => this._makeProgress(),
      25
    )
  }

  private _makeProgress() {
    const progress = this.state ? this.state.progress : 0
    this.setState({
      progress: progress + (95 - progress) / 50
    })
  }

  private _startCompletedAnimation() {
    this._clearAnimations()
    this.setState({ progress: 100 })
    this._completedAnimationTimer = window.setTimeout(
      () => this._showCompleted(),
      150
    )
  }

  private _showCompleted() {
    this.setState({
      progress: 0
    })
  }

  private _clearProgressAnimation() {
    window.clearInterval(this._progressAnimationTimer)
  }

  private _clearCompletedAnimation() {
    window.clearTimeout(this._completedAnimationTimer)
  }

  private _clearAnimations() {
    this._clearProgressAnimation()
    this._clearCompletedAnimation()
  }
}
