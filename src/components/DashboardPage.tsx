import * as React from 'react'

import './DashboardPage.scss'
import ProgressBar from './ProgressBar'

export interface IPageProps {
  maxWidth?: string
}

export default class Page extends React.Component<IPageProps> {
  private get _maxWidth() {
    if (this.props.maxWidth === undefined) return '63rem'
    return this.props.maxWidth
  }

  public constructor(props: IPageProps) {
    super(props)
  }

  public render() {
    return (
      <div className='page' style={{ maxWidth: this._maxWidth }}>
        {this.props.children}
      </div>
    )
  }
}
