import * as React from 'react'

import './Column.scss'

export interface IColumnProps {
  span: number
  stubborn?: boolean
  top?: string
}

export default class Column extends React.Component<IColumnProps> {
  public render() {
    const p = this.props

    const styles: any = []

    if (p.top) styles.marginTop = p.top

    return (
      <div style={styles} className={this._getClasses()}>
        {p.children}
      </div>
    )
  }

  private _getClasses(): string {
    return [
      'col',
      `col--${this.props.span}`,
      this.props.stubborn ? 'col--stubborn' : ''
    ]
      .filter(n => !!n)
      .join(' ')
  }
}
