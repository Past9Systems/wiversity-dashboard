import { inject, observer } from 'mobx-react'
import * as React from 'react'
import Store from '../state/Store'

export interface IOnRouteProps {
  store?: Store
  routeName: string
}

@inject('store')
@observer
export default class OnRoute extends React.Component<IOnRouteProps> {
  public render() {
    if (this.props.store!.route !== this.props.routeName) return null

    return this.props.children
  }
}
