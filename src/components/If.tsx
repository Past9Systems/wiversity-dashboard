import * as React from 'react'

export interface IIfProps {
  condition: boolean
}

export default class If extends React.Component<IIfProps> {
  public render() {
    if (this.props.condition && this.props.children) return this.props.children
    return null
  }
}
