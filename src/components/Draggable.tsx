import * as React from 'react'

import './Draggable.scss'

export interface IDraggableProps {
  isDraggable?: boolean
  margin?: string
  noDragX?: boolean
  noDragY?: boolean
  onDragStart: () => void
  onDragEnd: () => void
  contentScrollTop: number
  contentClientHeight: number
  contentOffsetTop: number
  contentScrollY: (amount: number) => void
}

export interface IDraggableState {
  dragging: boolean
  dragStart: {
    x: number
    y: number
    scrollY: number
  }
  dragCurrent: {
    x: number
    y: number
    scrollY: number
  }
}

export default class Draggable extends React.Component<
  IDraggableProps,
  IDraggableState
> {
  private static yBumber = 50

  private _element: HTMLDivElement | null
  private _upWindowListener: any = this._onPointerUp.bind(this)
  private _moveWindowListener: any = this._onPointerMove.bind(this)

  public constructor(props: IDraggableProps) {
    super(props)
    this.state = {
      dragCurrent: {
        scrollY: this.props.contentScrollTop,
        x: 0,
        y: 0
      },
      dragStart: {
        scrollY: this.props.contentScrollTop,
        x: 0,
        y: 0
      },
      dragging: false
    }
  }

  private get _isDraggable(): boolean {
    if (this.props.isDraggable === undefined) return true
    return this.props.isDraggable
  }

  public componentWillUnmount() {
    window.removeEventListener('pointerup', this._upWindowListener)
    window.removeEventListener('pointermove', this._moveWindowListener)
  }

  public componentDidUpdate(prev: IDraggableProps) {
    if (!this.state.dragging) return
    if (prev.contentScrollTop !== this.state.dragCurrent.scrollY) {
      this.setState({
        dragCurrent: {
          ...this.state.dragCurrent,
          scrollY: this.props.contentScrollTop
        }
      })
    }
  }

  private _startDrag(x: number, y: number) {
    if (!this._isDraggable) return
    if (this.state.dragging) return

    this.setState({
      dragCurrent: {
        scrollY: this.props.contentScrollTop,
        x,
        y
      },
      dragStart: {
        scrollY: this.props.contentScrollTop,
        x,
        y
      },
      dragging: true
    })
    this.props.onDragStart()

    window.addEventListener('pointerup', this._upWindowListener, {
      passive: false
    })
    window.addEventListener('pointermove', this._moveWindowListener, {
      passive: false
    })
  }

  private _moveDrag(x: number, y: number) {
    if (!this._isDraggable) return
    if (!this.state.dragging) return

    const aboveVisibleArea = this._aboveVisibleArea()
    const belowVisibleArea = this._belowVisibleArea()

    if (aboveVisibleArea > 0 && belowVisibleArea <= 0)
      this.props.contentScrollY(-aboveVisibleArea)
    else if (aboveVisibleArea <= 0 && belowVisibleArea > 0)
      this.props.contentScrollY(belowVisibleArea)

    this.setState({
      dragCurrent: {
        scrollY: this.props.contentScrollTop,
        x,
        y
      }
    })
  }

  private _stopDrag() {
    if (!this.state.dragging) return
    window.removeEventListener('pointerup', this._upWindowListener)
    window.removeEventListener('pointermove', this._moveWindowListener)
    this.setState({
      dragCurrent: { x: 0, y: 0, scrollY: 0 },
      dragStart: { x: 0, y: 0, scrollY: 0 },
      dragging: false
    })
    this.props.onDragEnd()
  }

  private _onPointerMove(e: React.PointerEvent<HTMLDivElement>) {
    this._moveDrag(e.pageX, e.pageY)
  }

  private _onPointerUp(_: React.PointerEvent<HTMLDivElement>) {
    this._stopDrag()
  }

  private _onPointerDown(e: React.PointerEvent<HTMLDivElement>) {
    this._startDrag(e.pageX, e.pageY)
    e.stopPropagation()
    e.preventDefault()
  }

  private _aboveVisibleArea(): number {
    return (
      this.props.contentOffsetTop -
      (this.state.dragCurrent.y - Draggable.yBumber)
    )
  }

  private _belowVisibleArea(): number {
    return (
      this.state.dragCurrent.y +
      Draggable.yBumber -
      (this.props.contentOffsetTop + this.props.contentClientHeight)
    )
  }

  public render() {
    const p = this.props
    const s = this.state

    const styles = {
      left:
        s.dragging && !p.noDragX
          ? `${s.dragCurrent.x - s.dragStart.x}px`
          : undefined,
      margin: this.props.margin,
      pointerEvents: (s.dragging ? 'none' : 'auto') as 'none' | 'auto',
      top:
        s.dragging && !p.noDragY
          ? `${s.dragCurrent.y -
              s.dragStart.y +
              s.dragCurrent.scrollY -
              s.dragStart.scrollY}px`
          : undefined,
      zIndex: (s.dragging ? '100' : 'auto') as number | 'auto'
    }

    return (
      <div
        ref={e => (this._element = e)}
        style={styles}
        onPointerDown={e => this._onPointerDown(e)}
        className='draggable'
      >
        {this.props.children}
      </div>
    )
  }
}
