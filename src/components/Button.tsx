import * as React from 'react'
import { Params, Router } from 'router5'
import { RouteNames } from '../Routes'

import { inject, observer } from 'mobx-react'
import './Button.scss'

export type ButtonActionTypes =
  | 'primary'
  | 'primary-outline'
  | 'primary-caption'
  | 'secondary'
  | 'secondary-outline'
  | 'secondary-caption'
  | 'success'
  | 'success-outline'
  | 'success-caption'
  | 'danger'
  | 'danger-outline'
  | 'danger-caption'
  | 'warning'
  | 'warning-outline'
  | 'warning-caption'

export interface IButtonProps {
  router?: Router
  label?: string
  tooltip?: string
  onClick?: () => void
  onMouseDown?: (e: React.MouseEvent<HTMLElement>) => void
  onPointerDown?: (e: React.PointerEvent<HTMLElement>) => void
  disabled?: boolean
  stacked?: boolean
  center?: boolean
  float?: 'left' | 'right'
  absolutePosition?: boolean
  right?: string
  left?: string
  top?: string
  bottom?: string
  actionType?: ButtonActionTypes
  outline?: boolean
  busy?: boolean
  routeName?: string
  routeParams?: Params
  margin?: string
  size?: 'normal' | 'small' | 'x-small'
}

@inject('router')
@observer
export default class Button extends React.Component<IButtonProps> {
  public render() {
    const p = this.props

    const styles = {
      bottom: this.props.bottom,
      left: this.props.left,
      margin: this.props.margin,
      right: this.props.right,
      top: this.props.top
    }

    if (!this.props.routeName)
      return (
        <button
          style={styles}
          className={this._getClasses()}
          title={this.props.tooltip}
          disabled={p.disabled}
          onClick={e => {
            if (p.disabled) return
            if (p.onClick) p.onClick()
          }}
          onMouseDown={e => {
            if (p.disabled) return
            if (p.onMouseDown) p.onMouseDown(e)
          }}
          onPointerDown={e => {
            if (p.disabled) return
            if (p.onPointerDown) p.onPointerDown(e)
          }}
        >
          {p.children || p.label}
        </button>
      )
    else {
      const routeName = this.props.routeName || RouteNames.Home
      const routeParams = this.props.routeParams || {}

      return (
        <a
          style={styles}
          className={this._getClasses(true)}
          title={this.props.tooltip}
          onClick={e => {
            e.preventDefault()

            if (p.disabled) return

            if (p.onClick) p.onClick()

            this.props.router!.navigate(routeName, routeParams)
          }}
          onMouseDown={e => {
            if (p.disabled) return
            if (p.onMouseDown) p.onMouseDown(e)
          }}
          onPointerDown={e => {
            if (p.disabled) return
            if (p.onPointerDown) p.onPointerDown(e)
          }}
          href={this.props.router!.buildUrl(routeName, routeParams)}
        >
          {p.children || p.label}
        </a>
      )
    }
  }

  private _getClasses(isLink = false): string {
    const p = this.props

    const linkClass: string = isLink ? 'button--link' : ''

    const centerClass: string = p.center ? 'button--center' : ''

    let floatClass = ''
    if (p.float === 'right') floatClass = 'button--right'
    if (p.float === 'left') floatClass = 'button--left'

    let absolutePositionClass = ''
    if (p.absolutePosition) absolutePositionClass = 'button--pos-abs'

    let sizeClass = ''
    if (p.size === 'small') sizeClass = 'button--small'
    if (p.size === 'x-small') sizeClass = 'button--x-small'

    let actionTypeClass = ''
    if (p.actionType === 'primary') actionTypeClass = 'button--primary'
    if (p.actionType === 'secondary') actionTypeClass = 'button--secondary'
    if (p.actionType === 'success') actionTypeClass = 'button--success'
    if (p.actionType === 'danger') actionTypeClass = 'button--danger'
    if (p.actionType === 'warning') actionTypeClass = 'button--warning'

    if (p.actionType === 'primary-outline')
      actionTypeClass = 'button--primary-outline'
    if (p.actionType === 'secondary-outline')
      actionTypeClass = 'button--secondary-outline'
    if (p.actionType === 'success-outline')
      actionTypeClass = 'button--success-outline'
    if (p.actionType === 'danger-outline')
      actionTypeClass = 'button--danger-outline'
    if (p.actionType === 'warning-outline')
      actionTypeClass = 'button--warning-outline'

    if (p.actionType === 'primary-caption')
      actionTypeClass = 'button--primary-caption'
    if (p.actionType === 'secondary-caption')
      actionTypeClass = 'button--secondary-caption'
    if (p.actionType === 'success-caption')
      actionTypeClass = 'button--success-caption'
    if (p.actionType === 'danger-caption')
      actionTypeClass = 'button--danger-caption'
    if (p.actionType === 'warning-caption')
      actionTypeClass = 'button--warning-caption'

    return [
      'button',
      p.busy ? 'button--busy' : '',
      p.outline ? 'button--outline' : '',
      actionTypeClass,
      linkClass,
      centerClass,
      floatClass,
      absolutePositionClass,
      sizeClass
    ]
      .filter(n => !!n)
      .join(' ')
  }
}
