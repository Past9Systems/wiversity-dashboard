import * as React from 'react'

import If from './If'

import { FontAwesomeIcon as Icon } from '@fortawesome/react-fontawesome'
import {
  checkCircleIcon,
  exclamationCircleIcon,
  infoCircleIcon,
  timesCircleSolidIcon,
  timesIcon
} from '../lib/Icons'

import { IResponseMessage } from '../api/ApiResponse'
import translate from '../lib/translate'
import './Notification.scss'

export interface INotificationProps {
  message?: string | IResponseMessage | null
  type?: 'error' | 'warning' | 'success' | 'info'
  onDismiss?: () => void
  isDismissable?: boolean
}

export default class Notification extends React.Component<INotificationProps> {
  public render() {
    const p = this.props

    const noMessage =
      p.message === undefined || p.message === null || p.message === ''
    const noChildren =
      p.children === undefined || p.children === null || p.children === []

    if (noMessage && noChildren) return null

    return (
      <div className={this._getClasses()}>
        <div className='notification__icon'>
          <If condition={p.type === 'error'}>
            <Icon icon={timesCircleSolidIcon} />
          </If>
          <If condition={p.type === 'warning'}>
            <Icon icon={exclamationCircleIcon} />
          </If>
          <If condition={p.type === 'success'}>
            <Icon icon={checkCircleIcon} />
          </If>
          <If condition={p.type === 'info'}>
            <Icon icon={infoCircleIcon} />
          </If>
        </div>
        <div className='notification__message'>
          <span dangerouslySetInnerHTML={{ __html: translate(p.message) }} />
          {p.children}
        </div>
        <If condition={!!p.isDismissable}>
          <button
            className='notification__dismiss-button'
            onClick={e => p.onDismiss && p.onDismiss()}
          >
            <Icon icon={timesIcon} />
          </button>
        </If>
      </div>
    )
  }

  private _getClasses(): string {
    return [
      'notification',
      this.props.type === 'error' ? 'notification--error' : '',
      this.props.type === 'warning' ? 'notification--warning' : '',
      this.props.type === 'success' ? 'notification--success' : '',
      this.props.type === 'info' ? 'notification--info' : ''
    ]
      .filter(n => !!n)
      .join(' ')
  }
}
