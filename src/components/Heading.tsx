import { inject } from 'mobx-react'
import * as React from 'react'
import { Params, Router } from 'router5'
import { RouteNames } from '../Routes'
import './Heading.scss'
import SlottedComponent from './SlottedComponent'

export interface IHeadingProps {
  text?: string | null
  level: 1 | 2 | 3 | 4 | 5 | 6
  loading?: boolean
  routeName?: RouteNames
  routeParams?: Params
  router?: Router
}

@inject('router')
export default class Heading extends SlottedComponent<IHeadingProps> {
  public right: React.ReactNode
  public above: React.ReactNode
  public below: React.ReactNode

  public render() {
    this.resolveSlots()

    return (
      <>
        <div className='heading'>
          {this.above}
          {this._renderHTag()}
          <div className='heading__below'>{this.below}</div>
        </div>
      </>
    )
  }

  private _renderLoadingPlaceholder() {
    if (this.props.loading && !this.props.text)
      return <div className='heading__loading-placeholder' />
    return null
  }

  private _renderHTag() {
    const p = this.props
    const classes = `heading__h heading__h--${p.level}`

    if (p.level === 1)
      return (
        <h1 className={classes}>
          {this._renderLoadingPlaceholder()}
          {this._renderText()}
        </h1>
      )
    if (p.level === 2)
      return (
        <h2 className={classes}>
          {this._renderLoadingPlaceholder()}
          {this._renderText()}
        </h2>
      )
    if (p.level === 3)
      return (
        <h3 className={classes}>
          {this._renderLoadingPlaceholder()}
          {this._renderText()}
        </h3>
      )
    if (p.level === 4)
      return (
        <h4 className={classes}>
          {this._renderLoadingPlaceholder()}
          {this._renderText()}
        </h4>
      )
    if (p.level === 5)
      return (
        <h5 className={classes}>
          {this._renderLoadingPlaceholder()}
          {this._renderText()}
        </h5>
      )
    if (p.level === 6)
      return (
        <h6 className={classes}>
          {this._renderLoadingPlaceholder()}
          {this._renderText()}
        </h6>
      )

    throw new Error(`${p.level} is not a valid Heading level`)
  }

  private _renderText() {
    if (!this.props.routeName || !this.props.routeParams) return this.props.text
    else
      return (
        <a
          className='heading__link'
          onClick={e => {
            e.preventDefault()
            this.props.router!.navigate(
              this.props.routeName!,
              this.props.routeParams!
            )
          }}
          href=''
        >
          {this.props.text}
        </a>
      )
  }
}
