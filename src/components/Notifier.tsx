import { inject, observer } from 'mobx-react'
import * as React from 'react'
import Notification from '../components/Notification'
import Store, { Status } from '../state/Store'

import './Notifier.scss'

export interface INotifierProps {
  store?: Store
}

@inject('store')
@observer
export default class Notifier extends React.Component<INotifierProps> {
  private static readonly TICK_MS = 100

  private _timerHandle: NodeJS.Timeout

  public componentDidMount() {
    const statuses = this.props.store!.app.statuses
    this._timerHandle = setInterval(function() {
      if (!statuses) return
      for (const key in statuses) {
        if (!statuses.hasOwnProperty(key)) continue
        ;(statuses[key] as Status).tick(Notifier.TICK_MS)
      }
    }, Notifier.TICK_MS)
  }

  public componentWillUnmount() {
    clearInterval(this._timerHandle)
  }

  public render() {
    const statuses = this.props.store!.app.statuses

    if (!statuses) return null

    return (
      <div className='notifier'>
        {Object.keys(statuses).map(k => {
          const ns = statuses[k] as Status

          if (!ns.isVisible || ns.type === null) return null

          return (
            <Notification
              key={k}
              type={ns.type}
              message={ns.message}
              isDismissable={true}
              onDismiss={() => ns.hide()}
            />
          )
        })}
      </div>
    )
  }
}
