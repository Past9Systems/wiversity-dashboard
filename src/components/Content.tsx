import * as React from 'react'

export interface IContentProps {
  slot: string
}

export default class Content extends React.Component<IContentProps> {}

;(Content.prototype as any).isSlotContent = true
