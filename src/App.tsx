import { inject, observer } from 'mobx-react'
import * as React from 'react'
import './App.scss'
import Notifier from './components/Notifier'
import ProgressBar from './components/ProgressBar'
import Dashboard from './pages/Dashboard'
import ForgotPasswordPage from './pages/guest/ForgotPasswordPage'
import LoginPage from './pages/guest/LoginPage'
import RegisterCreatorAccountPage from './pages/guest/RegisterCreatorAccountPage'
import ResetPasswordPage from './pages/guest/ResetPasswordPage'
import { RouteNames } from './Routes'
import Store from './state/Store'

export interface IAppProps {
  store?: Store
}

@inject('store')
@observer
export default class App extends React.Component<IAppProps> {
  public render() {
    const statuses = this.props.store!.app.statuses!
    let pending = false
    for (const key in statuses) {
      if (!statuses.hasOwnProperty(key)) continue
      pending = pending || statuses[key].pending
    }

    return (
      <>
        <ProgressBar inProgress={pending} noLayout={true} />
        <Notifier />
        {(() => {
          if (this.props.store!.route === RouteNames.RegisterCreatorAccount)
            return <RegisterCreatorAccountPage />

          if (this.props.store!.route === RouteNames.ForgotPassword)
            return <ForgotPasswordPage />

          if (this.props.store!.route === RouteNames.ResetPassword)
            return <ResetPasswordPage />

          if (this.props.store!.app.loggedIn) return <Dashboard />

          return <LoginPage />
        })()}
      </>
    )
  }
}
