export abstract class Command {
  public abstract className: string
}

export class AddCourseModule extends Command {
  public className = 'add-course-module'

  constructor(
    public courseId: string,
    public name: string,
    public position: number
  ) {
    super()
  }
}

export class AddCourseUnit extends Command {
  public className = 'add-course-unit'

  constructor(
    public courseId: string,
    public moduleId: string,
    public type: string,
    public name: string,
    public position: number
  ) {
    super()
  }
}

export class CreateCourse extends Command {
  public className = 'create-course'

  constructor(
    public schoolId: string,
    public name: string,
    public shortDescription: string,
    public longDescription: string
  ) {
    super()
  }
}

export class UpdateCourseDetails extends Command {
  public className = 'update-course-details'

  constructor(
    public courseId: string,
    public name: string,
    public shortDescription: string,
    public longDescription: string
  ) {
    super()
  }
}

export class SetCourseBackgroundImage extends Command {
  public className = 'set-course-background-image'

  constructor(public courseId: string, public file: File | null) {
    super()
  }
}

export class MoveCourseUnitToDifferentModule extends Command {
  public className = 'move-course-unit-to-different-module'

  constructor(
    public courseId: string,
    public unitId: string,
    public destModuleId: string,
    public position: number
  ) {
    super()
  }
}

export class RenameCourseModule extends Command {
  public className = 'rename-course-module'

  constructor(
    public courseId: string,
    public moduleId: string,
    public name: string
  ) {
    super()
  }
}

export class RenameCourseUnit extends Command {
  public className = 'rename-course-unit'

  constructor(
    public courseId: string,
    public unitId: string,
    public name: string
  ) {
    super()
  }
}

export class RepositionCourseModule extends Command {
  public className = 'reposition-course-module'

  constructor(
    public courseId: string,
    public moduleId: string,
    public position: number
  ) {
    super()
  }
}

export class RepositionCourseUnit extends Command {
  public className = 'reposition-course-unit'

  constructor(
    public courseId: string,
    public unitId: string,
    public position: number
  ) {
    super()
  }
}

export class SetCourseUnitVideo extends Command {
  public className = 'set-course-unit-video'

  constructor(
    public courseId: string,
    public unitId: string,
    public file: File | null
  ) {
    super()
  }
}

export class CancelCreatorEmailChange extends Command {
  public className = 'cancel-creator-email-change'

  constructor(public creatorId: string) {
    super()
  }
}

export class ChangeCreatorEmail extends Command {
  public className = 'change-creator-email'

  constructor(public creatorId: string, public email: string) {
    super()
  }
}

export class ChangeCreatorPassword extends Command {
  public className = 'change-creator-password'

  constructor(
    public creatorId: string,
    public currentPassword: string,
    public newPassword: string
  ) {
    super()
  }
}

export class RegisterCreatorAccount extends Command {
  public className = 'register-creator-account'

  constructor(
    public username: string,
    public email: string,
    public password: string
  ) {
    super()
  }
}

export class RequestCreatorPasswordResetEmail extends Command {
  public className = 'request-creator-password-reset-email'

  constructor(public username: string) {
    super()
  }
}

export class ResetCreatorPassword extends Command {
  public className = 'reset-creator-password'

  constructor(
    public creatorId: string,
    public token: string,
    public newPassword: string
  ) {
    super()
  }
}

export class VerifyCreatorEmail extends Command {
  public className = 'verify-creator-email'

  constructor(public creatorId: string, public emailHash: string) {
    super()
  }
}

export class CreateSchool extends Command {
  public className = 'create-school'

  constructor(public ownerId: string, public name: string) {
    super()
  }
}

export class RenameSchool extends Command {
  public className = 'rename-school'

  constructor(public schoolId: string, public name: string) {
    super()
  }
}

export class SetSchoolBackgroundImage extends Command {
  public className = 'set-school-background-image'

  constructor(public schoolId: string, public file: File | null) {
    super()
  }
}

export class ChangeStudentEmail extends Command {
  public className = 'change-student-email'

  constructor(public studentId: string, public email: string) {
    super()
  }
}

export class ChangeStudentPassword extends Command {
  public className = 'change-student-password'

  constructor(
    public studentId: string,
    public currentPassword: string,
    public newPassword: string
  ) {
    super()
  }
}

export class EnrollStudent extends Command {
  public className = 'enroll-student'

  constructor(
    public schoolId: string,
    public username: string,
    public email: string,
    public password: string
  ) {
    super()
  }
}
