import { Command } from './Commands'
import { VersionTag } from './Dtos'
import HttpContext from './HttpContext'

export default class ApiClient {
  public baseUrl: string

  private readonly _http: HttpContext

  public set token(val: string) {
    this._http.token = val
  }

  public set userId(val: string) {
    this._http.userId = val
  }

  public set onInvalidSession(callback: (message: string) => void) {
    this._http.onInvalidSession = callback
  }

  constructor(baseUrl: string) {
    this.baseUrl = baseUrl
    this._http = new HttpContext(this.baseUrl)
  }

  public async query<TResult>(
    query: string,
    variables: {} = {},
    versionsToAwait: VersionTag[] = []
  ): Promise<TResult> {
    return this._http.query<TResult>(query, variables, versionsToAwait)
  }

  public async command(
    command: Command,
    waitForTags = false
  ): Promise<VersionTag[]> {
    const commandName = command.className
    const anyCommand = command as any

    delete anyCommand.className

    let files: File[] = []

    if (anyCommand.file) {
      files.push(anyCommand.file)
      delete anyCommand.file
    }

    if (anyCommand.files) {
      files = files.concat(anyCommand.files)
      delete anyCommand.files
    }

    return this._http.command(commandName, command, files, waitForTags)
  }

  public async getDocument(
    documentId: string,
    accessToken: string
  ): Promise<Blob> {
    return this._http.getDocument(documentId, accessToken)
  }
}
