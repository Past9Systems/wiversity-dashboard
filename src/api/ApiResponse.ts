export class ApiResponse<T> {
  public success: boolean
  public value: T
  public error: ApiError
}

export class RequestStatus {
  public pending: boolean
  public error: ApiError | null
}

export class ApiError {
  public message: string | IResponseMessage
  public type: string
  public invalidFields: Map<string, string>

  public constructor(error: any) {
    this.message = error.message
    this.type = error.type
    this.invalidFields = error.invalidFields
  }
}

export interface IResponseMessage {
  code: string
}

export class GraphQlResponse<T> {
  public data: T
  public errors: GraphQlResponseError[]
}

export class GraphQlQueryLocation {
  public line: number
  public column: number
}

export class GraphQlApiError extends ApiError {
  public locations: GraphQlQueryLocation[]
  public path: string[]
}

export class GraphQlResponseError extends ApiError {
  public locations: GraphQlQueryLocation[]
  public path: string[]

  constructor(error: GraphQlApiError) {
    super(error)
    this.locations = error.locations
    this.path = error.path
  }
}
