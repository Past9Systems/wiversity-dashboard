import {
  ApiError,
  ApiResponse,
  GraphQlResponse,
  IResponseMessage
} from './ApiResponse'
import { VersionTag } from './Dtos'

async function post(
  url: string,
  token: string,
  data: any,
  headers: Map<string, string> | null = null,
  files: File[] | null = null
): Promise<any> {
  if (files === null) files = []

  const requestHeaders = new Headers()

  if (token) requestHeaders.append('authorization', `bearer ${token}`)

  let jsonBody = ''
  const formDataBody = new FormData()

  const isMultipart = files && files.length > 0

  if (isMultipart) {
    formDataBody.append('command', JSON.stringify(data))
    for (const file of files) formDataBody.append('files', file, file.name)
  } else {
    requestHeaders.append('content-type', 'application/json')
    jsonBody = JSON.stringify(data)
  }

  if (headers) headers.forEach((k, v) => requestHeaders.append(k, v))

  const request: RequestInit = {
    body: isMultipart ? formDataBody : jsonBody,
    headers: requestHeaders,
    method: 'POST'
  }

  let response: Response
  try {
    response = await fetch(url, request)
  } catch (e) {
    console.error(e)
    throw new Error('The server did not return a response.')
  }

  const contentType = response.headers.get('content-type')
  if (contentType && contentType.includes('application/json'))
    return response.json()

  throw new Error(
    "Could not understand response from server (content-type was not 'application/json')"
  )
}

async function postCommand(
  url: string,
  token: string,
  data: {},
  files: File[] | null = null,
  onInvalidSession: (message: string | IResponseMessage) => void
): Promise<any> {
  const response = await post(url, token, data, null, files)
  const result = (await response) as ApiResponse<VersionTag[]>
  if (result.success) {
    console.log(result.value)
    return result.value
  } else {
    console.error(`${response.status}: ${response.statusText}`)
    console.error(result.error)
    // Notify of invalid session errors
    if (result.error.type === 'Unauthorized' && onInvalidSession)
      onInvalidSession(result.error.message)
    throw new ApiError(result.error)
  }
}

async function postGraphQl<T>(
  url: string,
  token: string,
  data: {},
  onInvalidSession: (message: string | IResponseMessage) => void
): Promise<T> {
  const response = await post(url, token, data)

  const result = (await response) as GraphQlResponse<T>
  if (!result.errors || result.errors.length === 0) {
    console.log(result.data)
    return result.data
  } else {
    for (const error of result.errors) {
      console.error(error.message)
      // Notify of invalid session errors
      if (error.type === 'Unauthorized' && onInvalidSession)
        onInvalidSession(error.message)
    }
    throw result.errors[0]
  }
}

async function getDocumentBinary(
  url: string,
  accessToken: string
): Promise<Blob> {
  const requestHeaders = new Headers()

  requestHeaders.append('authorization', `bearer ${accessToken}`)
  const request: RequestInit = {
    headers: requestHeaders,
    method: 'GET'
  }

  let response: Response
  try {
    response = await fetch(url, request)
  } catch (e) {
    console.error(e)
    throw new Error('The server did not return a response')
  }

  const contentType = response.headers.get('content-type')

  if (!contentType) throw new Error('Document has no content-type header')

  return new Blob([await response.arrayBuffer()], { type: contentType })
}

export default class HttpContext {
  public baseUrl: string
  public onInvalidSession: (message: string) => void

  public token = ''
  public userId = ''

  constructor(baseUrl: string) {
    this.baseUrl = baseUrl
  }

  public async getDocument(
    documentId: string,
    accessToken: string
  ): Promise<Blob> {
    return getDocumentBinary(
      `${this.baseUrl}documents/${documentId}`,
      accessToken
    )
  }

  public async command<TRequest>(
    commandName: string,
    data: TRequest,
    files: File[] | null = null,
    waitForTags: boolean
  ): Promise<VersionTag[]> {
    const tagData = await postCommand(
      `${this.baseUrl}command/${commandName}${waitForTags ? '/await' : ''}`,
      this.token,
      data,
      files,
      this.onInvalidSession
    )
    if (!Array.isArray(tagData))
      return [
        new VersionTag(
          tagData.aggregateId,
          tagData.aggregateType,
          tagData.storeVersion
        )
      ]
    else
      return tagData.map(
        d => new VersionTag(d.aggregateId, d.aggregateType, d.storeVersion)
      )
  }

  public async query<TResult>(
    query: string,
    variables: {} = {},
    versionsToAwait: VersionTag[] = []
  ): Promise<TResult> {
    return postGraphQl<TResult>(
      `${this.baseUrl}graphql`,
      this.token,
      {
        query,
        variables,
        versionsToAwait
      },
      this.onInvalidSession
    )
  }
}
