export interface IEntity {
  id: string
}

export class VersionTag {
  public get isCourse(): boolean {
    return this.aggregateType === 'Course'
  }
  public get isCreator(): boolean {
    return this.aggregateType === 'Creator'
  }
  public get isDocument(): boolean {
    return this.aggregateType === 'Document'
  }
  public get isSchool(): boolean {
    return this.aggregateType === 'School'
  }
  public get isStudent(): boolean {
    return this.aggregateType === 'Student'
  }

  public constructor(
    public aggregateId: string,
    public aggregateType: string,
    public storeVersion: number
  ) {}
}

function mergeEntities<T extends IEntity>(
  items: T[],
  data: any,
  mergeFunc: (old: T | null, data: any) => T | null
) {
  const newList: T[] = []
  const mergeLists = getMergeLists(items, data)

  mergeLists.onlyLeft.forEach(l => newList.push(l))
  mergeLists.onlyRight.forEach(r => newList.push(r))
  mergeLists.both.forEach(b => newList.push(mergeFunc(b.left, b.right)!))

  return newList
}

export class Doc implements IEntity {
  public static create(data: any): Doc | null {
    if (data === undefined) return null
    if (data === null) return null
    const doc = new Doc()
    doc.id = getString(data.id)
    doc.mimeType = getString(data.mimeType)
    doc.publicName = getString(data.publicName)
    doc.dataAccessToken = getString(data.dataAccessToken)
    return doc
  }

  public static merge(old: Doc | null, data: any): Doc | null {
    if (data === undefined) return old
    if (data === null) return null
    if (!old) old = new Doc()
    const doc = new Doc()
    doc.id = mergeString(old.id, data.id)
    doc.mimeType = mergeString(old.mimeType, data.mimeType)
    doc.publicName = mergeString(old.publicName, data.publicName)
    doc.dataAccessToken = mergeString(old.dataAccessToken, data.dataAccessToken)
    return doc
  }

  public id: string
  public mimeType: string
  public publicName: string
  public dataAccessToken: string
}

export class Unit implements IEntity {
  public static create(data: any): Unit | null {
    if (data === undefined) return null
    if (data === null) return null
    const unit = new Unit()
    unit.id = getString(data.id)
    unit.moduleId = getString(data.moduleId)
    unit.position = getInt(data.position)
    unit.name = getString(data.name)
    unit.type = getString(data.type)
    return unit
  }

  public static merge(old: Unit | null, data: any): Unit | null {
    if (data === undefined) return old
    if (data === null) return null
    if (!old) old = new Unit()
    const unit = new Unit()
    unit.id = mergeString(old.id, data.id)
    unit.moduleId = mergeString(old.moduleId, data.moduleId)
    unit.position = mergeInt(old.position, data.position)
    unit.name = mergeString(old.name, data.name)
    unit.type = mergeString(old.type, data.type)
    return unit
  }

  public static createAll(data: any[]): Module[] {
    if (!data) return []

    return (Array.isArray(data)
      ? data.map((c: any) => Module.create(c))
      : []) as Module[]
  }

  public static mergeAll(old: Unit[], data: any) {
    return mergeEntities(old, data, (o, d) => Unit.merge(o, d))
  }

  public id = ''
  public moduleId = ''
  public position = 0
  public type = ''
  public name = ''
}

export class Module implements IEntity {
  public static create(data: any): Module | null {
    if (data === undefined) return null
    if (data === null) return null
    const mod = new Module()
    mod.id = getString(data.id)
    mod.position = getInt(data.position)
    mod.name = getString(data.name)
    mod.units = ((Array.isArray(data.units)
      ? data.units.map((c: any) => Unit.create(c))
      : []) as Unit[]).sort((a, b) => a.position - b.position)
    mod.units.sort(u => u.position)
    return mod
  }

  public static merge(old: Module | null, data: any): Module | null {
    if (data === undefined) return old
    if (data === null) return null
    if (!old) old = new Module()
    const mod = new Module()
    mod.id = mergeString(old.id, data.id)
    mod.position = mergeInt(old.position, data.position)
    mod.name = mergeString(old.name, data.name)
    mod.units = Unit.mergeAll(old.units, data.units).sort(
      (a, b) => a.position - b.position
    )
    return mod
  }

  public static createAll(data: any[]): Module[] {
    if (!data) return []

    return (Array.isArray(data)
      ? data.map((c: any) => Module.create(c))
      : []) as Module[]
  }

  public static mergeAll(old: Module[], data: any) {
    return mergeEntities(old, data, (o, d) => Module.merge(o, d))
  }

  public id = ''
  public position = 0
  public name = ''
  public units: Unit[] = []
}

export class Course implements IEntity {
  public static create(data: any): Course | null {
    if (data === undefined) return null
    if (data === null) return null
    const course = new Course()
    course.id = getString(data.id)
    course.schoolId = getString(data.schoolId)
    course.name = getString(data.name)
    course.longDescription = getString(data.longDescription)
    course.shortDescription = getString(data.shortDescription)
    course.backgroundImageId = getString(data.backgroundImageId)
    course.backgroundImage = !!data.backgroundImage
      ? Doc.create(data.backgroundImage)
      : null
    course.modules = ((Array.isArray(data.modules)
      ? data.modules.map((c: any) => Module.create(c))
      : []) as Module[]).sort((a, b) => a.position - b.position)

    // tslint:disable-next-line:no-use-before-declare
    course.school = !!data.school ? School.create(data.school) : null
    return course
  }

  public static merge(old: Course | null, data: any): Course | null {
    if (data === undefined) return old
    if (data === null) return null
    if (!old) old = new Course()
    const course = new Course()
    course.id = mergeString(old.id, data.id)
    course.schoolId = mergeString(old.schoolId, data.schoolId)
    course.name = mergeString(old.name, data.name)
    course.longDescription = mergeString(
      old.longDescription,
      data.longDescription
    )
    course.shortDescription = mergeString(
      old.shortDescription,
      data.shortDescription
    )
    course.backgroundImageId = mergeString(
      old.backgroundImageId,
      data.backgroundImageId
    )
    course.backgroundImage = Doc.merge(
      old.backgroundImage,
      data.backgroundImage
    )
    course.modules = Module.mergeAll(old.modules, data.modules).sort(
      (a, b) => a.position - b.position
    )
    // tslint:disable-next-line:no-use-before-declare
    course.school = School.merge(old.school, data.school)
    return course
  }

  public static mergeAll(old: Course[], data: any) {
    return mergeEntities(old, data, (o, d) => Course.merge(o, d))
  }

  public id: string
  public schoolId: string
  public name: string
  public longDescription: string
  public shortDescription: string
  public backgroundImageId: string
  public backgroundImage: Doc | null
  public modules: Module[] = []
  public school: School | null
}

export class School implements IEntity {
  public static create(data: any): School | null {
    if (data === undefined) return null
    if (data === null) return null
    const school = new School()
    school.id = getString(data.id)
    school.ownerId = getString(data.ownerId)
    // tslint:disable-next-line:no-use-before-declare
    school.owner = Creator.create(data.owner)
    school.name = getString(data.name)
    school.createdAt = getDate(data.createdAt)
    school.courses = Array.isArray(data.courses)
      ? data.courses.map((c: any) => Course.create(c))
      : []
    school.backgroundImageId = getString(data.backgroundImageId)
    school.backgroundImage = !!data.backgroundImage
      ? Doc.create(data.backgroundImage)
      : null
    return school
  }

  public static merge(old: School | null, data: any): School | null {
    if (data === undefined) return old
    if (data === null) return null
    if (!old) old = new School()
    const school = new School()
    school.id = mergeString(old.id, data.id)
    school.ownerId = mergeString(old.ownerId, data.ownerId)
    // tslint:disable-next-line:no-use-before-declare
    school.owner = Creator.merge(old.owner, data.owner)
    school.name = mergeString(old.name, data.name)
    school.createdAt = mergeDate(old.createdAt, data.createdAt)
    school.courses = Course.mergeAll(old.courses, data.courses)
    school.backgroundImageId = mergeString(
      old.backgroundImageId,
      data.backgroundImageId
    )
    school.backgroundImage = Doc.merge(
      old.backgroundImage,
      data.backgroundImage
    )
    return school
  }

  public static mergeAll(old: School[], data: any) {
    return mergeEntities(old, data, (o, d) => School.merge(o, d))
  }

  public id: string
  public ownerId: string
  public owner: Creator | null
  public name: string
  public createdAt: Date | null
  public courses: Course[] = []
  public backgroundImageId: string
  public backgroundImage: Doc | null
}
export class Student implements IEntity {
  public static create(data: any): Student | null {
    if (data === undefined) return null
    if (data === null) return null
    const student = new Student()
    student.id = getString(data.id)
    student.schoolId = getString(data.schoolId)
    student.username = getString(data.username)
    student.pendingEmail = getString(data.pendingEmail)
    student.email = getString(data.email)
    student.accountRegisteredAt = getDate(data.accountRegisteredAt)
    return student
  }

  public static merge(old: Student | null, data: any): Student | null {
    if (data === undefined) return old
    if (data === null) return null
    if (!old) old = new Student()
    const student = new Student()
    student.id = mergeString(old.id, data.id)
    student.schoolId = mergeString(old.schoolId, data.schoolId)
    student.username = mergeString(old.username, data.username)
    student.pendingEmail = mergeString(old.pendingEmail, data.pendingEmail)
    student.email = mergeString(old.email, data.email)
    student.accountRegisteredAt = mergeDate(
      old.accountRegisteredAt,
      data.accountRegisteredAt
    )
    return student
  }

  public id = ''
  public schoolId = ''
  public username = ''
  public pendingEmail = ''
  public email = ''
  public accountRegisteredAt: Date | null = null
}

export class Creator implements IEntity {
  public static create(data: any): Creator | null {
    if (data === undefined) return null
    if (data === null) return null
    const creator = new Creator()
    creator.id = getString(data.id)
    creator.username = getString(data.username)
    creator.pendingEmail = getString(data.pendingEmail)
    creator.email = getString(data.email)
    creator.accountRegisteredAt = getDate(data.accountRegisteredAt)
    return creator
  }

  public static merge(old: Creator | null, data: any): Creator | null {
    if (data === undefined) return old
    if (data === null) return null
    if (!old) old = new Creator()
    const creator = new Creator()
    creator.id = mergeString(old.id, data.id)
    creator.username = mergeString(old.username, data.username)
    creator.pendingEmail = mergeString(old.pendingEmail, data.pendingEmail)
    creator.email = mergeString(old.email, data.email)
    creator.accountRegisteredAt = mergeDate(
      old.accountRegisteredAt,
      data.accountRegisteredAt
    )
    return creator
  }

  public id = ''
  public username = ''
  public pendingEmail = ''
  public email = ''
  public accountRegisteredAt: Date | null = null
  public ownedSchools: School[] = []
}

function mergeString(oldVal: string, newVal: unknown): string {
  if (newVal === undefined) return oldVal
  return getString(newVal)
}

function mergeInt(oldVal: number, newVal: unknown): number {
  if (newVal === undefined) return oldVal
  return getInt(newVal)
}

function mergeFloat(oldVal: number, newVal: unknown): number {
  if (newVal === undefined) return oldVal
  return getFloat(newVal)
}

function mergeDate(oldVal: Date | null, newVal: unknown): Date | null {
  if (newVal === undefined) return oldVal
  const newDate = getDate(newVal)
  if (newDate === null) return oldVal
  return newDate
}

function getString(val: unknown): string {
  if (val === undefined) return ''
  if (val === null) return ''
  return (val as any).toString()
}

function getInt(val: unknown): number {
  if (val === undefined) return 0
  if (val === null) return 0
  return parseInt((val as any).toString(), 10)
}

function getFloat(val: unknown): number {
  if (val === undefined) return 0
  if (val === null) return 0
  return parseFloat((val as any).toString())
}

function getDate(val: unknown): Date | null {
  if (val === undefined) return null
  if (val === null) return null
  return new Date(getString(val).toString())
}

class MergeLists<T> {
  public onlyLeft: T[] = []

  public both: Array<{
    left: T
    right: T
  }> = []

  public onlyRight: T[] = []
}

function getMergeLists<T extends IEntity>(
  leftList: T[],
  rightList: T[]
): MergeLists<T> {
  const mergeLists: MergeLists<T> = new MergeLists<T>()

  if (leftList === null || leftList === undefined || leftList.length === 0) {
    mergeLists.onlyRight = rightList || []
    return mergeLists
  }

  if (rightList === null || rightList === undefined || rightList.length === 0) {
    mergeLists.onlyLeft = leftList || []
    return mergeLists
  }

  mergeLists.onlyLeft = leftList.filter(
    l => !rightList.find(r => r.id === l.id)
  )
  mergeLists.onlyRight = rightList.filter(
    r => !leftList.find(l => l.id === r.id)
  )

  for (const left of leftList) {
    const right = rightList.find(r => r.id === left.id)
    if (right) mergeLists.both.push({ left, right })
  }

  return mergeLists
}
