export default interface IConfig {
  apiBaseUrl: string
  smallDeviceMaxWidth: number
  mediumDeviceMaxWidth: number
}
