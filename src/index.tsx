/* tslint:disable:no-var-requires */
global.Promise = require('promise-polyfill')
require('whatwg-fetch')
require('ie-array-find-polyfill')
/* tslint:enable:no-var-requires */

import { Provider } from 'mobx-react'
import * as React from 'react'
import * as ReactDOM from 'react-dom'
import 'string.prototype.includes'
import App from './App'
import IConfig from './IConfig'
import './index.scss'
import { buildRouter } from './Routes'
import Store from './state/Store'

import { Router } from 'router5'
import 'typeface-roboto/index.css'
import ApiClient from './api/ApiClient'

const config: IConfig = {
  apiBaseUrl: 'http://192.168.2.6:9000/api/',
  mediumDeviceMaxWidth: 992,
  smallDeviceMaxWidth: 768
}

const api = new ApiClient(config.apiBaseUrl)
const store = new Store(api)
const router = buildRouter(store)

export interface IServices {
  config: IConfig
  api: ApiClient
  store: Store
  router: Router
}

ReactDOM.render(
  <Provider config={config} api={api} store={store} router={router}>
    <App />
  </Provider>,
  document.getElementById('root')
)
