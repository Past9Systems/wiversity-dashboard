import { action, observable } from 'mobx'
import AccountPageState from './account/AccountPageState'
import VerifyEmailPageState from './account/VerifyEmailPageState'
import CourseDetailsPageState from './courses/CourseDetailsPageState'
import CoursesInSchoolPageState from './courses/CoursesInSchoolPageState'
import CreateCoursePageState from './courses/CreateCoursePageState'
import EditCourseBackgroundPageState from './courses/EditCourseBackgroundImageState'
import EditCourseContentPageState from './courses/EditCourseContentPageState'
import EditCourseDetailsPageState from './courses/EditCourseDetailsPageState'
import EditModulePageState from './courses/EditModulePageState'
import EditUnitPageState from './courses/EditUnitPageState'
import CreateSchoolPageState from './schools/CreateSchoolPageState'
import EditSchoolBackgroundPageState from './schools/EditSchoolBackgroundPageState'
import EditSchoolPageState from './schools/EditSchoolPageState'
import SchoolDetailsPageState from './schools/SchoolDetailsPageState'
import SchoolsListPageState from './schools/SchoolsListPageState'
import { Context } from './Store'

export default class DashboardState {
  @observable
  public username: string

  @observable
  public schoolId: string

  @observable
  public menuOpen: boolean

  @observable
  public contentScrollTop: number

  @observable
  public contentClientHeight: number

  @observable
  public contentOffsetTop: number

  @observable
  public accountPage: AccountPageState

  @observable
  public verifyEmailPage: VerifyEmailPageState

  @observable
  public schoolsListPage: SchoolsListPageState

  @observable
  public createSchoolPage: CreateSchoolPageState

  @observable
  public schoolDetailsPage: SchoolDetailsPageState

  @observable
  public editSchoolPage: EditSchoolPageState

  @observable
  public editSchoolBackgroundPage: EditSchoolBackgroundPageState

  @observable
  public createCoursePage: CreateCoursePageState

  @observable
  public coursesInSchoolPage: CoursesInSchoolPageState

  @observable
  public courseDetailsPage: CourseDetailsPageState

  @observable
  public editCourseDetailsPage: EditCourseDetailsPageState

  @observable
  public editCourseBackgroundPage: EditCourseBackgroundPageState

  @observable
  public editCourseContentPage: EditCourseContentPageState

  @observable
  public editModulePage: EditModulePageState

  @observable
  public editUnitPage: EditUnitPageState

  protected context: Context

  public constructor(context: Context) {
    this.context = context
    this.accountPage = new AccountPageState(context)
    this.verifyEmailPage = new VerifyEmailPageState(context)
    this.schoolsListPage = new SchoolsListPageState(context)
    this.createSchoolPage = new CreateSchoolPageState(context)
    this.schoolDetailsPage = new SchoolDetailsPageState(context)
    this.editSchoolPage = new EditSchoolPageState(context)
    this.editSchoolBackgroundPage = new EditSchoolBackgroundPageState(context)
    this.createCoursePage = new CreateCoursePageState(context)
    this.coursesInSchoolPage = new CoursesInSchoolPageState(context)
    this.courseDetailsPage = new CourseDetailsPageState(context)
    this.editCourseDetailsPage = new EditCourseDetailsPageState(context)
    this.editCourseBackgroundPage = new EditCourseBackgroundPageState(context)
    this.editCourseContentPage = new EditCourseContentPageState(context)
    this.editModulePage = new EditModulePageState(context)
    this.editUnitPage = new EditUnitPageState(context)
  }

  @action
  public scrollBy(amount: number) {
    let y = this.contentScrollTop + amount
    if (y < 0) y = 0
    this.contentScrollTop = y
  }

  @action
  public toggleMenu(show?: boolean) {
    this.menuOpen = show === undefined ? !this.menuOpen : show
  }

  public setUsername(username: string) {
    this.username = username
  }
}
