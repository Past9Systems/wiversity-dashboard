import { action, observable } from 'mobx'
import { ResetCreatorPassword } from '../../api/Commands'
import { Context, Status } from '../Store'

export default class ResetPasswordPageState {
  @observable
  public statuses: {
    resetPassword: Status
  }

  @observable
  public creatorId: string

  @observable
  public token: string

  protected context: Context

  public constructor(context: Context) {
    this.context = context
  }

  @action
  public onWillMount() {
    this.statuses = {
      resetPassword: new Status()
    }
    this.context.root.app.statuses = this.statuses
  }

  @action
  public async resetPassword(newPassword: string) {
    this.statuses!.resetPassword.start()

    try {
      await this.context.api.command(
        new ResetCreatorPassword(this.creatorId, this.token, newPassword)
      )
      this.statuses!.resetPassword.success('PasswordChangedReturnToLogin')
    } catch (e) {
      this.statuses!.resetPassword.fail(e)
    }
  }
}
