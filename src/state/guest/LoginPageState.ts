import { action, observable } from 'mobx'
import { Context, Status } from '../Store'

export default class LoginPageState {
  @observable
  public statuses: {
    login: Status
  }

  protected context: Context

  public constructor(context: Context) {
    this.context = context
    this.context.root.app.statuses = this.statuses
  }

  @action
  public onWillMount() {
    this.statuses = {
      login: new Status()
    }
    this.context.root.app.statuses = this.statuses
  }

  @action
  public async logIn(username: string, password: string) {
    this.statuses!.login.start()

    try {
      const res = await this.context.api.query<{ creatorIdentity: string }>(
        `
        query GetCreatorByLogin($username: String, $password: String) {
          creatorIdentity(username: $username, password: $password)
        }
      `,
        { username, password }
      )

      this.context.root.app.setIdentity(res.creatorIdentity)
      this.statuses!.login.success()
    } catch (e) {
      this.statuses!.login.fail(e)
      this.context.root.app.clearIdentity()
    }
  }
}
