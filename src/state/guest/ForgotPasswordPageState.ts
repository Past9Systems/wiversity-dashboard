import { action, observable } from 'mobx'
import { RequestCreatorPasswordResetEmail } from '../../api/Commands'
import { Context, Status } from '../Store'

export default class ForgotPasswordPageState {
  @observable
  public statuses: {
    requestResetPasswordEmail: Status
  }

  protected context: Context

  public constructor(context: Context) {
    this.context = context
  }

  @action
  public onWillMount() {
    this.statuses = {
      requestResetPasswordEmail: new Status()
    }
    this.context.root.app.statuses = this.statuses
  }

  @action
  public async requestResetEmail(username: string) {
    this.statuses!.requestResetPasswordEmail.start()

    try {
      await this.context.api.command(
        new RequestCreatorPasswordResetEmail(username)
      )
      this.statuses!.requestResetPasswordEmail.success('PasswordResetEmailSent')
    } catch (e) {
      this.statuses!.requestResetPasswordEmail.fail(e)
    }
  }
}
