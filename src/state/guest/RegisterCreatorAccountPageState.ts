import { action, observable } from 'mobx'
import { RegisterCreatorAccount } from '../../api/Commands'
import { Context, Status } from '../Store'

export default class RegisterCreatorAccountPageState {
  @observable
  public statuses: {
    creatingAccount: Status
  }

  protected context: Context

  public constructor(context: Context) {
    this.context = context
  }

  public onWillMount() {
    this.statuses = {
      creatingAccount: new Status()
    }
    this.context.root.app.statuses = this.statuses
  }

  @action
  public async registerCreatorAccount(
    username: string,
    email: string,
    password: string
  ): Promise<void> {
    this.statuses!.creatingAccount.start()

    try {
      const res = await this.context.api.query<{ creatorIdentity: any }>(
        `
        query GetCreatorByLogin($username: String, $password: String) {
          creatorIdentity(username: $username, password: $password)
        }
        `,
        { username, password },
        await this.context.api.command(
          new RegisterCreatorAccount(username, email, password)
        )
      )

      this.context.root.app.setIdentity(res.creatorIdentity)
      this.statuses!.creatingAccount.success()
    } catch (e) {
      this.statuses!.creatingAccount.fail(e)
      this.context.root.app.clearIdentity()
    }
  }
}
