import { action, computed, observable, observe } from 'mobx'
import { Router, State as RouteState } from 'router5'
import ApiClient from '../api/ApiClient'
import { RouteNames } from '../Routes'
import AppState from './AppState'
import ForgotPasswordPageState from './guest/ForgotPasswordPageState'
import RegisterCreatorAccountPageState from './guest/RegisterCreatorAccountPageState'
import ResetPasswordPageState from './guest/ResetPasswordPageState'

export class Context {
  public root: Store
  public api: ApiClient
}

export class Status {
  private static readonly DURATION_MS = 500000

  @observable
  private _ticksUntilExpiration = 0

  @observable
  public pending: boolean

  @observable
  public successful: boolean

  @observable
  public successMessage?: string

  @observable
  public error?: Error

  public get hasError(): boolean {
    return !!this.error
  }

  public constructor(pending = false, error?: Error) {
    this.pending = pending
    this.error = error
  }

  @action
  public reset(): void {
    this.pending = false
    this.successful = false
    this.successMessage = undefined
    this.error = undefined
  }

  @action
  public start(): void {
    this.pending = true
    this.successful = false
    this.successMessage = undefined
    this.error = undefined
  }

  @action
  public success(message?: string): void {
    this.pending = false
    this.successful = true
    this.successMessage = message
    this.error = undefined
    this.show()
  }

  @action
  public fail(error: Error): void {
    this.pending = false
    this.successful = false
    this.successMessage = undefined
    this.error = error
    this.show()
  }

  @computed
  public get isVisible(): boolean {
    if (this._ticksUntilExpiration <= 0) return false
    if (this.message !== null) return true
    return false
  }

  @computed
  public get message(): string | null {
    if (this.successful && this.successMessage) return this.successMessage
    if (this.hasError && this.error && this.error.message)
      return this.error.message
    return null
  }

  @computed
  public get type(): 'success' | 'error' | null {
    if (this.successful) return 'success'
    if (this.hasError) return 'error'
    return null
  }

  @action
  public show() {
    this._ticksUntilExpiration = Status.DURATION_MS
  }

  @action
  public tick(duration: number) {
    this._ticksUntilExpiration -= duration
    if (this._ticksUntilExpiration < 0) this._ticksUntilExpiration = 0
  }

  @action
  public hide() {
    this._ticksUntilExpiration = 0
  }
}

export default class Store {
  public router: Router

  @observable
  public route: string

  @observable
  public app: AppState

  protected context: Context

  public constructor(api: ApiClient) {
    this.context = new Context()
    this.context.root = this

    this.app = new AppState(this.context)
    this.app.initializeState()
    api.onInvalidSession = msg => this.app.invalidSession(msg)

    this.context.api = api

    observe(this.app, change => {
      if (change.name === 'token') this.context.api.token = change.object.token

      if (change.name === 'userId')
        this.context.api.userId = change.object.userId
    })

    this.app.syncIdentity()

    setInterval(() => {
      this.app.syncIdentity()
    }, 1000)
  }

  @action
  public syncIdentity() {
    this.app.syncIdentity()
  }

  @action
  public setRoute(route: RouteState) {
    if (route === null) {
      this.route = RouteNames.Invalid
      return
    }

    this.route = route.name

    switch (route.name) {
      // Register Creator Account page
      case RouteNames.RegisterCreatorAccount:
        this.app.registerCreatorAccount = new RegisterCreatorAccountPageState(
          this.context
        )
        break

      // Forgot Password page
      case RouteNames.ForgotPassword:
        this.app.forgotPassword = new ForgotPasswordPageState(this.context)
        break

      // Reset Password page
      case RouteNames.ResetPassword:
        this.app.resetPassword = new ResetPasswordPageState(this.context)
        this.app.resetPassword.creatorId = route.params.creatorId
        this.app.resetPassword.token = route.params.token
        break

      // Home page
      case RouteNames.Home:
        break

      // Account maintenance
      case RouteNames.Account:
        break

      // Email address verification
      case RouteNames.VerifyEmail:
        this.app.dashboard.verifyEmailPage.emailHash = route.params.emailHash
        break

      // School pages
      case RouteNames.SchoolsList:
        break

      case RouteNames.CreateSchool:
        break

      case RouteNames.SchoolDetails:
        this.app.dashboard.schoolId = route.params.schoolId
        break

      case RouteNames.EditSchool:
        this.app.dashboard.schoolId = route.params.schoolId
        break

      case RouteNames.EditSchoolBackground:
        this.app.dashboard.schoolId = route.params.schoolId
        break

      // Course pages
      case RouteNames.CoursesInSchool:
        this.app.dashboard.schoolId = route.params.schoolId
        break

      case RouteNames.CreateCourse:
        this.app.dashboard.schoolId = route.params.schoolId
        break

      case RouteNames.CourseDetails:
        this.app.dashboard.schoolId = route.params.schoolId
        this.app.dashboard.courseDetailsPage.courseId = route.params.courseId
        break

      case RouteNames.EditCourseDetails:
        this.app.dashboard.schoolId = route.params.schoolId
        this.app.dashboard.editCourseDetailsPage.courseId =
          route.params.courseId
        break

      case RouteNames.EditCourseBackground:
        this.app.dashboard.schoolId = route.params.schoolId
        this.app.dashboard.editCourseBackgroundPage.courseId =
          route.params.courseId
        break

      case RouteNames.EditCourseContent:
        this.app.dashboard.schoolId = route.params.schoolId
        this.app.dashboard.editCourseContentPage.courseId =
          route.params.courseId
        break

      case RouteNames.EditModule:
        this.app.dashboard.schoolId = route.params.schoolId
        this.app.dashboard.editModulePage.courseId = route.params.courseId
        this.app.dashboard.editModulePage.moduleId = route.params.moduleId

      case RouteNames.EditUnit:
        this.app.dashboard.schoolId = route.params.schoolId
        this.app.dashboard.editUnitPage.courseId = route.params.courseId
        this.app.dashboard.editUnitPage.moduleId = route.params.moduleId
        this.app.dashboard.editUnitPage.unitId = route.params.unitId

      // Student pages
      case RouteNames.StudentsInSchoolList:
        this.app.dashboard.schoolId = route.params.schoolId
        break

      case RouteNames.StudentDetails:
        //this.app.dashboard.studentDetailsPage.schoolId = route.params.schoolId
        //this.app.dashboard.studentDetailsPage.studentId = route.params.studentId
        break

      // Invalid route
      default:
        this.route = RouteNames.Invalid
        break
    }
  }
}
