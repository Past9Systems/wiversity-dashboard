import * as Cookies from 'js-cookie'
import { action, computed, observable } from 'mobx'
import { RouteNames } from '../Routes'
import DashboardState from './DashboardState'
import ForgotPasswordPageState from './guest/ForgotPasswordPageState'
import LoginPageState from './guest/LoginPageState'
import RegisterCreatorAccountPageState from './guest/RegisterCreatorAccountPageState'
import ResetPasswordPageState from './guest/ResetPasswordPageState'
import { Context, Status } from './Store'

function decodeToken(token: string): { userId: string | null } {
  const parts = token.split('.')

  if (parts.length !== 3) return { userId: null }

  const claims = JSON.parse(atob(parts[1]))
  return {
    userId: claims.sub
  }
}

export default class AppState {
  @observable
  public invalidSessionMessage: string

  @observable
  public token: string

  @observable
  public userId: string

  @observable
  public loginPage: LoginPageState

  @observable
  public registerCreatorAccount: RegisterCreatorAccountPageState

  @observable
  public forgotPassword: ForgotPasswordPageState

  @observable
  public resetPassword: ResetPasswordPageState

  @observable
  public dashboard: DashboardState

  @observable
  public statuses: { [key: string]: Status } | null

  protected context: Context

  public constructor(context: Context) {
    this.context = context
  }

  public initializeState() {
    this.loginPage = new LoginPageState(this.context)
    this.registerCreatorAccount = new RegisterCreatorAccountPageState(
      this.context
    )
    this.forgotPassword = new ForgotPasswordPageState(this.context)
    this.resetPassword = new ResetPasswordPageState(this.context)
    this.dashboard = new DashboardState(this.context)
  }

  @action
  public invalidSession(message: string) {
    this.clearIdentity()
    this.invalidSessionMessage = message
  }

  @action
  public syncIdentity() {
    const cookieToken = Cookies.get('token')
    this.token = cookieToken !== undefined ? cookieToken : ''

    const cookieUserId = Cookies.get('userId')
    this.userId = cookieUserId !== undefined ? cookieUserId : ''
  }

  @action
  public clearIdentity() {
    this.setIdentity('')
  }

  @action
  public setIdentity(token: string) {
    if (token) {
      Cookies.set('token', token)
    } else {
      Cookies.remove('token')
    }

    const userId = decodeToken(token).userId

    if (userId) {
      Cookies.set('userId', userId)
    } else {
      Cookies.remove('userId')
    }

    this.invalidSessionMessage = ''

    this.syncIdentity()
  }

  @computed
  public get loggedIn(): boolean {
    return !!this.token && !!this.userId
  }

  @action
  public logOut() {
    this.clearIdentity()
    this.context.root.router.navigate(RouteNames.Home)

    this.initializeState()
  }
}
