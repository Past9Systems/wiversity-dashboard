import { action, observable } from 'mobx'
import {
  CancelCreatorEmailChange,
  ChangeCreatorEmail,
  ChangeCreatorPassword
} from '../../api/Commands'
import { Creator } from '../../api/Dtos'
import { Context, Status } from '../Store'

export default class AccountPageState {
  @observable
  public statuses: {
    loadingAccount: Status
    changingEmail: Status
    changingPassword: Status
  }

  @observable
  public account: Creator | null

  protected context: Context

  public constructor(context: Context) {
    this.context = context
  }

  public onWillMount() {
    this.statuses = {
      changingEmail: new Status(),
      changingPassword: new Status(),
      loadingAccount: new Status()
    }
    this.context.root.app.statuses = this.statuses
  }

  @action
  public async loadAccount(): Promise<void> {
    this.statuses.changingEmail.reset()
    this.statuses.changingPassword.reset()
    this.statuses.loadingAccount.start()

    try {
      const res = await this.context.api.query<{ creator: any }>(
        `
        query GetCreator($creatorId: String) {
          creator(id: $creatorId) {
            id
            username
            email
            pendingEmail
          }
        }
      `,
        { creatorId: this.context.root.app.userId }
      )

      this.statuses.loadingAccount.success()
      this.account = Creator.create(res.creator)
    } catch (e) {
      this.statuses.loadingAccount.fail(e)
    }
  }

  @action
  public async cancelPendingEmailChange(): Promise<void> {
    this.statuses.changingEmail.start()

    try {
      const res = await this.context.api.query<{ creator: any }>(
        `
        query GetCreator($creatorId: String) {
          creator(id: $creatorId) {
            email
            pendingEmail
          }
        }
        `,
        { creatorId: this.context.root.app.userId },
        await this.context.api.command(
          new CancelCreatorEmailChange(this.context.root.app.userId)
        )
      )

      this.statuses.changingEmail.success(
        'Pending email change has been canceled.'
      )
      this.account = Creator.merge(this.account, res.creator)
    } catch (e) {
      this.statuses.changingEmail.fail(e)
    }
  }

  @action
  public async changeEmailAddress(email: string): Promise<void> {
    this.statuses.changingEmail.start()

    try {
      const res = await this.context.api.query<{ creator: any }>(
        `
        query GetCreator($creatorId: String) {
          creator(id: $creatorId) {
            email
            pendingEmail
          }
        }
        `,
        { creatorId: this.context.root.app.userId },
        await this.context.api.command(
          new ChangeCreatorEmail(this.context.root.app.userId, email)
        )
      )

      this.account = Creator.merge(this.account, res.creator)
      this.statuses.changingEmail.success()
    } catch (e) {
      this.statuses.changingEmail.fail(e)
    }
  }

  @action
  public async changePassword(
    currentPassword: string,
    newPassword: string
  ): Promise<void> {
    this.statuses.changingPassword.start()

    try {
      await this.context.api.command(
        new ChangeCreatorPassword(
          this.context.root.app.userId,
          currentPassword,
          newPassword
        )
      )
      this.statuses.changingPassword.success('Your password has been changed.')
    } catch (e) {
      this.statuses.changingPassword.fail(e)
    }
  }
}
