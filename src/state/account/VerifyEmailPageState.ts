import { action, observable } from 'mobx'
import { VerifyCreatorEmail } from '../../api/Commands'
import { Creator } from '../../api/Dtos'
import { Context, Status } from '../Store'

export default class VerifyEmailPageState {
  @observable
  public statuses: {
    verifyingEmail: Status
  }

  @observable
  public emailHash: string

  public creator: Creator | null

  protected context: Context

  public constructor(context: Context) {
    this.context = context
  }

  public onWillMount() {
    this.statuses = {
      verifyingEmail: new Status()
    }
    this.context.root.app.statuses = this.statuses
  }

  @action
  public async verifyEmail(): Promise<void> {
    this.statuses.verifyingEmail.start()

    try {
      const res = await this.context.api.query<{ creator: any }>(
        `
        query GetCreator($creatorId: String) {
          creator(id: $creatorId) {
            email
          }
        }
        `,
        { creatorId: this.context.root.app.userId },
        await this.context.api.command(
          new VerifyCreatorEmail(this.context.root.app.userId, this.emailHash)
        )
      )

      this.creator = Creator.create(res.creator)
      this.statuses.verifyingEmail.success()
    } catch (e) {
      this.statuses.verifyingEmail.fail(e)
    }
  }
}
