import { action, observable } from 'mobx'
import { SetSchoolBackgroundImage } from '../../api/Commands'
import { School } from '../../api/Dtos'
import { IUniqueFile } from '../../components/FileInput'
import { Context, Status } from '../Store'

export default class EditSchoolBackgroundPageState {
  @observable
  public statuses: {
    loadingSchool: Status
    uploadingImage: Status
  }

  @observable
  public school: School | null

  @observable
  public files: IUniqueFile[] = []

  protected context: Context

  public constructor(context: Context) {
    this.context = context
  }

  public onWillMount() {
    this.statuses = {
      loadingSchool: new Status(),
      uploadingImage: new Status()
    }
    this.context.root.app.statuses = this.statuses
  }

  get schoolId(): string {
    return this.context.root.app.dashboard.schoolId
  }

  @action
  public async loadSchool(): Promise<void> {
    this.statuses.uploadingImage.reset()
    this.statuses.loadingSchool.start()

    try {
      const res = await this.context.api.query<{ school: any }>(
        `
        query GetSchool($schoolId: String) {
          school(id: $schoolId) {
            id
            name
            backgroundImage {
              id
              dataAccessToken
            }
          }
        }
        `,
        { schoolId: this.schoolId }
      )

      this.statuses.loadingSchool.success()
      this.school = School.create(res.school)
    } catch (e) {
      this.statuses.loadingSchool.fail(e)
    }
  }

  @action
  public setFiles(files: IUniqueFile[]) {
    this.files = files
  }

  @action
  public async uploadBackgroundImage(): Promise<void> {
    this.statuses.loadingSchool.reset()
    this.statuses.uploadingImage.start()

    try {
      const res = await this.context.api.query<{ school: any }>(
        `
        query GetSchool($schoolId: String) {
          school(id: $schoolId) {
            backgroundImage {
              id
              dataAccessToken
            }
          }
        }
        `,
        { schoolId: this.schoolId },
        await this.context.api.command(
          new SetSchoolBackgroundImage(
            this.schoolId,
            this.files.length > 0 ? this.files[0] : null
          )
        )
      )

      this.files = []
      this.statuses.uploadingImage.success()
      this.school = School.merge(this.school, res.school)
    } catch (e) {
      this.statuses.uploadingImage.fail(e)
    }
  }
}
