import { action, observable } from 'mobx'
import { School } from '../../api/Dtos'
import { Context, Status } from '../Store'

export default class SchoolsListPageState {
  @observable
  public statuses: {
    loadingSchools: Status
  }

  protected context: Context

  public constructor(context: Context) {
    this.context = context
  }

  public onWillMount() {
    this.statuses = {
      loadingSchools: new Status()
    }
    this.context.root.app.statuses = this.statuses
  }

  @action
  public async loadFirstOwnedSchool(): Promise<School | null> {
    this.statuses!.loadingSchools.pending = true
    this.statuses!.loadingSchools.error = undefined

    try {
      const res = await this.context.api.query<{ creator: any }>(
        `
        query GetSchoolsOwnedBy($ownerId: String) {
          creator(id: $ownerId) {
            ownedSchools {
              id
              createdAt
            }
          }
        }
      `,
        { ownerId: this.context.root.app.userId }
      )

      this.statuses!.loadingSchools.success()

      const schoolsByOwner = (res.creator.ownedSchools as any[])
        .map(d => School.create(d))
        .sort((s1, s2) => {
          if (!s1 || s1.createdAt === null) return 0
          if (!s2 || s2.createdAt === null) return 0
          if (s1.createdAt < s2.createdAt) return -1
          if (s1.createdAt > s2.createdAt) return 1
          return 0
        })

      if (schoolsByOwner.length > 0) return schoolsByOwner[0]
    } catch (e) {
      this.statuses!.loadingSchools.fail(e)
    }

    return null
  }
}
