import { action, observable } from 'mobx'
import { RenameSchool } from '../../api/Commands'
import { School } from '../../api/Dtos'
import { Context, Status } from '../Store'

export default class EditSchoolPageState {
  @observable
  public statuses: {
    loadingSchool: Status
    renamingSchool: Status
  }

  @observable
  public school: School | null

  protected context: Context

  public constructor(context: Context) {
    this.context = context
  }

  public onWillMount() {
    this.statuses = {
      loadingSchool: new Status(),
      renamingSchool: new Status()
    }
    this.context.root.app.statuses = this.statuses
  }

  get schoolId(): string {
    return this.context.root.app.dashboard.schoolId
  }

  @action
  public async loadSchool(): Promise<void> {
    this.statuses!.renamingSchool.reset()
    this.statuses!.loadingSchool.start()

    try {
      const res = await this.context.api.query<{ school: any }>(
        `
        query GetSchool($schoolId: String) {
          school(id: $schoolId) {
            id
            name
            backgroundImage {
              id
              dataAccessToken
            }
          }
        }
        `,
        { schoolId: this.schoolId }
      )

      this.statuses!.loadingSchool.success()
      this.school = School.create(res.school)
    } catch (e) {
      this.statuses!.loadingSchool.fail(e)
    }
  }

  @action
  public async renameSchool(name: string): Promise<void> {
    this.statuses!.loadingSchool.reset()
    this.statuses!.renamingSchool.start()

    try {
      const res = await this.context.api.query<{ school: any }>(
        `
        query GetSchool($schoolId: String) {
          school(id: $schoolId) {
            name
          }
        }
        `,
        { schoolId: this.schoolId },
        await this.context.api.command(new RenameSchool(this.schoolId, name))
      )

      this.statuses!.renamingSchool.success()
      this.school = School.merge(this.school, res.school)
    } catch (e) {
      this.statuses!.renamingSchool.fail(e)
    }
  }
}
