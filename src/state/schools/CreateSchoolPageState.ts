import { action, observable } from 'mobx'
import { CreateSchool } from '../../api/Commands'
import { School } from '../../api/Dtos'
import { Context, Status } from '../Store'

export default class CreateSchoolPageState {
  @observable
  public statuses: {
    createSchool: Status
  }

  @observable
  public school: School | null

  protected context: Context

  public constructor(context: Context) {
    this.context = context
    this.defaultState()
  }

  public onWillMount() {
    this.statuses = {
      createSchool: new Status()
    }
    this.context.root.app.statuses = this.statuses
  }

  @action
  public async createSchool(schoolName: string): Promise<void> {
    this.statuses.createSchool.start()

    try {
      const res = await this.context.api.query<{ schoolsByOwner: any[] }>(
        `
        query GetSchoolsByOwner($ownerId: String) {
          schoolsByOwner(ownerId: $ownerId) {
            id
          }
        }
        `,
        { ownerId: this.context.root.app.userId, schoolName },
        await this.context.api.command(
          new CreateSchool(this.context.root.app.userId, schoolName)
        )
      )

      this.school = School.create(res.schoolsByOwner[0])
      this.statuses.createSchool.success()
    } catch (e) {
      this.statuses.createSchool.fail(e)
    }
  }

  @action
  public defaultState() {
    this.school = null
  }
}
