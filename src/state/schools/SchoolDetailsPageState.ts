import { action, observable } from 'mobx'
import { School } from '../../api/Dtos'
import { Context, Status } from '../Store'

export default class SchoolDetailsPageState {
  @observable
  public statuses: {
    loadingSchool: Status
  }

  @observable
  public school: School | null

  protected context: Context

  public constructor(context: Context) {
    this.context = context
  }

  @action
  public onWillMount() {
    this.statuses = {
      loadingSchool: new Status()
    }
    this.context.root.app.statuses = this.statuses
  }

  @action
  public clearSchool(): void {
    this.school = null
  }

  @action
  public async loadSchool(): Promise<void> {
    this.statuses.loadingSchool.start()

    try {
      const res = await this.context.api.query<{ school: any }>(
        `
          query GetSchoolDetails($schoolId: String) {
            school(id: $schoolId) {
              id
              name
              owner {
                id
                username
              }
              courses {
                id
                schoolId
                name
              }
              backgroundImage {
                id
                dataAccessToken
              }
            }
          }
        `,
        { schoolId: this.context.root.app.dashboard.schoolId }
      )

      this.statuses.loadingSchool.success()
      this.school = School.create(res.school)
    } catch (e) {
      this.statuses.loadingSchool.fail(e)
    }
  }
}
