import { action, computed, observable } from 'mobx'
import { RenameCourseModule } from '../../api/Commands'
import { Course, Module, School } from '../../api/Dtos'
import { Context, Status } from '../Store'

export default class EditModulePageState {
  @observable
  public statuses: {
    loadingCourse: Status
    renamingModule: Status
  }

  @observable
  public course: Course | null

  @observable
  public courseId: string | null

  @observable
  public moduleId: string | null

  @computed
  public get school(): School | null {
    if (!this.course) return null
    return this.course.school
  }

  @computed
  public get module(): Module | null {
    if (!this.course) return null
    if (!this.course.modules) return null
    const mod = this.course.modules.find(m => m.id === this.moduleId)
    if (!mod) return null
    return mod
  }

  protected context: Context

  public constructor(context: Context) {
    this.context = context
  }

  public onWillMount() {
    this.statuses = {
      loadingCourse: new Status(),
      renamingModule: new Status()
    }
    this.context.root.app.statuses = this.statuses
  }

  @action
  public async loadCourse(): Promise<void> {
    this.statuses.loadingCourse.start()

    try {
      const res = await this.context.api.query<{ course: any }>(
        `
        query GetCourse($courseId: String, $moduleId: String) {
          course(id: $courseId) {
            id
            name
            school {
              id
              name
            }
            modules(id: $moduleId) {
              id
              name
            }
          }
        }
        `,
        {
          courseId: this.courseId,
          moduleId: this.moduleId
        }
      )

      this.course = Course.create(res.course)
      this.statuses.loadingCourse.success()
    } catch (e) {
      this.statuses.loadingCourse.fail(e)
    }
  }

  @action
  public async renameModule(name: string): Promise<void> {
    this.statuses.renamingModule.start()

    try {
      const res = await this.context.api.query<{ course: any }>(
        `
        query GetCourse($courseId: String, $moduleId: String) {
          course(id: $courseId) {
            modules(id: $moduleId) {
              id
              name
            }
          }
        }
        `,
        {
          courseId: this.courseId,
          moduleId: this.moduleId
        },
        await this.context.api.command(
          new RenameCourseModule(this.courseId!, this.moduleId!, name)
        )
      )

      this.course = Course.merge(this.course, res.course)
      this.statuses.renamingModule.success()
    } catch (e) {
      this.statuses.renamingModule.fail(e)
    }
  }
}
