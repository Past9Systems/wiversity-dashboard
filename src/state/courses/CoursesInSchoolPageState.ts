import { action, observable } from 'mobx'
import { School } from '../../api/Dtos'
import { Context, Status } from '../Store'

export default class CoursesInSchoolPageState {
  @observable
  public statuses: {
    loadingSchool: Status
  }

  @observable
  public school: School | null

  protected context: Context

  public constructor(context: Context) {
    this.context = context
  }

  public onWillMount() {
    this.statuses = {
      loadingSchool: new Status()
    }
    this.context.root.app.statuses = this.statuses
  }

  @action
  public async loadSchool(): Promise<void> {
    this.statuses!.loadingSchool.start()

    try {
      const res = await this.context.api.query<{ school: any }>(
        `
        query GetSchool($schoolId: String) {
          school(id: $schoolId) {
            id
            name
            courses {
              id
              name
            }
          }
        }
        `,
        { schoolId: this.context.root.app.dashboard.schoolId }
      )

      this.school = School.create(res.school)
      this.statuses!.loadingSchool.success()
    } catch (e) {
      this.statuses!.loadingSchool.fail(e)
    }
  }
}
