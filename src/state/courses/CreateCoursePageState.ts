import { action, observable } from 'mobx'
import { CreateCourse } from '../../api/Commands'
import { School } from '../../api/Dtos'
import { Context, Status } from '../Store'

export default class CreateCoursePageState {
  @observable
  public statuses: {
    loadingSchool: Status
    creatingCourse: Status
  }

  @observable
  public school: School | null

  @observable
  public courseId: string | null

  protected context: Context

  public constructor(context: Context) {
    this.context = context
  }

  public onWillMount() {
    this.statuses = {
      creatingCourse: new Status(),
      loadingSchool: new Status()
    }
    this.context.root.app.statuses = this.statuses
  }

  @action
  public resetState() {
    this.school = null
    this.courseId = null
  }

  @action
  public async loadSchool(): Promise<void> {
    this.statuses.loadingSchool.start()

    try {
      const res = await this.context.api.query<{ school: any }>(
        `
        query GetSchool($schoolId: String) {
          school(id: $schoolId) {
            name
          }
        }
        `,
        { schoolId: this.context.root.app.dashboard.schoolId }
      )

      this.school = School.create(res.school)
      this.statuses.loadingSchool.success()
    } catch (e) {
      this.statuses.loadingSchool.fail(e)
    }
  }

  @action
  public async createCourse(
    name: string,
    shortDescription: string,
    longDescription: string
  ): Promise<void> {
    this.statuses.creatingCourse.start()

    try {
      const tags = await this.context.api.command(
        new CreateCourse(
          this.context.root.app.dashboard.schoolId,
          name,
          shortDescription,
          longDescription
        ),
        true
      )
      this.courseId = tags.find(t => t.isCourse)!.aggregateId
      this.statuses.creatingCourse.success()
    } catch (e) {
      this.statuses.creatingCourse.fail(e)
    }
  }
}
