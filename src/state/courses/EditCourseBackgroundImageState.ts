import { action, observable } from 'mobx'
import { SetCourseBackgroundImage } from '../../api/Commands'
import { Course } from '../../api/Dtos'
import { IUniqueFile } from '../../components/FileInput'
import { Context, Status } from '../Store'

export default class EditCourseBackgroundPageState {
  @observable
  public statuses: {
    loadingCourse: Status
    uploadingImage: Status
  }

  @observable
  public courseId: string

  @observable
  public course: Course | null

  @observable
  public files: IUniqueFile[] = []

  protected context: Context

  public constructor(context: Context) {
    this.context = context
  }

  public onWillMount() {
    this.statuses = {
      loadingCourse: new Status(),
      uploadingImage: new Status()
    }
    this.context.root.app.statuses = this.statuses
  }

  @action
  public async loadCourse(): Promise<void> {
    this.statuses.uploadingImage.reset()
    this.statuses.loadingCourse.start()

    try {
      const res = await this.context.api.query<{ course: any }>(
        `
        query GetCourse($courseId: String) {
          course(id: $courseId) {
            id
            name
            backgroundImage {
              id
              dataAccessToken
            }
            school {
              id
              name
            }
          }
        }
        `,
        { courseId: this.courseId }
      )

      this.statuses.loadingCourse.success()
      this.course = Course.create(res.course)
    } catch (e) {
      this.statuses.loadingCourse.fail(e)
    }
  }

  @action
  public setFiles(files: IUniqueFile[]) {
    this.files = files
  }

  @action
  public async uploadBackgroundImage(): Promise<void> {
    this.statuses.loadingCourse.reset()
    this.statuses.uploadingImage.start()

    try {
      const res = await this.context.api.query<{ course: any }>(
        `
        query GetCourse($courseId: String) {
          course(id: $courseId) {
            id
            name
            backgroundImage {
              id
              dataAccessToken
            }
            school {
              id
              name
            }
          }
        }
        `,
        { courseId: this.courseId },
        await this.context.api.command(
          new SetCourseBackgroundImage(
            this.courseId,
            this.files.length > 0 ? this.files[0] : null
          )
        )
      )

      this.files = []
      this.statuses.uploadingImage.success()
      this.course = Course.merge(this.course, res.course)
    } catch (e) {
      this.statuses.uploadingImage.fail(e)
    }
  }
}
