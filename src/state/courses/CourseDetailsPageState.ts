import { action, observable } from 'mobx'
import { Course } from '../../api/Dtos'
import { Context, Status } from '../Store'

export default class CourseDetailsPageState {
  @observable
  public statuses: {
    loadingCourse: Status
  }

  @observable
  public courseId: string

  @observable
  public course: Course | null

  protected context: Context

  public constructor(context: Context) {
    this.context = context
  }

  public onWillMount() {
    this.statuses = {
      loadingCourse: new Status()
    }
    this.context.root.app.statuses = this.statuses
  }

  @action
  public clearCourse(): void {
    this.course = null
  }

  @action
  public async loadCourse(): Promise<void> {
    this.statuses!.loadingCourse.start()

    try {
      const res = await this.context.api.query<{ course: any }>(
        `
        query GetCourseDetails($courseId: String) {
          course(id: $courseId) {
            id
            name
            backgroundImage {
              id
              dataAccessToken
            }
            school {
              name
            }
          }
        }
      `,
        { courseId: this.courseId }
      )

      this.course = Course.create(res.course)
      this.statuses!.loadingCourse.success()
    } catch (e) {
      this.statuses!.loadingCourse.fail(e)
    }
  }
}
