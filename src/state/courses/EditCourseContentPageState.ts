import { action, observable } from 'mobx'
import {
  AddCourseModule,
  AddCourseUnit,
  MoveCourseUnitToDifferentModule,
  RepositionCourseModule,
  RepositionCourseUnit
} from '../../api/Commands'
import { Course } from '../../api/Dtos'
import { Context, Status } from '../Store'

export default class EditCourseContentPageState {
  @observable
  public statuses: {
    addingModule: Status
    addingUnit: Status
    loadingCourse: Status
    repositioningModule: Status
    repositioningUnit: Status
  }

  @observable
  public courseId: string

  @observable
  public course: Course | null

  protected context: Context

  public constructor(context: Context) {
    this.context = context
    this.context.root.app.statuses = this.statuses
  }

  @action
  public onWillMount() {
    this.course = null
    this.statuses = {
      addingModule: new Status(),
      addingUnit: new Status(),
      loadingCourse: new Status(),
      repositioningModule: new Status(),
      repositioningUnit: new Status()
    }
    this.context.root.app.statuses = this.statuses
  }

  @action
  public async loadCourse(): Promise<void> {
    this.statuses!.loadingCourse.start()

    try {
      const res = await this.context.api.query<{ course: any }>(
        `
        query GetCourse($courseId: String) {
          course(id: $courseId) {
            id
            name
            modules {
              id
              position
              name
              units {
                id
                moduleId
                position
                name
              }
            }
            school {
              name
            }
          }
        }
        `,
        { courseId: this.courseId }
      )

      this.course = Course.create(res.course)
      this.statuses!.loadingCourse.success()
    } catch (e) {
      this.statuses!.loadingCourse.fail(e)
    }
  }

  @action
  public async addModule(name: string, position: number): Promise<boolean> {
    this.statuses!.addingModule.start()

    try {
      const res = await this.context.api.query<{ course: any }>(
        `
        query GetCourse($courseId: String) {
          course(id: $courseId) {
            name
            modules {
              id
              position
              name
              units {
                id
                moduleId
                position
                name
              }
            }
          }
        }
        `,
        { courseId: this.courseId },
        await this.context.api.command(
          new AddCourseModule(this.courseId, name, position)
        )
      )
      this.course = Course.merge(this.course, res.course)
      this.statuses!.addingModule.success()
    } catch (e) {
      this.statuses!.addingModule.fail(e)
    }

    return this.statuses!.addingModule.successful
  }

  @action
  public async repositionModule(
    moduleId: string,
    position: number
  ): Promise<void> {
    this.statuses!.repositioningModule.start()

    try {
      const res = await this.context.api.query<{ course: any }>(
        `
          query GetCourse($courseId: String) {
            course(id: $courseId) {
              name
              modules {
                id
                position
                name
                units {
                  id
                  moduleId
                  position
                  name
                }
              }
            }
          }
        `,
        { courseId: this.courseId },
        await this.context.api.command(
          new RepositionCourseModule(this.courseId, moduleId, position)
        )
      )
      this.course = Course.merge(this.course, res.course)
      this.statuses!.repositioningModule.success()
    } catch (e) {
      this.statuses!.repositioningModule.fail(e)
    }
  }

  @action
  public async repositionUnit(
    sourceModuleId: string,
    destModuleId: string,
    unitId: string,
    position: number
  ): Promise<void> {
    this.statuses!.repositioningUnit.start()

    const command =
      destModuleId === sourceModuleId
        ? new RepositionCourseUnit(this.courseId, unitId, position)
        : new MoveCourseUnitToDifferentModule(
            this.courseId,
            unitId,
            destModuleId,
            position
          )

    try {
      const res = await this.context.api.query<{ course: any }>(
        `
        query GetCourse($courseId: String) {
          course(id: $courseId) {
            id
            name
            modules {
              id
              position
              name
              units {
                id
                moduleId
                position
                name
              }
            }
            school {
              name
            }
          }
        }
        `,
        { courseId: this.courseId },
        await this.context.api.command(command)
      )
      this.course = Course.create(res.course)
      this.statuses!.repositioningUnit.success()
    } catch (e) {
      this.statuses!.repositioningUnit.fail(e)
    }
  }

  @action
  public async addUnit(
    moduleId: string,
    name: string,
    position: number
  ): Promise<boolean> {
    this.statuses!.addingUnit.start()

    try {
      const res = await this.context.api.query<{ course: any }>(
        `
        query GetCourse($courseId: String) {
          course(id: $courseId) {
            name
            modules {
              id
              position
              name
              units {
                id
                moduleId
                position
                name
              }
            }
          }
        }
        `,
        { courseId: this.courseId },
        await this.context.api.command(
          new AddCourseUnit(this.courseId, moduleId, 'Media', name, position)
        )
      )
      this.course = Course.merge(this.course, res.course)
      this.statuses!.addingUnit.success()
    } catch (e) {
      this.statuses!.addingUnit.fail(e)
    }

    return this.statuses!.addingUnit.successful
  }
}
