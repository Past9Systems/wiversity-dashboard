import { action, computed, observable } from 'mobx'
import { RenameCourseUnit, SetCourseUnitVideo } from '../../api/Commands'
import { Course, Module, School, Unit } from '../../api/Dtos'
import { IUniqueFile } from '../../components/FileInput'
import { Context, Status } from '../Store'

export default class EditUnitPageState {
  @observable
  public statuses: {
    loadingCourse: Status
    renamingUnit: Status
    uploadingVideo: Status
  }

  @observable
  public course: Course | null

  @observable
  public courseId: string | null

  @observable
  public moduleId: string | null

  @observable
  public unitId: string | null

  @computed
  public get school(): School | null {
    if (!this.course) return null
    return this.course.school
  }

  @computed
  public get module(): Module | null {
    if (!this.course) return null
    if (!this.course.modules) return null
    const mod = this.course.modules.find(m => m.id === this.moduleId)
    if (!mod) return null
    return mod
  }

  @computed
  public get unit(): Unit | null {
    if (!this.module) return null
    if (!this.module.units) return null
    const unit = this.module.units.find(u => u.id === this.unitId)
    if (!unit) return null
    return unit
  }

  @observable
  public files: IUniqueFile[] = []

  protected context: Context

  public constructor(context: Context) {
    this.context = context
  }

  public onWillMount() {
    this.statuses = {
      loadingCourse: new Status(),
      renamingUnit: new Status(),
      uploadingVideo: new Status()
    }
    this.context.root.app.statuses = this.statuses
  }

  @action
  public async loadCourse(): Promise<void> {
    this.statuses.loadingCourse.start()

    try {
      const res = await this.context.api.query<{ course: any }>(
        `
        query GetCourse($courseId: String, $moduleId: String, $unitId: String) {
          course(id: $courseId) {
            id
            name
            school {
              id
              name
            }
            modules(id: $moduleId) {
              id
              name
              units(id: $unitId) {
                id
                name
                type
                videoId
              }
            }
          }
        }
        `,
        {
          courseId: this.courseId,
          moduleId: this.moduleId,
          unitId: this.unitId
        }
      )

      this.course = Course.create(res.course)
      this.statuses.loadingCourse.success()
    } catch (e) {
      this.statuses.loadingCourse.fail(e)
    }
  }

  @action
  public async renameUnit(name: string): Promise<void> {
    this.statuses.renamingUnit.start()

    try {
      const res = await this.context.api.query<{ course: any }>(
        `
        query GetCourse($courseId: String, $moduleId: String, $unitId: String) {
          course(id: $courseId) {
            modules(id: $moduleId) {
              id
              units(id: $unitId) {
                id
                name
                type
                videoId
              }
            }
          }
        }
        `,
        {
          courseId: this.courseId,
          moduleId: this.moduleId,
          unitId: this.unitId
        },
        await this.context.api.command(
          new RenameCourseUnit(this.courseId!, this.unitId!, name)
        )
      )

      this.course = Course.merge(this.course, res.course)
      this.statuses.renamingUnit.success()
    } catch (e) {
      this.statuses.renamingUnit.fail(e)
    }
  }

  @action
  public async uploadVideo(): Promise<void> {
    this.statuses.uploadingVideo.start()

    try {
      const res = await this.context.api.query<{ course: any }>(
        `
        query GetCourse($courseId: String, $moduleId: String, $unitId: String) {
          course(id: $courseId) {
            modules(id: $moduleId) {
              id
              units(id: $unitId) {
                id
                name
                type
                videoId
              }
            }
          }
        }
        `,
        {
          courseId: this.courseId,
          moduleId: this.moduleId,
          unitId: this.unitId
        },
        await this.context.api.command(
          new SetCourseUnitVideo(
            this.courseId!,
            this.unitId!,
            this.files.length > 0 ? this.files[0] : null
          )
        )
      )

      this.files = []
      this.statuses.uploadingVideo.success()
      this.course = Course.merge(this.course, res.course)
    } catch (e) {
      this.statuses.uploadingVideo.start()
    }
  }
}
