import { action, observable } from 'mobx'
import { UpdateCourseDetails } from '../../api/Commands'
import { Course } from '../../api/Dtos'
import { Context, Status } from '../Store'

export default class EditCourseDetailsPageState {
  @observable
  public statuses: {
    loadingCourse: Status
    updatingCourse: Status
  }

  @observable
  public course: Course | null

  @observable
  public courseId: string | null

  protected context: Context

  public constructor(context: Context) {
    this.context = context
  }

  public onWillMount() {
    this.statuses = {
      loadingCourse: new Status(),
      updatingCourse: new Status()
    }
    this.context.root.app.statuses = this.statuses
  }

  @action
  public resetState() {
    this.statuses!.loadingCourse = new Status()
    this.statuses!.updatingCourse = new Status()
    this.course = null
  }

  @action
  public async loadCourse(): Promise<void> {
    this.statuses!.loadingCourse.start()

    try {
      const res = await this.context.api.query<{ course: any }>(
        `
        query GetCourse($courseId: String) {
          course(id: $courseId) {
            name
            longDescription
            shortDescription
            school {
              id
              name
            }
          }
        }
        `,
        { courseId: this.courseId! }
      )
      this.course = Course.create(res.course)
      this.statuses!.loadingCourse.success()
    } catch (e) {
      this.statuses!.loadingCourse.fail(e)
    }
  }

  @action
  public async updateCourseDetails(
    name: string,
    shortDescription: string,
    longDescription: string
  ): Promise<void> {
    this.statuses!.updatingCourse.start()

    try {
      const res = await this.context.api.query<{ course: any }>(
        `
        query GetCourse($courseId: String) {
          course(id: $courseId) {
            name
            longDescription
            shortDescription
            school {
              id
              name
            }
          }
        }
        `,
        { courseId: this.courseId },
        await this.context.api.command(
          new UpdateCourseDetails(
            this.courseId!,
            name,
            shortDescription,
            longDescription
          )
        )
      )
      this.course = Course.merge(this.course, res.course)
      this.statuses!.updatingCourse.success()
    } catch (e) {
      this.statuses!.updatingCourse.fail(e)
    }
  }
}
