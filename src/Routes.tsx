import { createRouter, Route, Router } from 'router5'
import browserPlugin from 'router5/plugins/browser'
import Store from './state/Store'

export enum RouteNames {
  Invalid = 'INVALID',

  RegisterCreatorAccount = 'RegisterCreatorAccount',
  ForgotPassword = 'ForgotPassword',
  ResetPassword = 'ResetPassword',

  Home = 'Home',

  Account = 'Account',

  VerifyEmail = 'VerifyEmail',

  SchoolsList = 'SchoolsList',
  CreateSchool = 'CreateSchool',
  SchoolDetails = 'SchoolDetails',
  EditSchool = 'EditSchool',
  EditSchoolBackground = 'EditSchoolBackground',

  CoursesInSchool = 'CoursesInSchool',
  CreateCourse = 'CreateCourse',
  CourseDetails = 'CourseDetails',
  EditCourseDetails = 'EditCourseDetails',
  EditCourseBackground = 'EditCourseBackground',
  EditCourseContent = 'EditCourseContent',
  EditModule = 'EditModule',
  EditUnit = 'EditUnit',

  StudentsInSchoolList = 'StudentsInSchoolList',
  StudentDetails = 'StudentDetails'
}

export const routes: Route[] = [
  {
    name: RouteNames.RegisterCreatorAccount,
    path: '/register-creator-account'
  },
  { name: RouteNames.ForgotPassword, path: '/forgot-password' },
  { name: RouteNames.ResetPassword, path: '/reset-password/:creatorId/:token' },

  { name: RouteNames.Home, path: '/' },

  { name: RouteNames.Account, path: '/account' },

  { name: RouteNames.VerifyEmail, path: '/account/verify-email/:emailHash' },

  { name: RouteNames.SchoolsList, path: '/schools' },
  { name: RouteNames.CreateSchool, path: '/schools/create' },
  { name: RouteNames.SchoolDetails, path: '/schools/:schoolId' },
  { name: RouteNames.EditSchool, path: '/schools/:schoolId/edit' },
  {
    name: RouteNames.EditSchoolBackground,
    path: '/schools/:schoolId/background'
  },

  { name: RouteNames.CoursesInSchool, path: '/schools/:schoolId/courses' },
  { name: RouteNames.CreateCourse, path: '/schools/:schoolId/courses/create' },
  {
    name: RouteNames.CourseDetails,
    path: '/schools/:schoolId/courses/:courseId'
  },
  {
    name: RouteNames.EditCourseDetails,
    path: '/schools/:schoolId/courses/:courseId/edit'
  },
  {
    name: RouteNames.EditCourseBackground,
    path: '/schools/:schoolId/courses/:courseId/edit-background'
  },
  {
    name: RouteNames.EditCourseContent,
    path: '/schools/:schoolId/courses/:courseId/edit-content'
  },
  {
    name: RouteNames.EditModule,
    path: '/schools/:schoolId/courses/:courseId/modules/:moduleId/edit'
  },
  {
    name: RouteNames.EditUnit,
    path:
      '/schools/:schoolId/courses/:courseId/modules/:moduleId/units/:unitId/edit'
  },

  {
    name: RouteNames.StudentsInSchoolList,
    path: '/schools/:schoolId/students'
  },
  {
    name: RouteNames.StudentDetails,
    path: '/schools/:schoolId/students/:studentId'
  }
]

export function buildRouter(store: Store): Router {
  const router = createRouter(routes).usePlugin(browserPlugin())

  router.subscribe(s => store.setRoute(s.route))

  store.router = router

  router.start()

  store.setRoute(router.getState())

  return router
}
