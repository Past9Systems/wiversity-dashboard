import * as escapeHTML from 'escape-html'
import { IResponseMessage } from '../api/ApiResponse'

const language = navigator.language

let translations: any = {}
import(`/language/${language}.json`).then(res => {
  translations = !!res ? res : {}
})

export default function translate(
  message: IResponseMessage | string | null | undefined
): string {
  console.log('tx message', message)

  if (!!message && !language) return message.toString()

  if (typeof message === 'object') {
    const msg = message as IResponseMessage
    if (translations && translations.hasOwnProperty(msg.code)) {
      const template = translations[msg.code]
      let formatted = template

      for (const key in msg) {
        if (!msg.hasOwnProperty(key)) continue
        if (typeof msg[key] !== 'string' && typeof msg[key] !== 'number')
          continue
        if (key === 'code') continue

        let lastFormatted: string | null = null
        do {
          lastFormatted = formatted
          formatted = formatted.replace(`\$\{${key}\}`, escapeHTML(msg[key]))
        } while (lastFormatted !== formatted)
      }
      return formatted
    }
    return msg.code
  } else if (typeof message === 'string') {
    if (translations && translations.hasOwnProperty(message))
      return translations[message]
    else return message
  }

  return ''
}
