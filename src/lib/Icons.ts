import { findIconDefinition, library } from '@fortawesome/fontawesome-svg-core'

import { far as farFree } from '@fortawesome/free-regular-svg-icons'
import { fas as fasFree } from '@fortawesome/free-solid-svg-icons'
import { fas as fasPro } from '@fortawesome/pro-solid-svg-icons'

library.add(farFree, fasFree, fasPro)

export const angleRightIcon = findIconDefinition({
  iconName: 'angle-right',
  prefix: 'fas'
})
export const arrowToBottomIcon = findIconDefinition({
  iconName: 'arrow-to-bottom',
  prefix: 'fas'
})
export const arrowToTopIcon = findIconDefinition({
  iconName: 'arrow-to-top',
  prefix: 'fas'
})
export const banIcon = findIconDefinition({
  iconName: 'ban',
  prefix: 'fas'
})
export const barsIcon = findIconDefinition({
  iconName: 'bars',
  prefix: 'fas'
})
export const booksIcon = findIconDefinition({
  iconName: 'books',
  prefix: 'fas'
})
export const cameraIcon = findIconDefinition({
  iconName: 'camera',
  prefix: 'fas'
})
export const caretLeftIcon = findIconDefinition({
  iconName: 'caret-left',
  prefix: 'fas'
})
export const caretRightIcon = findIconDefinition({
  iconName: 'caret-right',
  prefix: 'fas'
})
export const chalkboardIcon = findIconDefinition({
  iconName: 'chalkboard',
  prefix: 'fas'
})
export const checkCircleIcon = findIconDefinition({
  iconName: 'check-circle',
  prefix: 'fas'
})
export const dollarSignIcon = findIconDefinition({
  iconName: 'dollar-sign',
  prefix: 'fas'
})
export const editIcon = findIconDefinition({
  iconName: 'edit',
  prefix: 'fas'
})
export const exclamationCircleIcon = findIconDefinition({
  iconName: 'exclamation-circle',
  prefix: 'fas'
})
export const fileArchiveIcon = findIconDefinition({
  iconName: 'file-archive',
  prefix: 'far'
})
export const fileAudioIcon = findIconDefinition({
  iconName: 'file-audio',
  prefix: 'far'
})
export const fileCodeIcon = findIconDefinition({
  iconName: 'file-code',
  prefix: 'far'
})
export const fileAltIcon = findIconDefinition({
  iconName: 'file-alt',
  prefix: 'far'
})
export const fileExcelIcon = findIconDefinition({
  iconName: 'file-excel',
  prefix: 'far'
})
export const fileIcon = findIconDefinition({
  iconName: 'file',
  prefix: 'far'
})
export const fileImageIcon = findIconDefinition({
  iconName: 'file-image',
  prefix: 'far'
})
export const filePdfIcon = findIconDefinition({
  iconName: 'file-pdf',
  prefix: 'far'
})
export const filePowerpointIcon = findIconDefinition({
  iconName: 'file-powerpoint',
  prefix: 'far'
})
export const fileVideoIcon = findIconDefinition({
  iconName: 'file-video',
  prefix: 'far'
})
export const fileWordIcon = findIconDefinition({
  iconName: 'file-word',
  prefix: 'far'
})
export const homeIcon = findIconDefinition({
  iconName: 'home',
  prefix: 'fas'
})
export const imageIcon = findIconDefinition({
  iconName: 'image',
  prefix: 'fas'
})
export const infoCircleIcon = findIconDefinition({
  iconName: 'info-circle',
  prefix: 'fas'
})
export const plusIcon = findIconDefinition({
  iconName: 'plus',
  prefix: 'fas'
})
export const powerOffIcon = findIconDefinition({
  iconName: 'power-off',
  prefix: 'fas'
})
export const schoolIcon = findIconDefinition({
  iconName: 'school',
  prefix: 'fas'
})
export const spinnerThirdIcon = findIconDefinition({
  iconName: 'spinner-third',
  prefix: 'fas'
})
export const timesCircleIcon = findIconDefinition({
  iconName: 'times-circle',
  prefix: 'far'
})
export const timesCircleSolidIcon = findIconDefinition({
  iconName: 'times-circle',
  prefix: 'fas'
})
export const timesIcon = findIconDefinition({
  iconName: 'times',
  prefix: 'fas'
})
export const userGraduateIcon = findIconDefinition({
  iconName: 'user-graduate',
  prefix: 'fas'
})
export const userIcon = findIconDefinition({
  iconName: 'user',
  prefix: 'fas'
})
