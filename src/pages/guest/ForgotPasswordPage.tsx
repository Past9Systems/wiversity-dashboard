import { inject, observer } from 'mobx-react'
import * as React from 'react'
import { Router } from 'router5'
import { IServices } from '../..'
import Button from '../../components/Button'
import { Card, CardBody, CardFooter, CardHeader } from '../../components/Card'
import Column from '../../components/Column'
import Heading from '../../components/Heading'
import If from '../../components/If'
import Notification from '../../components/Notification'
import ProgressBar from '../../components/ProgressBar'
import Row from '../../components/Row'
import SplashBackground from '../../components/SplashBackground'
import TextField from '../../components/TextField'
import { RouteNames } from '../../Routes'
import AppState from '../../state/AppState'
import ForgotPasswordPageState from '../../state/guest/ForgotPasswordPageState'

export interface IForgotPasswordProps {
  router?: Router
  page?: ForgotPasswordPageState
  app?: AppState
}

export interface IForgotPasswordInternalState {
  username: string
}

@inject((services: IServices) => ({
  app: services.store.app,
  page: services.store.app.forgotPassword,
  router: services.router
}))
@observer
export default class ForgotPasswordPage extends React.Component<
  IForgotPasswordProps,
  IForgotPasswordInternalState
> {
  public constructor(props: IForgotPasswordProps) {
    super(props)
    this.state = {
      username: ''
    }
  }

  public componentDidMount() {
    this.props.app!.clearIdentity()
  }

  public componentWillMount() {
    this.props.page!.onWillMount()
  }

  public onUsernameChange(username: string) {
    this.setState({ username })
  }

  public render() {
    const page = this.props.page!

    return (
      <SplashBackground>
        <Row topSpace={5}>
          <Column span={8} />
          <Column span={8}>
            <Card>
              <CardHeader>
                <Heading level={3} text='Forgot your password?' />
              </CardHeader>

              <ProgressBar
                inProgress={page.statuses.requestResetPasswordEmail.pending}
              />

              <CardBody>
                <p>
                  Tell us your username and we'll email you a link to reset it.
                </p>

                <TextField
                  label='Username'
                  value={this.state.username}
                  onChange={e => this.onUsernameChange(e)}
                  disabled={page.statuses.requestResetPasswordEmail.pending}
                  onEnter={async () => this._submitRequest()}
                  error={page.statuses.requestResetPasswordEmail.error}
                  errorField='username'
                  autoFocus={true}
                  stacked={true}
                />
              </CardBody>

              <CardFooter>
                <Button
                  label='Send me a password reset email'
                  actionType='primary'
                  onClick={async () => this._submitRequest()}
                  disabled={page.statuses.requestResetPasswordEmail.pending}
                  float='right'
                />

                <Button
                  label='Return to Login'
                  actionType='primary-caption'
                  onClick={() => this.props.router!.navigate(RouteNames.Home)}
                />
              </CardFooter>
            </Card>
          </Column>
          <Column span={8} />
        </Row>
      </SplashBackground>
    )
  }

  private async _submitRequest() {
    return this.props.page!.requestResetEmail(this.state.username)
  }
}
