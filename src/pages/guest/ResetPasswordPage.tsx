import { inject, observer } from 'mobx-react'
import * as React from 'react'
import { Router } from 'router5'
import { IServices } from '../..'
import Button from '../../components/Button'
import DashboardPage from '../../components/DashboardPage'
import Frame from '../../components/Frame'
import If from '../../components/If'
import Notification from '../../components/Notification'
import SplashBackground from '../../components/SplashBackground'
import TextField from '../../components/TextField'
import { RouteNames } from '../../Routes'
import AppState from '../../state/AppState'
import ResetPasswordPageState from '../../state/guest/ResetPasswordPageState'

export interface IResetPasswordProps {
  router?: Router
  app?: AppState
  page?: ResetPasswordPageState
}

export interface IResetPasswordInternalState {
  newPassword: string
  confirmPassword: string
}

@inject((services: IServices) => ({
  page: services.store.app.resetPassword,
  app: services.store.app,
  router: services.router
}))
@observer
export default class ResetPasswordPage extends React.Component<
  IResetPasswordProps,
  IResetPasswordInternalState
> {
  public constructor(props: IResetPasswordProps) {
    super(props)
    this.state = {
      confirmPassword: '',
      newPassword: ''
    }
  }

  public componentWillMount() {
    this.props.page!.onWillMount()
  }

  public componentDidMount() {
    this.props.app!.clearIdentity()
  }

  public render() {
    const page = this.props.page!

    return (
      <SplashBackground>
        <DashboardPage maxWidth='600px'>
          <Frame headingSize={3} heading='Reset your password'>
            <TextField
              label='New Password'
              value={this.state.newPassword}
              onChange={e => this._onNewPasswordChange(e)}
              disabled={
                page.statuses.resetPassword.pending ||
                page.statuses.resetPassword.successful
              }
              onEnter={async () => this._submitResetPassword()}
              error={page.statuses.resetPassword.error}
              errorField='password'
              autoFocus={true}
              password={true}
              stacked={true}
            />

            <TextField
              label='Confirm Password'
              value={this.state.confirmPassword}
              onChange={e => this._onConfirmPasswordChange(e)}
              disabled={
                page.statuses.resetPassword.pending ||
                page.statuses.resetPassword.successful
              }
              onEnter={async () => this._submitResetPassword()}
              error={page.statuses.resetPassword.error}
              errorField='confirmPassword'
              password={true}
              stacked={true}
            />

            <Button
              label='Change password'
              actionType='primary'
              onClick={async () => this._submitResetPassword()}
              disabled={
                page.statuses.resetPassword.pending ||
                page.statuses.resetPassword.successful
              }
              float='right'
            />

            <Button
              label='Return to Login'
              actionType='primary-caption'
              onClick={() => this.props.router!.navigate(RouteNames.Home)}
            />
          </Frame>
        </DashboardPage>
      </SplashBackground>
    )
  }

  private _onNewPasswordChange(newPassword: string) {
    this.setState({ newPassword })
  }

  private _onConfirmPasswordChange(confirmPassword: string) {
    this.setState({ confirmPassword })
  }

  private async _submitResetPassword() {
    if (this.state.newPassword !== this.state.confirmPassword) {
      this.props.page!.statuses.resetPassword.fail({
        invalidFields: {
          confirmPassword: {
            code: 'PasswordsDoNotMatch'
          },
          password: {
            code: 'PasswordsDoNotMatch'
          }
        },
        message: {
          code: 'InvalidFields'
        }
      } as any)
      return
    }

    await this.props.page!.resetPassword(this.state.newPassword)

    if (!this.props.page!.statuses.resetPassword.error) {
      this.setState({
        confirmPassword: '',
        newPassword: ''
      })
    }
  }
}
