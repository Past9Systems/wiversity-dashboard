import 'promise-polyfill'

import { inject, observer } from 'mobx-react'
import * as React from 'react'
import { Router } from 'router5'
import { IServices } from '../..'
import Button from '../../components/Button'
import { Card, CardBody, CardFooter, CardHeader } from '../../components/Card'
import Center from '../../components/Center'
import Heading from '../../components/Heading'
import Notification from '../../components/Notification'
import ProgressBar from '../../components/ProgressBar'
import Row from '../../components/Row'
import SplashBackground from '../../components/SplashBackground'
import TextField from '../../components/TextField'
import { RouteNames } from '../../Routes'
import AppState from '../../state/AppState'
import LoginPageState from '../../state/guest/LoginPageState'

export interface ILoginProps {
  router?: Router
  page?: LoginPageState
  app?: AppState
}

export interface ILoginPageInternalState {
  username: string
  password: string
}

@inject((services: IServices) => ({
  app: services.store.app,
  page: services.store.app.loginPage,
  router: services.router
}))
@observer
export default class LoginPage extends React.Component<
  ILoginProps,
  ILoginPageInternalState
> {
  public constructor(props: ILoginProps) {
    super(props)
    this.state = {
      password: 'changeme',
      username: 'RossPast9'
    }
  }

  public componentWillMount() {
    this.props.page!.onWillMount()
  }

  public onUsernameChange(username: string) {
    this.setState({ username })
  }

  public onPasswordChange(password: string) {
    this.setState({ password })
  }

  public render() {
    const page = this.props.page!

    return (
      <SplashBackground>
        <Center maxWidth='300px' top='1rem'>
          <Notification
            message={this.props.app!.invalidSessionMessage}
            type='error'
          />
        </Center>

        <Center maxWidth='300px' top='5rem'>
          <Card>
            <CardHeader>
              <Heading level={2} text='Log In' />
            </CardHeader>

            <ProgressBar inProgress={page.statuses!.login.pending} />

            <CardBody>
              <TextField
                label='Username'
                value={this.state.username}
                placeholder='Your username'
                onChange={e => this.onUsernameChange(e)}
                disabled={page.statuses!.login.pending}
                onEnter={async () => this._submitLogin()}
                autoFocus={true}
                stacked={true}
              />

              <TextField
                label='Password'
                value={this.state.password}
                placeholder='Your password'
                onChange={e => this.onPasswordChange(e)}
                disabled={page.statuses!.login.pending}
                onEnter={async () => this._submitLogin()}
                password={true}
                stacked={true}
              />
            </CardBody>

            <CardFooter>
              <Row>
                <Button
                  label='Log in'
                  actionType='primary'
                  onClick={async () => this._submitLogin()}
                  disabled={page.statuses!.login.pending}
                  float='right'
                />

                <Button
                  label='Forgot password?'
                  actionType='primary-caption'
                  onClick={() =>
                    this.props.router!.navigate(RouteNames.ForgotPassword)
                  }
                  float='left'
                />
              </Row>

              <Row topSpace={2}>
                <Button
                  label='Create an account'
                  actionType='primary-caption'
                  onClick={() =>
                    this.props.router!.navigate(
                      RouteNames.RegisterCreatorAccount
                    )
                  }
                  float='right'
                />
              </Row>
            </CardFooter>
          </Card>
        </Center>
      </SplashBackground>
    )
  }

  private async _submitLogin() {
    await this.props.page!.logIn(this.state.username, this.state.password)
  }
}
