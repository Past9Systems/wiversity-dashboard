import { inject, observer } from 'mobx-react'
import * as React from 'react'
import { Router } from 'router5'
import { IServices } from '../..'
import Button from '../../components/Button'
import { Card, CardBody, CardFooter, CardHeader } from '../../components/Card'
import Column from '../../components/Column'
import Heading from '../../components/Heading'
import Notification from '../../components/Notification'
import ProgressBar from '../../components/ProgressBar'
import Row from '../../components/Row'
import SplashBackground from '../../components/SplashBackground'
import TextField from '../../components/TextField'
import { RouteNames } from '../../Routes'
import AppState from '../../state/AppState'
import RegisterCreatorAccountPageState from '../../state/guest/RegisterCreatorAccountPageState'

export interface IRegisterCreatorAccountProps {
  router?: Router
  page?: RegisterCreatorAccountPageState
  app?: AppState
}

export interface IRegisterCreatorAccountInternalState {
  username: string
  email: string
  password: string
  confirmPassword: string
}

@inject((services: IServices) => ({
  app: services.store.app,
  page: services.store.app.registerCreatorAccount,
  router: services.router
}))
@observer
export default class RegisterCreatorAccountPage extends React.Component<
  IRegisterCreatorAccountProps,
  IRegisterCreatorAccountInternalState
> {
  public constructor(props: IRegisterCreatorAccountProps) {
    super(props)
    this.state = {
      confirmPassword: '',
      email: '',
      password: '',
      username: ''
    }
  }

  public componentWillMount() {
    this.props.page!.onWillMount()
  }

  public componentDidMount() {
    this.props.app!.clearIdentity()
  }

  public render() {
    const page = this.props.page!

    return (
      <SplashBackground>
        <Row topSpace={5}>
          <Column span={8} />
          <Column span={8}>
            <Card>
              <CardHeader>
                <Heading level={3} text='Create an Account' />
              </CardHeader>

              <ProgressBar inProgress={page.statuses.creatingAccount.pending} />

              <CardBody>
                <p>Tell us some basic info about you to get started.</p>

                <TextField
                  label='Username'
                  value={this.state.username}
                  onChange={e => this._onUsernameChange(e)}
                  disabled={page.statuses.creatingAccount.pending}
                  onEnter={async () => this._submitRequest()}
                  error={page.statuses.creatingAccount.error}
                  errorField='username'
                  autoFocus={true}
                  stacked={true}
                />

                <TextField
                  label='Email'
                  value={this.state.email}
                  onChange={e => this._onEmailChange(e)}
                  disabled={page.statuses.creatingAccount.pending}
                  onEnter={async () => this._submitRequest()}
                  error={page.statuses.creatingAccount.error}
                  errorField='email'
                  stacked={true}
                />

                <TextField
                  label='Password'
                  value={this.state.password}
                  onChange={e => this._onPasswordChange(e)}
                  disabled={page.statuses.creatingAccount.pending}
                  onEnter={async () => this._submitRequest()}
                  errorField='password'
                  error={page.statuses.creatingAccount.error}
                  password={true}
                  stacked={true}
                />

                <TextField
                  label='Confirm password'
                  value={this.state.confirmPassword}
                  onChange={e => this._onConfirmPasswordChange(e)}
                  disabled={page.statuses.creatingAccount.pending}
                  onEnter={async () => this._submitRequest()}
                  errorField='confirmPassword'
                  error={page.statuses.creatingAccount.error}
                  password={true}
                  stacked={true}
                />
              </CardBody>

              <CardFooter>
                <Button
                  label='Create account'
                  float='right'
                  actionType='primary'
                  disabled={page.statuses.creatingAccount.pending}
                  onClick={async () => this._submitRequest()}
                />

                <Button
                  label='Return to Login'
                  actionType='primary-caption'
                  onClick={() => this.props.router!.navigate(RouteNames.Home)}
                />
              </CardFooter>
            </Card>
          </Column>
          <Column span={8} />
        </Row>
      </SplashBackground>
    )
  }

  private async _submitRequest() {
    const page = this.props.page!

    if (this.state.password !== this.state.confirmPassword) {
      page.statuses.creatingAccount.fail({
        invalidFields: {
          confirmPassword: 'Passwords do not match',
          password: 'Passwords do not match'
        },
        message: 'Some fields are invalid'
      } as any)
      return
    }

    await page.registerCreatorAccount(
      this.state.username,
      this.state.email,
      this.state.password
    )

    if (!page.statuses.creatingAccount.error) {
      this.setState({
        confirmPassword: '',
        email: '',
        password: '',
        username: ''
      })

      this.props.router!.navigate(RouteNames.Home, { replace: true })
    }
  }

  private _onUsernameChange(username: string) {
    this.setState({ username })
  }

  private _onEmailChange(email: string) {
    this.setState({ email })
  }

  private _onPasswordChange(password: string) {
    this.setState({ password })
  }

  private _onConfirmPasswordChange(confirmPassword: string) {
    this.setState({ confirmPassword })
  }
}
