import { inject, observer } from 'mobx-react'
import * as React from 'react'
import { Router } from 'router5'
import CreateCoursePageState from 'src/state/courses/CreateCoursePageState'
import { IServices } from '../..'
import Button from '../../components/Button'
import DashboardPage from '../../components/DashboardPage'
import Frame from '../../components/Frame'
import TextField from '../../components/TextField'
import { RouteNames } from '../../Routes'
import DashboardState from '../../state/DashboardState'

export interface ICreateCourseProps {
  router?: Router
  page?: CreateCoursePageState
  dashboard?: DashboardState
}

export interface ICreateCoursePageInternalState {
  name: string
  shortDescription: string
  longDescription: string
}

@inject((services: IServices) => ({
  dashboard: services.store.app.dashboard,
  page: services.store.app.dashboard.createCoursePage,
  router: services.router
}))
@observer
export default class CreateCoursePage extends React.Component<
  ICreateCourseProps,
  ICreateCoursePageInternalState
> {
  public constructor(props: ICreateCourseProps) {
    super(props)
    this.state = {
      longDescription: '',
      name: '',
      shortDescription: ''
    }
  }

  public componentWillMount() {
    this.props.page!.onWillMount()
  }

  public async componentDidMount() {
    this.props.page!.resetState()
    await this.props.page!.loadSchool()
  }

  public onNameChange(name: string) {
    this.setState({ name })
  }

  public onShortDescriptionChange(shortDescription: string) {
    this.setState({ shortDescription })
  }

  public onLongDescriptionChange(longDescription: string) {
    this.setState({ longDescription })
  }

  public render() {
    const page = this.props.page!

    return (
      <DashboardPage>
        <Frame
          headingSize={2}
          heading='Create Course'
          breadcrumbs={{
            crumbs: [
              {
                routeName: RouteNames.Home,
                routeParams: {},
                text: 'Home'
              },
              {
                routeName: RouteNames.SchoolDetails,
                routeParams: { schoolId: this.props.dashboard!.schoolId },
                text: page.school && page.school.name
              },
              {
                routeName: RouteNames.CoursesInSchool,
                routeParams: { schoolId: this.props.dashboard!.schoolId },
                text: 'Courses'
              }
            ],
            current: 'Create Course',
            noMargin: true
          }}
        >
          <TextField
            label='Course name'
            value={this.state.name}
            placeholder='Enter your new course&#39;s name here'
            onChange={e => this.onNameChange(e)}
            stacked={true}
            autoFocus={true}
            disabled={
              page.statuses.loadingSchool.pending ||
              page.statuses.creatingCourse.pending
            }
            onEnter={async () => this._submitForm()}
            errorField='name'
            error={page.statuses.creatingCourse.error}
            maxWidth='600px'
          />

          <TextField
            label='Short description'
            value={this.state.shortDescription}
            placeholder='Enter a summary of your course here (or you can do this later)'
            onChange={e => this.onShortDescriptionChange(e)}
            stacked={true}
            disabled={
              page.statuses.loadingSchool.pending ||
              page.statuses.creatingCourse.pending
            }
            errorField='shortDescription'
            error={page.statuses.creatingCourse.error}
            maxWidth='600px'
            multiline={true}
            height='4rem'
          />

          <TextField
            label='Long description'
            value={this.state.longDescription}
            placeholder='Enter a detailed description of your course here (or you can do this later)'
            onChange={e => this.onLongDescriptionChange(e)}
            stacked={true}
            disabled={
              page.statuses.loadingSchool.pending ||
              page.statuses.creatingCourse.pending
            }
            errorField='longDescription'
            error={page.statuses.creatingCourse.error}
            maxWidth='600px'
            multiline={true}
            height='12rem'
          />

          <Button
            label='Create'
            actionType='primary'
            float='right'
            disabled={
              page.statuses.loadingSchool.pending ||
              page.statuses.creatingCourse.pending
            }
            onClick={async () => this._submitForm()}
          />
        </Frame>
      </DashboardPage>
    )
  }

  private async _submitForm() {
    await this.props.page!.createCourse(
      this.state.name,
      this.state.shortDescription,
      this.state.longDescription
    )
    if (!this.props.page!.courseId) return
    this.props.router!.navigate(RouteNames.CourseDetails, {
      courseId: this.props.page!.courseId,
      schoolId: this.props.dashboard!.schoolId
    })
  }
}
