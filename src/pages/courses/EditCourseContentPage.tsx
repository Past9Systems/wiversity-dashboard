import { inject, observer } from 'mobx-react'
import * as React from 'react'
import { IServices } from '../..'
import Button from '../../components/Button'
import Center from '../../components/Center'
import DashboardPage from '../../components/DashboardPage'
import Frame from '../../components/Frame'
import { RouteNames } from '../../Routes'
import EditCourseContentPageState from '../../state/courses/EditCourseContentPageState'
import DashboardState from '../../state/DashboardState'

import { FontAwesomeIcon as Icon } from '@fortawesome/react-fontawesome'
import { Module } from '../../api/Dtos'
import Content from '../../components/Content'
import Draggable from '../../components/Draggable'
import Droppable from '../../components/Droppable'
import Heading from '../../components/Heading'
import TextField from '../../components/TextField'
import { editIcon, plusIcon, timesIcon } from '../../lib/Icons'
import './EditCourseContentPage.scss'

export interface IEditCourseContentPageProps {
  page?: EditCourseContentPageState
  dashboard?: DashboardState
}

export interface IEditCourseContentInternalState {
  draggingModuleIndex: number
  draggingUnitIndex: number
  draggingUnitModuleId: string | null
  isDraggingModule: boolean
  isDraggingUnit: boolean
  newModuleName: string
  showCreateModule: boolean
  newUnitName: string
  showCreateUnitInModule: number
  overModuleDropIndex: number
  overUnitDropIndex: number
  overUnitModuleId: string | null
}

@inject((services: IServices) => ({
  dashboard: services.store.app.dashboard,
  page: services.store.app.dashboard.editCourseContentPage
}))
@observer
export default class EditCourseContentPage extends React.Component<
  IEditCourseContentPageProps,
  IEditCourseContentInternalState
> {
  public constructor(props: IEditCourseContentPageProps) {
    super(props)
    this.state = {
      draggingModuleIndex: -1,
      draggingUnitIndex: -1,
      draggingUnitModuleId: null,
      isDraggingModule: false,
      isDraggingUnit: false,
      newModuleName: '',
      newUnitName: '',
      overModuleDropIndex: -1,
      overUnitDropIndex: -1,
      overUnitModuleId: null,
      showCreateModule: false,
      showCreateUnitInModule: -1
    }
  }

  public async componentWillMount() {
    this.props.page!.onWillMount()
  }

  public async componentDidMount() {
    await this.props.page!.loadCourse()
  }

  public onNewModuleNameChange(newModuleName: string) {
    this.setState({ newModuleName })
  }

  public onNewUnitNameChange(newUnitName: string) {
    this.setState({ newUnitName })
  }

  public render() {
    const page = this.props.page!

    return (
      <DashboardPage>
        <Frame
          headingSize={3}
          heading='Edit Course Content'
          noPadding={true}
          breadcrumbs={{
            crumbs: [
              {
                routeName: RouteNames.Home,
                routeParams: {},
                text: 'Home'
              },
              {
                routeName: RouteNames.SchoolDetails,
                routeParams: { schoolId: this.props.dashboard!.schoolId },
                text:
                  page.course && page.course!.school && page.course.school!.name
              },
              {
                routeName: RouteNames.CoursesInSchool,
                routeParams: { schoolId: this.props.dashboard!.schoolId },
                text: 'Courses'
              },
              {
                routeName: RouteNames.CourseDetails,
                routeParams: {
                  courseId: page.courseId,
                  schoolId: this.props.dashboard!.schoolId
                },
                text: page.course && page.course.name
              }
            ],
            current: 'Edit Course Content',
            noMargin: true
          }}
        >
          <Content slot='aboveHeading'>
            <Button
              onClick={() =>
                this.setState({
                  showCreateModule: true,
                  showCreateUnitInModule: -1
                })
              }
              float='right'
              size='small'
              tooltip='Add module'
              actionType='secondary-caption'
            >
              <Icon icon={plusIcon} />
            </Button>
          </Content>
        </Frame>
        {this._renderNoModules()}
        <div className='course-content'>{this._renderModules()}</div>
      </DashboardPage>
    )
  }

  private _renderNoModules() {
    const page = this.props.page!

    if (!page.statuses.loadingCourse.successful) return null
    if (page.course && page.course.modules.length > 0) return null
    if (this.state.showCreateModule) return null

    return (
      <Center maxWidth='400px' top='3rem'>
        <div style={{ textAlign: 'center' }}>
          This course doesn't have any modules.
          <br />
          <br />
          <Button
            label='Create a module'
            actionType='primary'
            center={true}
            onClick={() =>
              this.setState({
                showCreateModule: true,
                showCreateUnitInModule: -1
              })
            }
          />
        </div>
      </Center>
    )
  }

  private _onUnitDragStart(moduleId: string, unitIndex: number) {
    this.setState({
      draggingModuleIndex: -1,
      draggingUnitIndex: unitIndex,
      draggingUnitModuleId: moduleId,
      isDraggingModule: false,
      isDraggingUnit: true,
      showCreateModule: false,
      showCreateUnitInModule: -1
    })
  }

  private _onUnitDragEnd() {
    this.setState({
      isDraggingUnit: false
    })
  }

  private _renderUnitDropTarget(
    destModule: Module,
    dropIndex: number,
    position: 'above' | 'below'
  ) {
    let visible = true

    if (!this.state.isDraggingUnit) visible = false
    if (this.state.draggingUnitModuleId === destModule.id) {
      if (this.state.draggingUnitIndex === dropIndex) visible = false
      if (this.state.draggingUnitIndex === dropIndex - 1) visible = false
    }

    const active =
      dropIndex === this.state.overUnitDropIndex &&
      destModule.id === this.state.overUnitModuleId

    return (
      <Droppable
        canDrop={active}
        style={{
          bottom: position === 'below' ? '-0.75rem' : undefined,
          height: '0.5rem',
          position: 'absolute',
          top: position === 'above' ? '-0.75rem' : undefined,
          width: '100%'
        }}
        onDrop={() => {
          if (!active) return
          const sourceModule = this.props.page!.course!.modules.find(
            m => m.id === this.state.draggingUnitModuleId
          )
          const unitId = sourceModule!.units[this.state.draggingUnitIndex].id
          this.props.page!.repositionUnit(
            sourceModule!.id,
            destModule.id,
            unitId,
            dropIndex
          )
        }}
        onEnter={() => {
          if (!this.state.isDraggingUnit) return
          this.setState({
            overUnitDropIndex: dropIndex,
            overUnitModuleId: destModule.id
          })
        }}
        onLeave={() => {
          if (!active) return
          this.setState({
            overUnitDropIndex: -1,
            overUnitModuleId: null
          })
        }}
      >
        <div
          className={`unit-drop-target ${
            visible ? 'unit-drop-target--visible' : ''
          } ${
            dropIndex === this.state.overUnitDropIndex &&
            destModule.id === this.state.overUnitModuleId
              ? 'unit-drop-target--active'
              : ''
          } ${position === 'above' ? 'unit-drop-target--above' : ''} ${
            position === 'below' ? 'unit-drop-target--below' : ''
          }`}
        />
      </Droppable>
    )
  }

  private _onModuleDragStart(moduleIndex: number) {
    this.setState({
      draggingModuleIndex: moduleIndex,
      draggingUnitIndex: -1,
      draggingUnitModuleId: null,
      isDraggingModule: true,
      isDraggingUnit: false,
      showCreateModule: false,
      showCreateUnitInModule: -1
    })
  }

  private _onModuleDragEnd() {
    this.setState({ isDraggingModule: false })
  }

  private _renderModuleDropTarget(
    dropIndex: number,
    position: 'above' | 'below',
    isLast = false
  ) {
    let visible = true

    if (!this.state.isDraggingModule) visible = false
    if (this.state.draggingModuleIndex === dropIndex) visible = false
    if (this.state.draggingModuleIndex === dropIndex - 1) visible = false

    const active = dropIndex === this.state.overModuleDropIndex

    return (
      <Droppable
        canDrop={active}
        style={{
          bottom: position === 'below' ? '-0.75rem' : undefined,
          height: '0.5rem',
          position: 'absolute',
          top: position === 'above' ? '-0.75rem' : undefined,
          width: '100%'
        }}
        onDrop={() => {
          if (!active) return
          const moduleId = this.props.page!.course!.modules[
            this.state.draggingModuleIndex
          ].id
          this.props.page!.repositionModule(moduleId, dropIndex)
        }}
        onEnter={() => {
          if (!this.state.isDraggingModule) return
          this.setState({
            overModuleDropIndex: dropIndex
          })
        }}
        onLeave={() => {
          if (!active) return
          this.setState({
            overModuleDropIndex: -1
          })
        }}
      >
        <div
          className={`module-drop-target ${
            visible ? 'module-drop-target--visible' : ''
          } ${active ? 'module-drop-target--active' : ''} ${
            position === 'above' ? 'module-drop-target--above' : ''
          } ${position === 'below' ? 'module-drop-target--below' : ''} ${
            isLast ? 'module-drop-target--last' : ''
          }
          `}
        />
      </Droppable>
    )
  }

  private _renderModules() {
    const p = this.props.page!
    const d = this.props.dashboard!

    if (!p.statuses.loadingCourse.successful) return null

    let moduleCount = 0
    if (p.course && p.course.modules) moduleCount = p.course.modules.length

    return (
      <>
        {p.course!.modules.map((m, moduleIndex) => {
          return (
            <div
              key={moduleIndex}
              className={`module-placeholder ${
                this.state.isDraggingModule &&
                this.state.draggingModuleIndex === moduleIndex
                  ? 'module-placeholder--dragging'
                  : ''
              }`}
            >
              {this._renderModuleDropTarget(moduleIndex, 'above')}
              <Draggable
                contentScrollTop={d.contentScrollTop}
                contentClientHeight={d.contentClientHeight}
                contentOffsetTop={d.contentOffsetTop}
                contentScrollY={y => d.scrollBy(y)}
                onDragStart={() => this._onModuleDragStart(moduleIndex)}
                onDragEnd={() => this._onModuleDragEnd()}
                isDraggable={!p.statuses.repositioningModule.pending}
                noDragX={true}
                margin='1rem 0'
              >
                <div
                  className={`module ${
                    this.state.isDraggingModule &&
                    this.state.draggingModuleIndex === moduleIndex
                      ? 'module--dragging'
                      : ''
                  } ${
                    p.statuses.repositioningModule.pending ||
                    p.statuses.repositioningUnit.pending
                      ? 'module--non-draggable'
                      : ''
                  }`}
                >
                  <div className='module__heading'>
                    <Heading text={m.name} level={4}>
                      <Content slot='above'>
                        <Button
                          tooltip='Edit module'
                          float='right'
                          size='x-small'
                          actionType='secondary-caption'
                          onPointerDown={e => e.stopPropagation()}
                          routeName={RouteNames.EditModule}
                          routeParams={{
                            courseId: p.courseId,
                            moduleId: m.id,
                            schoolId: d.schoolId
                          }}
                        >
                          <Icon icon={editIcon} />
                        </Button>
                        <Button
                          tooltip='Add unit'
                          onClick={() => {
                            console.log('click')
                            this.setState({
                              showCreateModule: false,
                              showCreateUnitInModule: moduleIndex
                            })
                          }}
                          onPointerDown={e => e.stopPropagation()}
                          float='right'
                          size='x-small'
                          actionType='secondary-caption'
                        >
                          <Icon icon={plusIcon} />
                        </Button>
                      </Content>
                    </Heading>
                  </div>

                  {m.units.map((u, unitIndex) => {
                    return (
                      <div
                        key={unitIndex}
                        className={`unit-placeholder ${
                          this.state.isDraggingUnit &&
                          this.state.draggingUnitIndex === unitIndex &&
                          this.state.draggingUnitModuleId === m.id
                            ? 'unit-placeholder--dragging'
                            : ''
                        }`}
                      >
                        {this._renderUnitDropTarget(m, unitIndex, 'above')}
                        <Draggable
                          contentScrollTop={d.contentScrollTop}
                          contentClientHeight={d.contentClientHeight}
                          contentOffsetTop={d.contentOffsetTop}
                          contentScrollY={y => d.scrollBy(y)}
                          onDragStart={() =>
                            this._onUnitDragStart(m.id, unitIndex)
                          }
                          onDragEnd={() => this._onUnitDragEnd()}
                          isDraggable={!p.statuses.repositioningUnit.pending}
                          noDragX={true}
                        >
                          <div
                            key={unitIndex}
                            className={`unit ${
                              this.state.isDraggingUnit &&
                              m.id === this.state.draggingUnitModuleId &&
                              this.state.draggingUnitIndex === unitIndex
                                ? 'unit--dragging'
                                : ''
                            } ${
                              p.statuses.repositioningModule.pending ||
                              p.statuses.repositioningUnit.pending
                                ? 'unit--non-draggable'
                                : ''
                            }`}
                          >
                            <Heading text={u.name} level={5}>
                              <Content slot='above'>
                                <Button
                                  tooltip='Edit unit'
                                  float='right'
                                  size='x-small'
                                  actionType='secondary-caption'
                                  onPointerDown={e => e.stopPropagation()}
                                  routeName={RouteNames.EditUnit}
                                  routeParams={{
                                    courseId: p.courseId,
                                    moduleId: m.id,
                                    unitId: u.id,
                                    schoolId: d.schoolId
                                  }}
                                >
                                  <Icon icon={editIcon} />
                                </Button>
                              </Content>
                            </Heading>
                          </div>
                        </Draggable>
                      </div>
                    )
                  })}
                  {this._renderUnitDropTarget(m, m.units.length, 'below')}
                  {this._renderCreateUnit(moduleIndex)}
                </div>
              </Draggable>
            </div>
          )
        })}
        {this._renderModuleDropTarget(moduleCount, 'below', true)}
        {this._renderCreateModule()}
      </>
    )
  }

  private _renderCreateUnit(moduleIndex: number) {
    if (this.state.showCreateUnitInModule !== moduleIndex) return

    console.log('show create unit')

    const page = this.props.page!

    return (
      <div className='create-unit' onPointerDown={e => e.stopPropagation()}>
        <div className='create-unit__textfield'>
          <TextField
            autoFocus={true}
            marginTop='0.1rem'
            marginBottom='0.1rem'
            placeholder='Unit name'
            float='left'
            onChange={e => this.onNewUnitNameChange(e)}
            onEnter={async () => this._addUnit()}
            errorField='name'
            error={page.statuses.addingUnit.error}
            width='100%'
          />
        </div>
        <div className='create-unit__buttons'>
          <Button
            tooltip='Cancel'
            actionType='secondary-caption'
            float='right'
            onClick={() => this.setState({ showCreateUnitInModule: -1 })}
            size='small'
          >
            <Icon icon={timesIcon} />
          </Button>
          <Button
            actionType='primary'
            label='Create'
            float='right'
            margin='0'
            onClick={async () => this._addUnit()}
            size='small'
          />
        </div>
      </div>
    )
  }

  private _renderCreateModule() {
    if (!this.state.showCreateModule) return null

    const page = this.props.page!

    return (
      <div className='create-module'>
        <div className='create-module__textfield'>
          <TextField
            autoFocus={true}
            marginTop='0.1rem'
            marginBottom='0.1rem'
            placeholder='Module name'
            float='left'
            onChange={e => this.onNewModuleNameChange(e)}
            onEnter={async () => this._addModule()}
            errorField='name'
            error={page.statuses.addingModule.error}
            width='100%'
          />
        </div>
        <div className='create-module__buttons'>
          <Button
            tooltip='Cancel'
            actionType='secondary-caption'
            float='right'
            onClick={() => this.setState({ showCreateModule: false })}
          >
            <Icon icon={timesIcon} />
          </Button>
          <Button
            actionType='primary'
            label='Create'
            float='right'
            margin='0'
            onClick={async () => this._addModule()}
          />
        </div>
      </div>
    )
  }

  private async _addModule() {
    const success = await this.props.page!.addModule(
      this.state.newModuleName,
      this.props.page!.course!.modules.length
    )
    if (success)
      this.setState({
        newModuleName: '',
        showCreateModule: false
      })
  }

  private async _addUnit() {
    const mod = this.props.page!.course!.modules[
      this.state.showCreateUnitInModule
    ]
    const success = await this.props.page!.addUnit(
      mod.id,
      this.state.newUnitName,
      mod.units.length
    )

    if (success)
      this.setState({
        newUnitName: '',
        showCreateUnitInModule: -1
      })
  }
}
