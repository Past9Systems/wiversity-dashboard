import { inject, observer } from 'mobx-react'
import * as React from 'react'
import { IServices } from '../..'
import Button from '../../components/Button'
import Content from '../../components/Content'
import DashboardPage from '../../components/DashboardPage'
import Frame from '../../components/Frame'
import TextField from '../../components/TextField'
import { RouteNames } from '../../Routes'
import EditUnitPageState from '../../state/courses/EditUnitPageState'
import DashboardState from '../../state/DashboardState'

export interface IEditUnitPageProps {
  page?: EditUnitPageState
  dashboard?: DashboardState
}

export interface IEditUnitPageInternalState {
  name: string
}

@inject((services: IServices) => ({
  dashboard: services.store.app.dashboard,
  page: services.store.app.dashboard.editUnitPage
}))
@observer
export default class EditUnitPage extends React.Component<
  IEditUnitPageProps,
  IEditUnitPageInternalState
> {
  public constructor(props: IEditUnitPageProps) {
    super(props)
    this.state = { name: '' }
  }

  public async componentWillMount() {
    this.props.page!.onWillMount()
  }

  public async componentDidMount() {
    await this.props.page!.loadCourse()

    if (!this.props.page!.unit) return

    this.setState({
      name: this.props.page!.unit!.name
    })
  }

  public render() {
    const p = this.props.page!
    const d = this.props.dashboard!

    return (
      <DashboardPage>
        <Frame
          headingSize={3}
          heading={p.unit && p.unit.name}
          breadcrumbs={{
            crumbs: [
              {
                routeName: RouteNames.Home,
                routeParams: {},
                text: 'Home'
              },
              {
                routeName: RouteNames.SchoolDetails,
                routeParams: { schoolId: this.props.dashboard!.schoolId },
                text: p.course && p.course!.school && p.course.school!.name
              },
              {
                routeName: RouteNames.CoursesInSchool,
                routeParams: { schoolId: this.props.dashboard!.schoolId },
                text: 'Courses'
              },
              {
                routeName: RouteNames.EditCourseContent,
                routeParams: {
                  courseId: p.courseId,
                  schoolId: this.props.dashboard!.schoolId
                },
                text: p.course && p.course.name
              }
            ],
            current: p.unit && p.unit.name,
            noMargin: true
          }}
        >
          <TextField
            label='Unit name'
            value={this.state.name}
            onChange={e => this._onNameChanged(e)}
            disabled={
              p.statuses.loadingCourse.pending ||
              p.statuses.renamingUnit.pending
            }
            onEnter={async () => this._submitRenameUnit()}
            errorField='name'
            error={p.statuses.renamingUnit.error}
            maxWidth='600px'
            stacked={true}
          />

          <Content slot='footer'>
            <Button
              label='Save'
              float='right'
              actionType='primary'
              disabled={
                p.statuses.loadingCourse.pending ||
                p.statuses.renamingUnit.pending
              }
              onClick={async () => this._submitRenameUnit()}
            />
          </Content>
        </Frame>
      </DashboardPage>
    )
  }

  private _onNameChanged(name: string) {
    this.setState({ name })
  }

  private async _submitRenameUnit() {
    const p = this.props.page!

    await p.renameUnit(this.state.name)
    if (p.statuses.renamingUnit.hasError) return
    if (!p.unit) return

    this.setState({ name: p.unit.name })
  }

  private async _uploadContent() {
    const p = this.props.page!
  }
}
