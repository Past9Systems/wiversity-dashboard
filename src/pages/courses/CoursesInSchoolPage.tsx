import * as React from 'react'
import Button from '../../components/Button'
import Center from '../../components/Center'
import Content from '../../components/Content'
import DashboardPage from '../../components/DashboardPage'
import Frame from '../../components/Frame'
import { RouteNames } from '../../Routes'
import CoursesInSchoolPageState from '../../state/courses/CoursesInSchoolPageState'

import { FontAwesomeIcon as Icon } from '@fortawesome/react-fontawesome'
import { inject, observer } from 'mobx-react'
import { IServices } from '../..'
import Column from '../../components/Column'
import CourseSummary from '../../components/CourseSummary'
import { plusIcon } from '../../lib/Icons'
import DashboardState from '../../state/DashboardState'

export interface ICoursesInSchoolProps {
  page?: CoursesInSchoolPageState
  dashboard?: DashboardState
}

@inject((services: IServices) => ({
  dashboard: services.store.app.dashboard,
  page: services.store.app.dashboard.coursesInSchoolPage
}))
@observer
export default class CoursesInSchoolPage extends React.Component<
  ICoursesInSchoolProps
> {
  public componentWillMount() {
    this.props.page!.onWillMount()
  }

  public async componentDidMount() {
    await this.props.page!.loadSchool()
  }

  public render() {
    const page = this.props.page!

    return (
      <DashboardPage>
        <Frame
          headingSize={2}
          heading='Courses'
          breadcrumbs={{
            crumbs: [
              {
                routeName: RouteNames.Home,
                routeParams: {},
                text: 'Home'
              },
              {
                routeName: RouteNames.SchoolDetails,
                routeParams: { schoolId: this.props.dashboard!.schoolId },
                text: page.school && page.school.name
              }
            ],
            current: 'Courses',
            noMargin: true
          }}
        >
          <Content slot='aboveHeading'>
            <Button
              tooltip='Add a new course'
              actionType='secondary-caption'
              routeName={RouteNames.CreateCourse}
              routeParams={{ schoolId: this.props.dashboard!.schoolId }}
              float='right'
              size='small'
            >
              <Icon icon={plusIcon} />
            </Button>
          </Content>
        </Frame>
        {this._renderNoCourses()}
        {this._renderCourses()}
      </DashboardPage>
    )
  }

  private _renderNoCourses() {
    const page = this.props.page!

    if (!page.statuses.loadingSchool.successful) return null
    if (page.school && page.school.courses.length > 0) return null

    return (
      <Center maxWidth='400px' top='3rem'>
        <div style={{ textAlign: 'center' }}>
          You don't have any courses.
          <br />
          <br />
          <Button
            label='Create a course'
            actionType='primary'
            center={true}
            routeName={RouteNames.CreateCourse}
            routeParams={{ schoolId: this.props.dashboard!.schoolId }}
          />
        </div>
      </Center>
    )
  }

  private _renderCourses() {
    const page = this.props.page!

    if (!page.statuses.loadingSchool.successful) return null
    if (page.school && page.school.courses.length === 0) return null

    return page.school!.courses.map((c, i) => (
      <Column span={8} key={i}>
        <CourseSummary course={c} key={i} />
      </Column>
    ))
  }
}
