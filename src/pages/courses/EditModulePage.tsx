import { inject, observer } from 'mobx-react'
import * as React from 'react'
import { IServices } from '../..'
import Button from '../../components/Button'
import Content from '../../components/Content'
import DashboardPage from '../../components/DashboardPage'
import Frame from '../../components/Frame'
import TextField from '../../components/TextField'
import { RouteNames } from '../../Routes'
import EditModulePageState from '../../state/courses/EditModulePageState'
import DashboardState from '../../state/DashboardState'

export interface IEditModulePageProps {
  page?: EditModulePageState
  dashboard?: DashboardState
}

export interface IEditModulePageInternalState {
  name: string
}

@inject((services: IServices) => ({
  dashboard: services.store.app.dashboard,
  page: services.store.app.dashboard.editModulePage
}))
@observer
export default class EditModulePage extends React.Component<
  IEditModulePageProps,
  IEditModulePageInternalState
> {
  public constructor(props: IEditModulePageProps) {
    super(props)
    this.state = { name: '' }
  }

  public async componentWillMount() {
    this.props.page!.onWillMount()
  }

  public async componentDidMount() {
    await this.props.page!.loadCourse()

    if (!this.props.page!.module) return

    this.setState({
      name: this.props.page!.module!.name
    })
  }

  public render() {
    const p = this.props.page!
    const d = this.props.dashboard!

    return (
      <DashboardPage>
        <Frame
          headingSize={3}
          heading={p.module && p.module.name}
          breadcrumbs={{
            crumbs: [
              {
                routeName: RouteNames.Home,
                routeParams: {},
                text: 'Home'
              },
              {
                routeName: RouteNames.SchoolDetails,
                routeParams: { schoolId: this.props.dashboard!.schoolId },
                text: p.course && p.course!.school && p.course.school!.name
              },
              {
                routeName: RouteNames.CoursesInSchool,
                routeParams: { schoolId: this.props.dashboard!.schoolId },
                text: 'Courses'
              },
              {
                routeName: RouteNames.EditCourseContent,
                routeParams: {
                  courseId: p.courseId,
                  schoolId: this.props.dashboard!.schoolId
                },
                text: p.course && p.course.name
              }
            ],
            current: p.module && p.module.name,
            noMargin: true
          }}
        >
          <TextField
            label='Module name'
            value={this.state.name}
            onChange={e => this._onNameChanged(e)}
            disabled={
              p.statuses.loadingCourse.pending ||
              p.statuses.renamingModule.pending
            }
            onEnter={async () => this._submitRenameModule()}
            errorField='name'
            error={p.statuses.renamingModule.error}
            maxWidth='600px'
            stacked={true}
          />

          <Content slot='footer'>
            <Button
              label='Save'
              float='right'
              actionType='primary'
              disabled={
                p.statuses.loadingCourse.pending ||
                p.statuses.renamingModule.pending
              }
              onClick={async () => this._submitRenameModule()}
            />
          </Content>
        </Frame>
      </DashboardPage>
    )
  }

  private _onNameChanged(name: string) {
    this.setState({ name })
  }

  private async _submitRenameModule() {
    const p = this.props.page!

    await p.renameModule(this.state.name)
    if (p.statuses.renamingModule.hasError) return
    if (!p.module) return

    this.setState({ name: p.module.name })
  }
}
