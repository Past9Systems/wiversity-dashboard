import { inject, observer } from 'mobx-react'
import * as React from 'react'
import { Router } from 'router5'
import { IServices } from '../..'
import Button from '../../components/Button'
import DashboardPage from '../../components/DashboardPage'
import Frame from '../../components/Frame'
import TextField from '../../components/TextField'
import { RouteNames } from '../../Routes'
import EditCourseDetailsPageState from '../../state/courses/EditCourseDetailsPageState'
import DashboardState from '../../state/DashboardState'

export interface IEditCourseDetailsProps {
  router?: Router
  page?: EditCourseDetailsPageState
  dashboard?: DashboardState
}

export interface IEditCourseDetailsPageInternalState {
  name: string
  shortDescription: string
  longDescription: string
}

@inject((services: IServices) => ({
  dashboard: services.store.app.dashboard,
  page: services.store.app.dashboard.editCourseDetailsPage,
  router: services.router
}))
@observer
export default class EditCourseDetailsPage extends React.Component<
  IEditCourseDetailsProps,
  IEditCourseDetailsPageInternalState
> {
  public constructor(props: IEditCourseDetailsProps) {
    super(props)
    this.state = {
      longDescription: '',
      name: '',
      shortDescription: ''
    }
  }

  public componentWillMount() {
    this.props.page!.onWillMount()
  }

  public async componentDidMount() {
    this.props.page!.resetState()
    await this.props.page!.loadCourse()
    this._syncState()
  }

  public onNameChange(name: string) {
    this.setState({ name })
  }

  public onShortDescriptionChange(shortDescription: string) {
    this.setState({ shortDescription })
  }

  public onLongDescriptionChange(longDescription: string) {
    this.setState({ longDescription })
  }

  public render() {
    const page = this.props.page!

    return (
      <DashboardPage>
        <Frame
          headingSize={2}
          heading='Edit Course Details'
          breadcrumbs={{
            crumbs: [
              {
                routeName: RouteNames.Home,
                routeParams: {},
                text: 'Home'
              },
              {
                routeName: RouteNames.SchoolDetails,
                routeParams: { schoolId: this.props.dashboard!.schoolId },
                text:
                  page.course && page.course!.school && page.course.school!.name
              },
              {
                routeName: RouteNames.CoursesInSchool,
                routeParams: { schoolId: this.props.dashboard!.schoolId },
                text: 'Courses'
              },
              {
                routeName: RouteNames.CourseDetails,
                routeParams: {
                  courseId: page.courseId,
                  schoolId: this.props.dashboard!.schoolId
                },
                text: page.course && page.course.name
              }
            ],
            current: 'Edit Course Details',
            noMargin: true
          }}
        >
          <TextField
            label='Course name'
            value={this.state.name}
            placeholder='Enter your new course&#39;s name here'
            onChange={e => this.onNameChange(e)}
            stacked={true}
            autoFocus={true}
            disabled={
              page.statuses.loadingCourse.pending ||
              page.statuses.updatingCourse.pending
            }
            onEnter={async () => this._submitForm()}
            errorField='name'
            error={page.statuses.updatingCourse.error}
            maxWidth='600px'
          />

          <TextField
            label='Short description'
            value={this.state.shortDescription}
            placeholder='Enter a summary of your course here (or you can do this later)'
            onChange={e => this.onShortDescriptionChange(e)}
            stacked={true}
            disabled={
              page.statuses.loadingCourse.pending ||
              page.statuses.updatingCourse.pending
            }
            errorField='shortDescription'
            error={page.statuses.updatingCourse.error}
            maxWidth='600px'
            multiline={true}
            height='4rem'
          />

          <TextField
            label='Long description'
            value={this.state.longDescription}
            placeholder='Enter a detailed description of your course here (or you can do this later)'
            onChange={e => this.onLongDescriptionChange(e)}
            stacked={true}
            disabled={
              page.statuses.loadingCourse.pending ||
              page.statuses.updatingCourse.pending
            }
            errorField='longDescription'
            error={page.statuses.updatingCourse.error}
            maxWidth='600px'
            multiline={true}
            height='12rem'
          />

          <Button
            label='Create'
            actionType='primary'
            float='right'
            disabled={
              page.statuses.loadingCourse.pending ||
              page.statuses.updatingCourse.pending
            }
            onClick={async () => this._submitForm()}
          />
        </Frame>
      </DashboardPage>
    )
  }

  private async _submitForm() {
    await this.props.page!.updateCourseDetails(
      this.state.name,
      this.state.shortDescription,
      this.state.longDescription
    )
    this._syncState()
  }

  private _syncState() {
    this.setState({
      longDescription: this.props.page!.course
        ? this.props.page!.course!.longDescription
        : '',
      name: this.props.page!.course ? this.props.page!.course!.name : '',
      shortDescription: this.props.page!.course
        ? this.props.page!.course!.shortDescription
        : ''
    })
  }
}
