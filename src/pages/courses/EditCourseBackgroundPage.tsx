import { inject, observer } from 'mobx-react'
import * as React from 'react'
import { IServices } from '../..'
import Button from '../../components/Button'
import Content from '../../components/Content'
import DashboardPage from '../../components/DashboardPage'
import FileInput, { IUniqueFile } from '../../components/FileInput'
import Frame from '../../components/Frame'
import Image from '../../components/Image'
import Notification from '../../components/Notification'
import { RouteNames } from '../../Routes'
import EditCourseBackgroundPageState from '../../state/courses/EditCourseBackgroundImageState'
import DashboardState from '../../state/DashboardState'

export interface IEditCourseBackgroundPageProps {
  page?: EditCourseBackgroundPageState
  dashboard?: DashboardState
}

@inject((services: IServices) => ({
  dashboard: services.store.app.dashboard,
  page: services.store.app.dashboard.editCourseBackgroundPage
}))
@observer
export default class EditCourseBackgroundPage extends React.Component<
  IEditCourseBackgroundPageProps
> {
  public constructor(props: IEditCourseBackgroundPageProps) {
    super(props)
  }

  public componentWillMount() {
    this.props.page!.onWillMount()
  }

  public async componentDidMount() {
    await this.props.page!.loadCourse()
  }

  public render() {
    const page = this.props.page!

    return (
      <DashboardPage>
        <Frame
          headingSize={3}
          heading='Set Background Image '
          breadcrumbs={{
            crumbs: [
              {
                routeName: RouteNames.Home,
                routeParams: {},
                text: 'Home'
              },
              {
                routeName: RouteNames.SchoolDetails,
                routeParams: { schoolId: this.props.dashboard!.schoolId },
                text:
                  page.course && page.course!.school && page.course.school!.name
              },
              {
                routeName: RouteNames.CoursesInSchool,
                routeParams: { schoolId: this.props.dashboard!.schoolId },
                text: 'Courses'
              },
              {
                routeName: RouteNames.CourseDetails,
                routeParams: {
                  courseId: page.courseId,
                  schoolId: this.props.dashboard!.schoolId
                },
                text: page.course && page.course.name
              }
            ],
            current: 'Set Background Image',
            noMargin: true
          }}
        >
          <Image
            documentId={
              page.course &&
              page.course.backgroundImage &&
              page.course.backgroundImage.id
            }
            token={
              page.course &&
              page.course.backgroundImage &&
              page.course.backgroundImage.dataAccessToken
            }
          />

          <FileInput
            files={page.files}
            multiple={false}
            onChange={e => this._onImageChanged(e)}
            error={page.statuses.uploadingImage.error}
          />

          <Content slot='footer'>
            <Button
              label='Upload'
              float='right'
              actionType='primary'
              disabled={
                page.statuses.uploadingImage.pending ||
                page.statuses.uploadingImage.pending
              }
              onClick={async () => this._submitUploadImage()}
            />
          </Content>
        </Frame>
      </DashboardPage>
    )
  }

  private _onImageChanged(files: IUniqueFile[]) {
    this.props.page!.setFiles(files)
  }

  private async _submitUploadImage() {
    await this.props.page!.uploadBackgroundImage()
  }
}
