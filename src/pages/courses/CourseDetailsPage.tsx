import { inject, observer } from 'mobx-react'
import * as React from 'react'
import { IServices } from '../..'
import Button from '../../components/Button'
import Content from '../../components/Content'
import DashboardPage from '../../components/DashboardPage'
import Frame from '../../components/Frame'
import Image from '../../components/Image'
import { RouteNames } from '../../Routes'
import CourseDetailsPageState from '../../state/courses/CourseDetailsPageState'
import DashboardState from '../../state/DashboardState'

import { FontAwesomeIcon as Icon } from '@fortawesome/react-fontawesome'
import Column from '../../components/Column'
import If from '../../components/If'
import { booksIcon, cameraIcon, editIcon } from '../../lib/Icons'

export interface ICourseDetailsProps {
  page?: CourseDetailsPageState
  dashboard?: DashboardState
}

@inject((services: IServices) => ({
  dashboard: services.store.app.dashboard,
  page: services.store.app.dashboard.courseDetailsPage
}))
@observer
export default class CourseDetailsPage extends React.Component<
  ICourseDetailsProps
> {
  public componentWillMount() {
    this.props.page!.onWillMount()
  }

  public async componentDidMount() {
    this.props.page!.clearCourse()
    await this.props.page!.loadCourse()
  }

  public render() {
    const page = this.props.page!

    return (
      <DashboardPage>
        <Frame
          headingSize={3}
          heading={page.course && page.course!.name}
          noPadding={true}
          breadcrumbs={{
            crumbs: [
              {
                routeName: RouteNames.Home,
                routeParams: {},
                text: 'Home'
              },
              {
                routeName: RouteNames.SchoolDetails,
                routeParams: { schoolId: this.props.dashboard!.schoolId },
                text:
                  page.course && page.course.school && page.course.school.name
              },
              {
                routeName: RouteNames.CoursesInSchool,
                routeParams: { schoolId: this.props.dashboard!.schoolId },
                text: 'Courses'
              }
            ],
            current: page.course && page.course.name,
            noMargin: true
          }}
        >
          <Content slot='aboveHeading'>
            <If condition={page.statuses.loadingCourse.successful}>
              <Button
                tooltip='Edit school details'
                actionType='secondary-caption'
                routeName={RouteNames.EditCourseDetails}
                routeParams={{
                  courseId: page.course && page.course!.id,
                  schoolId: this.props.dashboard!.schoolId
                }}
                float='right'
                size='small'
              >
                <Icon icon={editIcon} />
              </Button>
            </If>
          </Content>

          <Image
            documentId={
              page.course &&
              page.course.backgroundImage &&
              page.course.backgroundImage.id
            }
            token={
              page.course &&
              page.course.backgroundImage &&
              page.course.backgroundImage.dataAccessToken
            }
          >
            <Button
              tooltip='Edit school details'
              actionType='secondary-caption'
              routeName={RouteNames.EditCourseBackground}
              routeParams={{
                courseId: this.props.page!.courseId,
                schoolId: this.props.dashboard!.schoolId
              }}
              size='small'
              absolutePosition={true}
              right='1.15rem'
              top='1.15rem'
            >
              <Icon icon={cameraIcon} />
            </Button>
          </Image>

          <Content slot='footer'>
            <Column span={8} />
            <Column span={8}>
              <Button
                tooltip='Courses in school'
                actionType='secondary-caption'
                center={true}
                routeName={RouteNames.EditCourseContent}
                routeParams={{
                  courseId: this.props.page!.courseId,
                  schoolId: this.props.dashboard!.schoolId
                }}
              >
                <Icon icon={booksIcon} />
                &nbsp; &nbsp; Edit Content
              </Button>
            </Column>
            <Column span={8} />
          </Content>
        </Frame>
      </DashboardPage>
    )
  }
}
