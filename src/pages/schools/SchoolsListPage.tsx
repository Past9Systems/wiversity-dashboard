import { inject, observer } from 'mobx-react'
import * as React from 'react'
import { Router } from 'router5'
import { IServices } from '../..'
import Column from '../../components/Column'
import Notification from '../../components/Notification'
import ProgressBar from '../../components/ProgressBar'
import { RouteNames } from '../../Routes'
import DashboardState from '../../state/DashboardState'
import SchoolsListPageState from '../../state/schools/SchoolsListPageState'

export interface ISchoolsListProps {
  router?: Router
  page?: SchoolsListPageState
  dashboard?: DashboardState
}

@inject((services: IServices) => ({
  dashboard: services.store.app.dashboard,
  page: services.store.app.dashboard.schoolsListPage,
  router: services.router
}))
@observer
export default class SchoolsListPage extends React.Component<
  ISchoolsListProps
> {
  public async componentWillMount() {
    this.props.page!.onWillMount()
  }

  public async componentDidMount() {
    const page = this.props.page!

    const school = await page.loadFirstOwnedSchool()

    // If the API call errored out, don't go anywhere. We want the
    // user to be able to see the error message on this page.
    if (page.statuses.loadingSchools.hasError) return

    if (school === null) this.props.router!.navigate(RouteNames.CreateSchool)
    else
      this.props.router!.navigate(
        RouteNames.SchoolDetails,
        { schoolId: school.id },
        { replace: true }
      )
  }

  public render() {
    const page = this.props.dashboard!.schoolsListPage

    return null
  }
}
