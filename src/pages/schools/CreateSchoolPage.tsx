import { inject, observer } from 'mobx-react'
import * as React from 'react'
import { Router } from 'router5'
import { IServices } from '../..'
import Button from '../../components/Button'
import Column from '../../components/Column'
import DashboardPage from '../../components/DashboardPage'
import Frame from '../../components/Frame'
import Notification from '../../components/Notification'
import TextField from '../../components/TextField'
import { RouteNames } from '../../Routes'
import CreateSchoolPageState from '../../state/schools/CreateSchoolPageState'

export interface ICreateSchoolProps {
  router?: Router
  page?: CreateSchoolPageState
}

export interface ICreateSchoolPageInternalState {
  schoolName: string
}

@inject((services: IServices) => ({
  page: services.store.app.dashboard.createSchoolPage,
  router: services.router
}))
@observer
export default class CreateSchoolPage extends React.Component<
  ICreateSchoolProps,
  ICreateSchoolPageInternalState
> {
  public constructor(props: ICreateSchoolProps) {
    super(props)
    this.props.page!.defaultState()
    this.state = {
      schoolName: ''
    }
  }

  public componentWillMount() {
    this.props.page!.onWillMount()
  }

  public onSchoolNameChange(schoolName: string) {
    this.setState({ schoolName })
  }

  public render() {
    const page = this.props.page!

    return (
      <>
        <Column span={6} />
        <Column span={12}>
          <DashboardPage>
            <Frame headingSize={2} heading='Create school'>
              <Notification type='info'>
                To get started, you need to first create a school. A school is
                like a store where your students can browse, purchase, and
                participate in your courses.
                <br />
                <br />
                Enter the name of your school below and click Create.
                <br />
                <br />
                <sup>(You can always change this later)</sup>
              </Notification>

              <TextField
                value={this.state.schoolName}
                placeholder='Enter your new school&#39;s name here'
                onChange={e => this.onSchoolNameChange(e)}
                stacked={true}
                autoFocus={true}
                disabled={page.statuses.createSchool.pending}
                onEnter={async () => this._submitForm()}
                errorField='name'
                error={page.statuses.createSchool.error}
              />

              <Button
                label='Create'
                actionType='primary'
                float='right'
                disabled={page.statuses.createSchool.pending}
                onClick={async () => this._submitForm()}
              />
            </Frame>
          </DashboardPage>
        </Column>
        <Column span={6} />
      </>
    )
  }

  private async _submitForm() {
    await this.props.page!.createSchool(this.state.schoolName)
    if (!this.props.page!.school) return
    this.props.router!.navigate(RouteNames.SchoolDetails, {
      schoolId: this.props.page!.school!.id
    })
  }
}
