import { inject, observer } from 'mobx-react'
import * as React from 'react'
import { IServices } from '../..'
import Button from '../../components/Button'
import Content from '../../components/Content'
import DashboardPage from '../../components/DashboardPage'
import Frame from '../../components/Frame'
import Notification from '../../components/Notification'
import TextField from '../../components/TextField'
import { RouteNames } from '../../Routes'
import DashboardState from '../../state/DashboardState'
import EditSchoolPageState from '../../state/schools/EditSchoolPageState'

export interface IEditSchoolPageProps {
  page?: EditSchoolPageState
  dashboard?: DashboardState
}

export interface IEditSchoolPageInternalState {
  name: string
}

@inject((services: IServices) => ({
  dashboard: services.store.app.dashboard,
  page: services.store.app.dashboard.editSchoolPage
}))
@observer
export default class EditSchoolPage extends React.Component<
  IEditSchoolPageProps,
  IEditSchoolPageInternalState
> {
  public constructor(props: IEditSchoolPageProps) {
    super(props)
    this.state = {
      name: ''
    }
  }

  public componentWillMount() {
    this.props.page!.onWillMount()
  }

  public async componentDidMount() {
    await this.props.page!.loadSchool()
    this.setState({
      name: this.props.page!.school ? this.props.page!.school!.name : ''
    })
  }

  public render() {
    const page = this.props.page!

    return (
      <DashboardPage>
        <Frame
          headingSize={3}
          heading='Edit School'
          breadcrumbs={{
            crumbs: [
              {
                routeName: RouteNames.Home,
                routeParams: {},
                text: 'Home'
              },
              {
                routeName: RouteNames.SchoolDetails,
                routeParams: { schoolId: this.props.dashboard!.schoolId },
                text: page.school && page.school.name
              }
            ],
            current: 'Edit School',
            noMargin: true
          }}
        >
          <TextField
            label='School name'
            value={this.state.name}
            onChange={e => this._onNameChanged(e)}
            disabled={
              page.statuses.renamingSchool.pending ||
              page.statuses.loadingSchool.pending
            }
            onEnter={async () => this._submitRenameSchool()}
            errorField='name'
            error={page.statuses.renamingSchool.error}
            maxWidth='600px'
            stacked={true}
          />

          <Content slot='footer'>
            <Button
              label='Save'
              float='right'
              actionType='primary'
              disabled={
                page.statuses.loadingSchool.pending ||
                page.statuses.renamingSchool.pending
              }
              onClick={async () => this._submitRenameSchool()}
            />
          </Content>
        </Frame>
      </DashboardPage>
    )
  }

  private _onNameChanged(name: string) {
    this.setState({ name })
  }

  private async _submitRenameSchool() {
    await this.props.page!.renameSchool(this.state.name)

    if (!this.props.page!.statuses.renamingSchool.hasError)
      this.setState({
        name: this.props.page!.school ? this.props.page!.school!.name : ''
      })
  }
}
