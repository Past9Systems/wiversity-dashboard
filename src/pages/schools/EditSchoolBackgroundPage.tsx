import { inject, observer } from 'mobx-react'
import * as React from 'react'
import { IServices } from '../..'
import Button from '../../components/Button'
import Content from '../../components/Content'
import DashboardPage from '../../components/DashboardPage'
import FileInput, { IUniqueFile } from '../../components/FileInput'
import Frame from '../../components/Frame'
import Image from '../../components/Image'
import Notification from '../../components/Notification'
import { RouteNames } from '../../Routes'
import DashboardState from '../../state/DashboardState'
import EditSchoolBackgroundPageState from '../../state/schools/EditSchoolBackgroundPageState'

export interface IEditSchoolBackgroundPageProps {
  page?: EditSchoolBackgroundPageState
  dashboard?: DashboardState
}

@inject((services: IServices) => ({
  dashboard: services.store.app.dashboard,
  page: services.store.app.dashboard.editSchoolBackgroundPage
}))
@observer
export default class EditSchoolBackgroundPage extends React.Component<
  IEditSchoolBackgroundPageProps
> {
  public constructor(props: IEditSchoolBackgroundPageProps) {
    super(props)
  }

  public async componentWillMount() {
    this.props.page!.onWillMount()
  }

  public async componentDidMount() {
    await this.props.page!.loadSchool()
  }

  public render() {
    const page = this.props.page!

    return (
      <DashboardPage>
        <Frame
          headingSize={3}
          heading='Set Background Image '
          breadcrumbs={{
            crumbs: [
              {
                routeName: RouteNames.Home,
                routeParams: {},
                text: 'Home'
              },
              {
                routeName: RouteNames.SchoolDetails,
                routeParams: { schoolId: this.props.dashboard!.schoolId },
                text: page.school && page.school.name
              }
            ],
            current: 'Set Background Image',
            noMargin: true
          }}
        >
          <Image
            documentId={
              page.school &&
              page.school.backgroundImage &&
              page.school.backgroundImage.id
            }
            token={
              page.school &&
              page.school.backgroundImage &&
              page.school.backgroundImage.dataAccessToken
            }
          />

          <FileInput
            files={page.files}
            multiple={false}
            onChange={e => this._onImageChanged(e)}
            error={page.statuses.uploadingImage.error}
          />

          <Content slot='footer'>
            <Button
              label='Upload'
              float='right'
              actionType='primary'
              disabled={
                page.statuses.uploadingImage.pending ||
                page.statuses.uploadingImage.pending
              }
              onClick={async () => this._submitUploadImage()}
            />
          </Content>
        </Frame>
      </DashboardPage>
    )
  }

  private _onImageChanged(files: IUniqueFile[]) {
    this.props.page!.setFiles(files)
  }

  private async _submitUploadImage() {
    await this.props.page!.uploadBackgroundImage()
  }
}
