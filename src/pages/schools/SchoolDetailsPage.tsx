import * as React from 'react'
import Button from '../../components/Button'
import Column from '../../components/Column'
import Content from '../../components/Content'
import DashboardPage from '../../components/DashboardPage'
import Frame from '../../components/Frame'
import If from '../../components/If'
import Image from '../../components/Image'
import { RouteNames } from '../../Routes'

import { FontAwesomeIcon as Icon } from '@fortawesome/react-fontawesome'
import { inject, observer } from 'mobx-react'
import { IServices } from '../..'
import {
  cameraIcon,
  chalkboardIcon,
  dollarSignIcon,
  editIcon,
  userGraduateIcon
} from '../../lib/Icons'
import DashboardState from '../../state/DashboardState'
import SchoolDetailsPageState from '../../state/schools/SchoolDetailsPageState'

export interface ISchoolDetailsPageProps {
  page?: SchoolDetailsPageState
  dashboard?: DashboardState
}

@inject((services: IServices) => ({
  dashboard: services.store.app.dashboard,
  page: services.store.app.dashboard.schoolDetailsPage
}))
@observer
export default class SchoolDetailsPage extends React.Component<
  ISchoolDetailsPageProps
> {
  public componentWillMount() {
    this.props.page!.onWillMount()
  }

  public async componentDidMount() {
    this.props.page!.clearSchool()
    await this.props.page!.loadSchool()
  }

  public render() {
    const page = this.props.page!

    return (
      <DashboardPage>
        <Frame
          headingSize={3}
          heading={page.school && page.school!.name}
          noPadding={true}
          breadcrumbs={{
            crumbs: [
              {
                routeName: RouteNames.Home,
                routeParams: {},
                text: 'Home'
              }
            ],
            current: page.school && page.school.name,
            noMargin: true
          }}
        >
          <Content slot='aboveHeading'>
            <Button
              tooltip='Edit school details'
              actionType='secondary-caption'
              routeName={RouteNames.EditSchool}
              routeParams={{ schoolId: this.props.dashboard!.schoolId }}
              float='right'
              size='small'
            >
              <Icon icon={editIcon} />
            </Button>
          </Content>

          <Image
            documentId={
              page.school &&
              page.school.backgroundImage &&
              page.school.backgroundImage.id
            }
            token={
              page.school &&
              page.school.backgroundImage &&
              page.school.backgroundImage.dataAccessToken
            }
          >
            <Button
              tooltip='Edit school details'
              actionType='secondary-caption'
              routeName={RouteNames.EditSchoolBackground}
              routeParams={{ schoolId: this.props.dashboard!.schoolId }}
              size='small'
              absolutePosition={true}
              right='1.15rem'
              top='1.15rem'
            >
              <Icon icon={cameraIcon} />
            </Button>
          </Image>

          <Content slot='footer'>
            <Column span={8}>
              <Button
                tooltip='Courses in school'
                actionType='secondary-caption'
                center={true}
                routeName={RouteNames.CoursesInSchool}
                routeParams={{ schoolId: this.props.dashboard!.schoolId }}
              >
                <Icon icon={chalkboardIcon} />
                <If condition={page.statuses.loadingSchool.successful}>
                  &nbsp; &nbsp;
                  {page.school && page.school.courses.length.toString()}
                  &nbsp;
                  {`${
                    page.school && page.school.courses.length === 1
                      ? 'course'
                      : 'courses'
                  }`}
                </If>
              </Button>
            </Column>
            <Column span={8}>
              <Button
                tooltip='Students in school'
                actionType='secondary-caption'
                center={true}
                routeName={RouteNames.StudentsInSchoolList}
                routeParams={{ schoolId: this.props.dashboard!.schoolId }}
              >
                <Icon icon={userGraduateIcon} />
                <If condition={page.statuses.loadingSchool.successful}>
                  &nbsp; &nbsp; 78 students
                </If>
              </Button>
            </Column>
            <Column span={8}>
              <Button
                tooltip='Finances'
                actionType='secondary-caption'
                center={true}
                routeName={RouteNames.StudentsInSchoolList}
                routeParams={{ schoolId: this.props.dashboard!.schoolId }}
              >
                <Icon icon={dollarSignIcon} />
                <If condition={page.statuses.loadingSchool.successful}>
                  &nbsp; -- revenue
                </If>
              </Button>
            </Column>
          </Content>
        </Frame>
      </DashboardPage>
    )
  }
}
