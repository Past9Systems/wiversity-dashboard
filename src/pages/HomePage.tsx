import { inject, observer } from 'mobx-react'
import * as React from 'react'
import { Router } from 'router5'
import { RouteNames } from '../Routes'

export interface IHomeProps {
  router?: Router
}

@inject('router')
@observer
export default class HomePage extends React.Component<IHomeProps> {
  public componentDidMount() {
    this.props.router!.navigate(RouteNames.SchoolsList, {}, { replace: true })
  }

  // tslint:disable-next-line:prefer-function-over-method
  public render() {
    return null
  }
}
