import { inject, observer } from 'mobx-react'
import * as React from 'react'
import { IServices } from '../..'
import Button from '../../components/Button'
import { Card, CardBody } from '../../components/Card'
import DashboardPage from '../../components/DashboardPage'
import If from '../../components/If'
import Notification from '../../components/Notification'
import ProgressBar from '../../components/ProgressBar'
import { RouteNames } from '../../Routes'
import VerifyEmailPageState from '../../state/account/VerifyEmailPageState'

export interface IVerifyEmailProps {
  page?: VerifyEmailPageState
}

@inject((services: IServices) => ({
  page: services.store.app.dashboard.verifyEmailPage
}))
@observer
export default class VerifyEmailPage extends React.Component<
  IVerifyEmailProps
> {
  public componentWillMount() {
    this.props.page!.onWillMount()
  }

  public async componentDidMount() {
    await this.props.page!.verifyEmail()
  }

  public render() {
    const page = this.props.page!

    return (
      <>
        <DashboardPage maxWidth='460px'>
          <Card>
            <CardBody>
              <If condition={page.statuses.verifyingEmail.successful}>
                <Notification type='success'>
                  Your email address{' '}
                  <strong>{page.creator && page.creator.email}</strong> has been
                  verified!
                </Notification>
              </If>

              <If condition={!page.statuses.verifyingEmail.pending}>
                <Button
                  label='Go to my account'
                  center={true}
                  actionType='primary'
                  routeName={RouteNames.Account}
                />
              </If>
            </CardBody>
          </Card>
        </DashboardPage>
      </>
    )
  }
}
