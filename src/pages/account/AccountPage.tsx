import { inject, observer } from 'mobx-react'
import * as React from 'react'
import { IServices } from '../..'
import Button from '../../components/Button'
import DashboardPage from '../../components/DashboardPage'
import Frame from '../../components/Frame'
import If from '../../components/If'
import Notification from '../../components/Notification'
import TextField from '../../components/TextField'
import AccountPageState from '../../state/account/AccountPageState'

export interface IAccountProps {
  page?: AccountPageState
}

export interface IAccountPageInternalState {
  email: string
  currentPassword: string
  newPassword: string
  confirmPassword: string
}

@inject((services: IServices) => ({
  page: services.store.app.dashboard.accountPage
}))
@observer
export default class AccountPage extends React.Component<
  IAccountProps,
  IAccountPageInternalState
> {
  public constructor(props: IAccountProps) {
    super(props)
    this.state = {
      confirmPassword: '',
      currentPassword: '',
      email: '',
      newPassword: ''
    }
  }

  public componentWillMount() {
    this.props.page!.onWillMount()
  }

  public async componentDidMount() {
    await this.props.page!.loadAccount()
    this.setState({
      email: this.props.page!.account ? this.props.page!.account!.email : ''
    })
  }

  public render() {
    const page = this.props.page!

    return (
      <>
        <DashboardPage maxWidth='460px'>
          <Frame headingSize={2} heading='My Account' />

          <Frame headingSize={3} heading='Change email address'>
            <If condition={!!page.account && !!page.account.pendingEmail}>
              <Notification type='info'>
                You requested that we set your email address to{' '}
                <strong>{page.account && page.account.pendingEmail}</strong>.
                We've sent a verification message to that account. Click the
                link in the message to complete the change.
              </Notification>
              <Button
                label='Cancel pending change'
                float='right'
                actionType='primary'
                onClick={async () => page.cancelPendingEmailChange()}
              />
            </If>

            <If condition={!page.account || !page.account.pendingEmail}>
              <TextField
                label='Current email address'
                value={this.state.email}
                onChange={e => this._onEmailChanged(e)}
                disabled={
                  page.statuses.loadingAccount.pending ||
                  page.statuses.changingEmail.pending
                }
                onEnter={async () => this._submitChangeEmailAddress()}
                errorField='email'
                error={page.statuses.changingEmail.error}
                stacked={true}
              />

              <Button
                label='Change email'
                float='right'
                actionType='primary'
                disabled={
                  page.statuses.loadingAccount.pending ||
                  page.statuses.changingEmail.pending
                }
                onClick={async () => this._submitChangeEmailAddress()}
              />
            </If>
          </Frame>

          <Frame headingSize={3} heading='Change password'>
            <TextField
              label='Current password'
              value={this.state.currentPassword}
              onChange={e => this._onCurrentPasswordChanged(e)}
              disabled={
                page.statuses.loadingAccount.pending ||
                page.statuses.changingPassword.pending
              }
              onEnter={async () => this._submitChangePassword()}
              errorField='currentPassword'
              error={page.statuses.changingPassword.error}
              password={true}
              stacked={true}
            />

            <TextField
              label='New password'
              value={this.state.newPassword}
              onChange={e => this._onNewPasswordChanged(e)}
              disabled={
                page.statuses.loadingAccount.pending ||
                page.statuses.changingPassword.pending
              }
              onEnter={async () => this._submitChangePassword()}
              errorField='password'
              error={page.statuses.changingPassword.error}
              password={true}
              stacked={true}
            />

            <TextField
              label='Confirm new password'
              value={this.state.confirmPassword}
              onChange={e => this._onConfirmPasswordChanged(e)}
              disabled={
                page.statuses.loadingAccount.pending ||
                page.statuses.changingPassword.pending
              }
              onEnter={async () => this._submitChangePassword()}
              errorField='confirmPassword'
              error={page.statuses.changingPassword.error}
              password={true}
              stacked={true}
            />

            <Button
              label='Change password'
              float='right'
              actionType='primary'
              disabled={
                page.statuses.loadingAccount.pending ||
                page.statuses.changingPassword.pending
              }
              onClick={async () => this._submitChangePassword()}
            />
          </Frame>
        </DashboardPage>
      </>
    )
  }

  private _onEmailChanged(email: string) {
    this.setState({ email })
  }

  private async _submitChangeEmailAddress() {
    await this.props.page!.changeEmailAddress(this.state.email)

    if (!this.props.page!.statuses.loadingAccount.error)
      this.setState({
        email: this.props.page!.account ? this.props.page!.account!.email : ''
      })
  }

  private _onCurrentPasswordChanged(currentPassword: string) {
    this.setState({ currentPassword })
  }

  private _onNewPasswordChanged(newPassword: string) {
    this.setState({ newPassword })
  }

  private _onConfirmPasswordChanged(confirmPassword: string) {
    this.setState({ confirmPassword })
  }

  private async _submitChangePassword() {
    if (this.state.newPassword !== this.state.confirmPassword) {
      this.props.page!.statuses.changingPassword.fail({
        invalidFields: {
          confirmPassword: 'Passwords do not match.',
          password: 'Passwords do not match.'
        },
        message: 'Some fields are invalid.'
      } as any)
      return
    }

    await this.props.page!.changePassword(
      this.state.currentPassword,
      this.state.newPassword
    )

    if (!this.props.page!.statuses.changingPassword.error) {
      this.setState({
        confirmPassword: '',
        currentPassword: '',
        newPassword: ''
      })
    }
  }
}
