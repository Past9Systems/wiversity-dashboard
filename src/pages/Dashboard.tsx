import * as React from 'react'
import Menu from '../components/Menu'
import OnRoute from '../components/OnRoute'
import Toolbar from '../components/Toolbar'
import { RouteNames } from '../Routes'
import AccountPage from './account/AccountPage'
import VerifyEmailPage from './account/VerifyEmailPage'
import CourseDetailsPage from './courses/CourseDetailsPage'
import CoursesInSchoolPage from './courses/CoursesInSchoolPage'
import CreateCoursePage from './courses/CreateCoursePage'
import HomePage from './HomePage'
import CreateSchoolPage from './schools/CreateSchoolPage'
import EditSchoolPage from './schools/EditSchoolPage'
import SchoolDetailsPage from './schools/SchoolDetailsPage'
import SchoolsListPage from './schools/SchoolsListPage'

import { observe } from 'mobx'
import { inject, observer } from 'mobx-react'
import { IServices } from '..'
import DashboardState from '../state/DashboardState'
import EditCourseBackgroundPage from './courses/EditCourseBackgroundPage'
import EditCourseContentPage from './courses/EditCourseContentPage'
import EditCourseDetailsPage from './courses/EditCourseDetailsPage'
import EditSchoolBackgroundPage from './schools/EditSchoolBackgroundPage'

import EditModulePage from './courses/EditModulePage'
import EditUnitPage from './courses/EditUnitPage'
import './Dashboard.scss'

export interface IDashboardProps {
  page?: DashboardState
}

@inject((services: IServices) => ({
  page: services.store.app.dashboard
}))
@observer
export default class Dashboard extends React.Component<IDashboardProps> {
  private _scrollTopObserverDisposer: () => void
  private _content: HTMLDivElement | null

  public constructor(props: IDashboardProps) {
    super(props)
  }

  public componentDidMount() {
    this._scrollTopObserverDisposer = observe(this.props.page!, change => {
      if (change.name === 'contentScrollTop') {
        if (this._content !== null) {
          // Scroll the page
          this._content.scrollTop = this.props.page!.contentScrollTop
          // Don't let the state property go past the actual scrollTop of the dashboard
          this.props.page!.contentScrollTop = this._content.scrollTop
        }
      } else if (change.name === 'contentHeightLocked') {
        if (this._content !== null) return
      }
    })

    // Get the initial content height
    this._onResize(null)

    window.addEventListener('resize', this._onResize.bind(this))

    // Get the initial scroll offset
    this._onScroll(null)
  }

  public componentWillUnmount() {
    this._scrollTopObserverDisposer()
    window.removeEventListener('resize', this._onResize.bind(this))
  }

  private _onResize(_: UIEvent | null) {
    if (this._content === null) return

    this.props.page!.contentClientHeight = this._content.clientHeight
    this.props.page!.contentOffsetTop = this._content.offsetTop
  }

  private _onScroll(_: React.UIEvent<HTMLDivElement> | null) {
    if (this._content === null) return

    this.props.page!.contentScrollTop = this._content.scrollTop
    this.props.page!.contentOffsetTop = this._content.offsetTop
  }

  public render() {
    return (
      <div className='dashboard'>
        <Menu />

        <Toolbar />

        <div
          className='dashboard__content'
          ref={e => (this._content = e)}
          onScroll={e => this._onScroll(e)}
        >
          <OnRoute routeName={RouteNames.Home}>
            <HomePage />
          </OnRoute>

          <OnRoute routeName={RouteNames.Account}>
            <AccountPage />
          </OnRoute>

          <OnRoute routeName={RouteNames.VerifyEmail}>
            <VerifyEmailPage />
          </OnRoute>

          <OnRoute routeName={RouteNames.SchoolsList}>
            <SchoolsListPage />
          </OnRoute>

          <OnRoute routeName={RouteNames.CreateSchool}>
            <CreateSchoolPage />
          </OnRoute>

          <OnRoute routeName={RouteNames.SchoolDetails}>
            <SchoolDetailsPage />
          </OnRoute>

          <OnRoute routeName={RouteNames.EditSchool}>
            <EditSchoolPage />
          </OnRoute>

          <OnRoute routeName={RouteNames.EditSchoolBackground}>
            <EditSchoolBackgroundPage />
          </OnRoute>

          <OnRoute routeName={RouteNames.CreateCourse}>
            <CreateCoursePage />
          </OnRoute>

          <OnRoute routeName={RouteNames.EditCourseDetails}>
            <EditCourseDetailsPage />
          </OnRoute>

          <OnRoute routeName={RouteNames.EditCourseBackground}>
            <EditCourseBackgroundPage />
          </OnRoute>

          <OnRoute routeName={RouteNames.EditCourseContent}>
            <EditCourseContentPage />
          </OnRoute>

          <OnRoute routeName={RouteNames.EditModule}>
            <EditModulePage />
          </OnRoute>

          <OnRoute routeName={RouteNames.EditUnit}>
            <EditUnitPage />
          </OnRoute>

          <OnRoute routeName={RouteNames.CoursesInSchool}>
            <CoursesInSchoolPage />
          </OnRoute>

          <OnRoute routeName={RouteNames.CourseDetails}>
            <CourseDetailsPage />
          </OnRoute>
        </div>
      </div>
    )
  }
}
