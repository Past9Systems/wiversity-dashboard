const {
  FuseBox,
  WebIndexPlugin,
  SassPlugin,
  CSSPlugin,
  CSSResourcePlugin,
  CopyPlugin,
  Sparky
} = require('fuse-box')

const fuse = FuseBox.init({
  homeDir: 'src',
  target: 'browser@es5',
  output: 'dist/$name.js',
  cache: true,
  sourceMaps: true,
  plugins: [
    [
      SassPlugin(),
      CSSResourcePlugin({
        dist: 'dist/assets',
        resolve: f => `/assets/${f}`
      }),
      CSSPlugin()
    ],
    CopyPlugin({
      files: ['*.txt'],
      dest: 'files',
      resolve: '/files'
    }),
    WebIndexPlugin({
      template: 'src/index.html'
    })
  ]
})

fuse.dev({
  open: true,
  port: 3000,
  fallback: 'index.html'
})

fuse.bundle('vendor').instructions(' ~ index.tsx')

fuse
  .bundle('app')
  .instructions(' > [index.tsx]')
  .hmr()
  .watch('src/**')

Sparky.task('copy-lang', () =>
  Sparky.watch('language/*.json', { base: 'src' }).dest('dist')
)

Sparky.task('default', ['copy-lang'], () => {
  fuse.run()
})
